#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 22:54:50

This host class will initialise parameters that is set in the main
class. Most of them will be stored in a parameter variable to be
used in workers. It will also build an initial grid and solve the 
TPSFEM. The grid will be divided into several sub-grids and sent to
workers. The final grid will be sent back from workers to combine 
into the final grid in the host processor.

"""

# Import other classes
from grid.Grid import Grid
from triangle.BuildEquationLinear import build_equation_linear_2D
from grid.GridDivide import subdivide_grid
from commun.Communication import Communication
from TPSFEM.TpsfemSolver import solve_tpsfem
from triangle.CombineGrids import combine_subgrids
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.RegionPlot as regionPlot
from data.DatasetManager import obtain_dataset
from data.RegionFunction import build_region, obtain_subregion, obtain_subregions
from file.CreateFolder import create_folder
from build.BuildInitialGrid import build_initial_grids


def host(commun, parameter, n, true_soln, rhs, grid_type):
	""" Initialise, partition and combine grids """

	print "Host starts"
	# Parameters
	dataset = parameter.get_dataset()
	region_no = parameter.get_region_no()

	# Create a folder to store results 
	if parameter.get_write_file():
		create_folder("./" + parameter.get_folder())


	# Find the number of processors
	p = commun.get_no_workers()

	# Create a an initial FEM n*n grid
	grid = build_initial_grid(n, true_soln, grid_type)


	# Build a data region
	data = obtain_dataset(dataset)
	region = build_region(data, [region_no,region_no])

#	regionPlot.plot_region(region)

	# Calculate matrix value and load vector value for the initial grid
	# And solve the system of equations
#	solve_fem(grid,true_soln,rhs)
	solve_tpsfem(grid, region, parameter)

	# Subdivide the grid
	sub_grids = subdivide_grid(p, grid)

#	femPlot2D.plot_fem_subgrids_2D(sub_grids)

	# Subdivide the region
	sub_regions = obtain_subregions(region, sub_grids)

#	regionPlot.plot_regions(sub_regions)

	# Set data region for each subgrid
	for i in range(p):
		sub_grids[i][0].set_region = sub_regions[i]

	print "worker   sweep    error indicator    true error"
	
	# Send each subgrid to the specified worker processor
	for i in range(p):
		commun.send_grid(i, sub_grids[i][0])



	# Initialise a list for subgrids
	subgrid_list = []

	# Receive subgrids from each worker
	for i in range(p):
		subgrid = commun.get_grid(i)
		subgrid_list.append(subgrid)

	# Combine the final subgrids of different workers
	grid = combine_subgrids(subgrid_list)

	# Plot the final grid and solution
#	femPlot2D.plot_fem_grid_2d(grid)
#	femPlot2D.plot_fem_subgrids_2d(subgrid_list)
#	tpsfemPlot2D.plot_tpsfem_solution_2d(grid)
#	tpsfemPlot2D.plot_tpsfem_solution_combine_2d(subgrid_list)

	print "Program terminates"