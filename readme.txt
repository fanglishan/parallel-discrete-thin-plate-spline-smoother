To run the program, type in terminal

mpiexec -np 5 python main.py

3 is the number of processors assigned for the program
1 for the host processor and the other will be worker processors

main.py is the file name of the program 

The node-edge data Structure is based on Stals' thesis
the parallel implementation is based on the previous FEM course codes

==========
Folders:

commun - parallel computing routines
data - data related routines
dataset - stores datasets
file - IO transfer related routines
function - stores model problem functions
grid - 
indicator - error indicator routines
plot - plot routines
result - 
TPSFEM - 
triangle - 
==========
Files:

main - 
host -
worker -
==========
