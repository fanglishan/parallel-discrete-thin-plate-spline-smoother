#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 12:46:55

The TPSFEM main program builds a uniform grid and partitioned the grid into a
given number of sub-grids. Sub-grids contains the a full node table and a ghost
node table for neighbour node tables. It will apply adaptive refinements on certain
regions until conditions are satisfied.

This class contains necessary parameters for solving the TPSFEM and starts the
host and workers.

"""

# Import other classes
from commun.Communication import Communication
from worker import worker
from host import host
from parameter.ParameterFunction import init_parameter

from function.FunctionManager import obtain_model_problem

# Import libraries
from sys import exit


""" Parameters for the initial grid """

# Dimension
d = 2

# Boundary type
# 1-Dirichlet, 2-Neumann
boundary = 1

# Basis function
# 1-linear, 2-quadratic
basis = 1

# Grd type
# 1D: 1-interval
# 2D: 2-square, 3-L-shaped
grid_type = 2

# Number of nodes (in each direction)
n = 6

# Number of data regions (in each direction)
region_no = 10


""" Parameters for the model problem """

# Model problem
# 0-zero, 1-linear, 2-quadratic, 3-cubic, 4-exp
# 5-sine, 6-oscillatory sine, 7-steep exp
model_problem = 5

# Smoothing parameter
alpha = 0.0000001    # 1e-7

# Tolerance for the solver
solver_tol = 1.0E-12

# Dataset
# 1:dimension, 2:model problem, 3:distribution
# 4:noise level, 5:data size
dataset = 25101


""" Parameters for the adaptive refinement """

# Error tolerance
#error_tol = 0.00000001   #1e-7
error_tol = 0.1

# Refinement approach
# 1: refine all above error tol
# 2: refine high indicator iteratively
refine = 2

# Number of uniform refinement before adaptive refinement
uni_refine = 0

# Maximum number of refinement in one iteration
max_no = 1

# Maximum number of refinement sweeps allowed
max_sweep = 30

# Error indicator type
# 1-Inf, 2-True, 3-refine linear Dirichlet
# 4-Dxx
indicator = 1

# Norm type
# 0-max norm, 1-one norm, 2-two norm
norm = 1

# Min data points for refinement
min_data = 0


""" Parameters for the environment """

# Whether to store info
#write_file = False
write_file = True

# Types of solution that TPSFEM will be compared to
# 1-true solution, 2-TPSFEM with fine grid
solution_type = 1

# Grid size of the fine grid
solution_grid_size = 200



###### Start the program #####

# Initialise a parameter
parameter = init_parameter(d, boundary, basis, region_no, model_problem, \
		alpha, solver_tol, dataset, error_tol, refine, uni_refine, \
		max_no, max_sweep, indicator, norm, min_data, write_file, \
		solution_type, solution_grid_size)

# Initialise commun
commun = Communication()   

# Run host and workers
if commun.get_my_no() == commun.get_host_no():
    host(commun, parameter, n, grid_type)
else:
    subgrid = worker(commun, parameter, true_soln, rhs, error_tol, solver_tol, max_no, max_sweep, indicator)
