#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-22 09:41:36
@Last Modified time: 2019-04-22 13:45:20

This class contains routines to solve the TPSFEM.

"""

# Import other classes
from grid.NodeTable import node_iterator
from grid.ConnectTable import connect_iterator
from grid.EdgeTable import endpt_iterator
from triangle.Triangle import triangle_iterator
from triangle.BuildEquationLinear import build_equation_linear_2D
#from tpsfem.OutputMatrix import output_matrices_2D, get_submatrices_2D
import plot.TpsfemPlot2D as plot2d
from parameter.GridParameter import BoundaryType
from parameter.RefineParameter import IndicatorType

# Import libraries
from numpy import inf, linalg, transpose, sqrt, concatenate, dot
from numpy.linalg import inv
from copy import copy
from scipy import eye, zeros
from scipy.sparse.linalg import spsolve
from scipy.sparse import lil_matrix, csr_matrix, csc_matrix


def obtain_node_index(node_list, node1, node2, node3):
	""" Obtain the index of the given node in the list """

	# Initialise indices of three nodes
	index1 = -1
	index2 = -1
	index3 = -1

	# Iterate through the node list
	for i in range(0, len(node_list)):

		# Compare the global id of node 1
		if str(node_list[i].get_global_id()) == str(node1.get_global_id()):

			# Set index 1
			index1 = i

		# Compare the global id of node 2
		if str(node_list[i].get_global_id()) == str(node2.get_global_id()):

			# Set index 2
			index2 = i

		# Compare the global id of node 3
		if str(node_list[i].get_global_id()) == str(node3.get_global_id()):

			# Set index 3
			index3 = i

	return index1, index2, index3



def add_submatrix_entries(grid, node_list, matrix, vector, parameter):
	""" Calculate the submatrix L values"""

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Smoothing parameter
	alpha = parameter.get_alpha()

	# Iterate through columns
	for j in range(0,len(node_list)):

		node_id2 = node_list[j].get_node_id()

		# Add values to vector
		vector[j,0] += node_list[j].get_data_load()

		# Iterate through rows
		for i in range(0,len(node_list)):

			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				matrix[i,j] = grid.get_matrix_A_value(node_id1, node_id2)

				# Get L matrix value from the existing connections
				L_value = grid.get_matrix_L_value(node_id1, node_id2)

				# Set value for submatrix L at 1,4 at 4*4 matrix
				matrix[i,j+3*no_nodes] = L_value
				matrix[i+3*no_nodes,j] = L_value
				matrix[i+no_nodes,j+no_nodes] = alpha*L_value
				matrix[i+2*no_nodes,j+2*no_nodes] = alpha*L_value

				# Get G matrix value from the existing connections
				G_value = grid.get_matrix_G_value(node_id1, node_id2)

				# Set value for submatrix G1 
				matrix[i+3*no_nodes,j+no_nodes] = G_value[0]
				matrix[j+no_nodes,i+3*no_nodes] = G_value[0]

				# Set value for submatrix G2
				matrix[i+3*no_nodes,j+2*no_nodes] = G_value[1]
				matrix[j+2*no_nodes,i+3*no_nodes] = G_value[1]


	# For Dirichlet boundary problems
	if parameter.get_boundary() == BoundaryType.Dirichlet:

		# Iterate through the node list
		for i in range(0,len(node_list)):

			node_id = node_list[i].get_node_id()

			# Iterate through each endpoint of the node
			for endpt in endpt_iterator(grid, node_id):

				# If the endpoint is a boundary point
				if grid.get_slave(endpt):

					# Node of the endpoint
					end_node = grid.get_node_ref(endpt)

					# Get matrix values from the connections in the grid
					A_value = grid.get_matrix_A_value(node_id, endpt)
					L_value = grid.get_matrix_L_value(node_id, endpt)
					G1_value = grid.get_matrix_G_value(node_id, endpt)[0]
					G1_T_value = grid.get_matrix_G_value(endpt, node_id)[0]
					G2_value = grid.get_matrix_G_value(node_id, endpt)[1]
					G2_T_value = grid.get_matrix_G_value(endpt, node_id)[1]

					# Add boundary conditions for the first subvector
					vector[i,0] -= A_value*end_node.get_value()
					vector[i,0] -= L_value*(alpha*end_node.get_d_values()[2])

					# Add boundary conditions for the second subvector
					vector[i+no_nodes,0] -= (alpha*L_value)*end_node.get_d_values()[0]
					vector[i+no_nodes,0] -= G1_T_value*(alpha*end_node.get_d_values()[2])

					# Add boundary conditions for the third subvector
					vector[i+2*no_nodes,0] -= (alpha*L_value)*end_node.get_d_values()[1]
					vector[i+2*no_nodes,0] -= G2_T_value*(alpha*end_node.get_d_values()[2])

					# Add boundary conditions for the forth subvector
					vector[i+3*no_nodes,0] -= L_value*end_node.get_value()
					vector[i+3*no_nodes,0] -= G1_value*end_node.get_d_values()[0]
					vector[i+3*no_nodes,0] -= G2_value*end_node.get_d_values()[1]


def add_uniqueness(matrix, no_nodes):
	""" Add uniquess constraints for the Neumann matrix """

	# Sum for g1=0
	matrix[4*no_nodes, no_nodes:2*no_nodes] = 1
		
	# symmetry of g1=0
	matrix[no_nodes:2*no_nodes, 4*no_nodes] = 1
	
	# Sum for g2=0
	matrix[4*no_nodes+1, 2*no_nodes:3*no_nodes] = 1
		
	# symmetry of g2=0
	matrix[2*no_nodes:3*no_nodes, 4*no_nodes+1] = 1

	# Sum for w1=0
	matrix[4*no_nodes+2, 3*no_nodes:4*no_nodes] = 1
		
	# symmetry of w1=0
	matrix[3*no_nodes:4*no_nodes, 4*no_nodes+2] = 1



###################################
# System of equations 2D
# 
# Dirichlet boundary
#
# [A  0   0    L   ] [c] = [d] - [h1]
# [0  a*L 0   -G1^T] [g1]  [0]   [h2]
# [0  0   a*L -G2^T] [g2]  [0]   [h3]
# [L -G1 -G2   0   ] [w]   [0]   [h4]
#
# Neumann boundary
#
# [A  0   0    L    0 0 0] [c] = [d] - [h1]
# [0  a*L 0   -G1^T 1 0 0] [g1]  [0]   [h2]
# [0  0   a*L -G2^T 0 1 0] [g2]  [0]   [h3]
# [L -G1 -G2   0    0 0 1] [w1]  [0]   [h4]
# [0  1   0    0    0 0 0] [w2]  [0]   [0]
# [0  0   1    0    0 0 0] [w3]  [0]   [0]
# [0  0   0    1    0 0 0] [w4]  [0]   [0]
#
###################################
def solve_system_equations(grid, parameter, node_list):
	""" Construc the stiffness matrix """

	# Number of nodes
	no_nodes = len(node_list)

	# Number of data points
	no_data = parameter.get_data_size()

	# For Dirchlet boundary model problem
	if parameter.get_boundary() == BoundaryType.Dirichlet:
	
		# Initialise a sparse grid
		matrix =  lil_matrix((4*no_nodes,4*no_nodes),dtype=float)

		# Initialise a vector
		vector = zeros([4*no_nodes, 1])

		# Initial guess x0 for CG
		ini_guess = zeros([4*no_nodes, 1])
	
	# For Neumann boundary model problem
	elif parameter.get_boundary_type() == BoundaryType.Neumann:

		# Initialise a sparse grid
		matrix =  lil_matrix((4*no_nodes+3,4*no_nodes+3),dtype=float)	

		# Initialise a vector
		vector = zeros([4*no_nodes+3, 1])

		# Initial guess x0 for CG
		ini_guess = zeros([4*no_nodes+3, 1])

	# Add submatrices from the grid connections
	add_submatrix_entries(grid, node_list, matrix, vector, parameter)

	# Add uniqueness for Neumann boundary problems
	if parameter.get_boundary_type() == BoundaryType.Neumann:
		add_uniqueness(matrix, no_nodes)
	"""
	# Obtain submatrices A, L, G1 and G2
	A, L, G1, G2 = get_submatrices_2D(matrix, node_list)
	print A
	print L
	print G1
	print G2
	"""
	# Solve system using the direct solver
	tpsfem_solution = spsolve(csr_matrix(matrix), vector)

	return matrix, vector, tpsfem_solution


def check_dict(dict, node_id):
	""" Check if the given node id is in the dictionary """
	
	# Quit if the dictionary is empty
	if (len(dict) == 0):
		return False

	# Check if the node id is in the dictionary
	return dict.has_key(str(node_id))


def build_node_list(grid, boundary_type):
	""" Build a list of nodes with connected nodes closer to each other """
	
	# The index of checked node for neighouring nodes
	index = 0

	# Initialise a list of nodes
	node_list = []

	# Dictionary of nodes
	node_dict = {}

	# Iterate through nodes in the grid
	for node in node_iterator(grid):

		# Check if the node is interior
		if not check_dict(node_dict,node.get_node_id()):

			# If endpoint is interior or if the boundary is Neumann
			if not grid.get_slave(node.get_node_id()) or boundary_type == BoundaryType.Neumann:

				# Add the node to the dictionary
				node_dict[str(node.get_node_id())] = 0

				# Add the node to the list
				node_list.append(grid.get_node_ref(node.get_node_id()))

		# Iterate through the list
		while index < len(node_list):

			index_node_id = node_list[index].get_node_id()

			# Check every endpoint of the current node
			for endpt_id in endpt_iterator(grid, index_node_id):

				# Check if the neighbour node has been added, interior and 
				# has nonzero connections
				if not check_dict(node_dict,endpt_id) and \
					grid.get_matrix_L_value(index_node_id,endpt_id) != 0:
					
					# If endpoint is interior or if the boundary is Neumann
					if not grid.get_slave(endpt_id) or boundary_type == BoundaryType.Neumann:

						# Add the node to the dictionary
						node_dict[str(endpt_id)] = 0

						# Add the node to the list
						node_list.append(grid.get_node_ref(endpt_id))
			
			# Increase the index count
			index += 1

	return node_list


def solve_tpsfem(grid, region, parameter, if_build=True):
	""" Solve TPSFEM with current grid and update the node values """

	# Boundary type of the problem
	boundary_type = parameter.get_boundary()

	# Calculate matrix value and load vector value for each node
	if if_build:
		build_equation_linear_2D(grid, region, parameter)

	# Build a list of nodes to reduce matrix band
	node_list = build_node_list(grid, boundary_type)

	# Build and solve a system of equations
	matrix, vector, tpsfem_solution = \
		solve_system_equations(grid, parameter, node_list)


	# Print out submatrices
#	output_matrices_2D(matrix, node_list)

	# Length of node list
	number_nodes = len(node_list)
	
	# Smoothing parameter
	alpha = parameter.get_alpha()

	# Update node values in the grid
	for i in range(0,len(node_list)):

		# Set node value
		node_list[i].set_value(tpsfem_solution[i])

		# Set derivative values
		node_list[i].set_d_values([tpsfem_solution[i+number_nodes], \
								   tpsfem_solution[i+2*number_nodes], \
								   tpsfem_solution[i+3*number_nodes]/alpha])
