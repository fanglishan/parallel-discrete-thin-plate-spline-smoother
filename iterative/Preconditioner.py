#######################################################################
# identity_preconditioner
#
# p = I*r. That is, the contents index by r_index are copied into the
# the part of the array indexed by p_index.
#
# The entries in ghost_vector_table are not used. It is listed in the
# set of parameters for consistency.
#
# Input: Grid grid
#      : dictionary ghost_vector_table
#      : dictionary vector_table
#      : int r_index
#      : int p_index
#
# Output: The result is stored in the p_index index of the arrays
# stored in vector_table. The slave node entries are not changed.
#
#######################################################################
def identity_preconditioner(grid, ghost_vector_table, vector_table,
							r_index, p_index,l_matrix=0, u_matrix=0, node_id_list=0):
	"""p = I*r"""

	# Import appropriate information from other classes
	from grid.NodeTable import node_iterator

	# Loop through the list of full nodes
	for node in node_iterator(grid):
		node_id = node.get_node_id()

		# Ignore the slave nodes
		if not node.get_slave():

			# Get the array of values assigned to the current node
			vector = vector_table[str(node_id)]

			# Copy across the entries indexed by r_index into those
			# indexed by p_index
			vector[p_index] = vector[r_index]

			# Update the array of values assigned to the current node
			vector_table[str(node_id)] = vector


def calculate_index(input_list, node_id):
	
	for i in range(0,len(input_list)):
		if input_list[i] == node_id:
			return i

	return


#######################################################################
# DD_init
#
# Find the LU decomposition of the matrix stored in this processor.
#
# DD_init must be called, once, before callingt DD_preconditioner
#
# Input: Grid grid
#
# Output: The LU decomposition is found and stored in a global variable
#
#######################################################################
def DD_init(grid):

	from scipy import eye
	from grid.NodeTable import node_iterator
	from grid.ConnectTable import connect_iterator
	from scipy.linalg import lu
	
	node_id_list = []
	for node in node_iterator(grid):
		if not node.get_slave():
			node_id_list.append(node.get_node_id().get_no())


	# Set the size of the matrix
	count = len(node_id_list)
	
	# Initialise the A matrix
	fem_matrix = eye(count)

	# Define the A matrix and load vector
	for node in node_iterator(grid):

		# Ignore slave (or boundary) nodes
		if not node.get_slave():

			# Calculate the row index for the current node
			row = calculate_index(node_id_list, node.get_node_id().get_no())

			# Set diagonal entry to be 4
			fem_matrix[row,row] = 4

			# Loop over the matrix entries in the current row
			for endpt1 in connect_iterator(grid, node.get_node_id()):

				# Ignore nodes outside this processor
				if grid.is_in(endpt1):

					# Ignore slave (or boundary) nodes
					if not grid.get_slave(endpt1):

						# Calculate the column index for the end node
						column = calculate_index(node_id_list,endpt1.get_no())

						# What is the corresponding matrix value (in the FEM grid)
						stiffness = grid.get_matrix_value(node.get_node_id(), endpt1)

						# Assign the matrix value to the corresponding entry in the matrix
						fem_matrix[row, column] = stiffness

	# compute permutation matrix, lower matrix and upper matrix
	p_matrix, l_matrix, u_matrix = lu(fem_matrix)

	return fem_matrix,l_matrix, u_matrix,node_id_list


#######################################################################
# DD_preconditioner
#
# p = D ^{-1}*r, where D is a block diagonal matrix and block i
# corresponds to the part of the fem matrix stored with worker i.
# DD_init should be called before calling this routine as it finds the
# sparse LU decomposition of the FEM matrix. Nodes should not be
# added or removed from the grid inbetween calling DD_init and
# DD_preconditioner.
#
#
# The entries in ghost_vector_table are not used. It is listed in the
# set of parameters for consistency.
#
# It is assumed that r is the residual vector so the values at the
# slave nodes is zero.
#
# Input : Grid grid
#       : dictionary ghost_vector_table
#       : dictionary vector_table
#       : int r_index
#       : int p_index
#
# Output: The result is stored in the p_index index of the arrays
# stored in vector_table. The slave node entries are not changed.
#
#######################################################################
def DD_preconditioner(grid, ghost_vector_table, vector_table,
					r_index, p_index,l_matrix, u_matrix, node_id_list):
	"""p = I*r"""

	# Import appropriate information from other classes
	from grid.NodeTable import node_iterator
	from scipy.linalg import solve
	from numpy import zeros

	r_vector = zeros(shape=(len(node_id_list),1))

	for node in node_iterator(grid):
		node_id = node.get_node_id()

		for i in range(0,len(node_id_list)):
			if node_id_list[i] == node_id.get_no():

				r_vector[i] = vector_table[str(node_id)][r_index]

	y_vector = solve(l_matrix,r_vector)

	p_vector = solve(u_matrix,y_vector)
	
	# Loop through the list of full nodes
	for node in node_iterator(grid):
		
		node_id = node.get_node_id()

		for i in range(0,len(node_id_list)):
			if node_id_list[i] == node_id.get_no():

				# Get the array of values assigned to the current node
				vector = vector_table[str(node_id)]

				# Copy across the entries indexed by r_index into those
				# indexed by p_index
				vector[p_index] = p_vector[i]

				# Update the array of values assigned to the current node
				vector_table[str(node_id)] = vector