#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-21 14:23:38
@Last Modified time: 2019-04-21 15:19:19

This files contains routines to build a 2D intial grid 
with "pacman" style grid.

"""

# Import other classes
from copy import copy
from numpy import pi, sin, cos
from grid.GlobalNodeID import GlobalNodeID
from function.FunctionStore import zero
from grid.Node import Node
from grid.Edge import Edge, DomainSet, RefineSet
from grid.GlobalNodeID import mesh_integers_2D
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator


def sine_function(angle,angle_sum=6):
	""" 
	Calculate a sine function on input data 
	
	Input: angle - current angle
		   angle_sum - 360 degreen angle, default as 6

	Output: sine function result
	"""
	return sin(2*pi*angle/angle_sum)


def cosine_function(angle,angle_sum=6):
	""" 
	Calculate a cosine function on input data 
	
	Input: angle - current angle
		   angle_sum - 360 degreen angle, default as 6

	Output: cosine function result
	"""
	return cos(2*pi*angle/angle_sum)


def add_edge(grid, id1, id2, refine_type, location, \
		boundary_function = zero):
	""" Add two edges between two given nodes to the grid """

	# Create the first edge from 1 to 2
	edge1 = Edge(id1, id2)

	# Set location for the edge
	edge1.set_location(location)

	# Set refine type for the edge
	edge1.set_refine_type(refine_type)

	# Set the boundary function for the edge
	edge1.set_boundary_function(boundary_function)
	
	# Set refine level as 0 since no refinement yet
	edge1.set_refine_level(0)
	
	# Copy the frist edge with all its information
	edge2 = copy(edge1)

	# Replace start node 1 with node 2
	edge2.set_endpt1(id2)

	# Replace end node 2 with node 1
	edge2.set_endpt2(id1)

	# Add edge 1 to the grid
	grid.add_edge_all(edge1)

	# Add edge 2 to the grid
	grid.add_edge_all(edge2)


def add_pacman_edges(start_angle, end_angle, N_angle, end_length, 
					 N_length, bnd_dirichlet, grid, node_mesh):
	""" Create and  edges to the grid """

	# Set base as the base edge in RefineSet
	base = RefineSet.base_edge

	# Set not_base as not base edge in RefineSet
	not_base = RefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary

	# Get the node id from the first node
	id1 = node_mesh[0].get_node_id()

	# Iterate and add edges from the origin to the first layer
	for i in range(0,N_angle+1):
		
		# Get the node id from the ith node node in the first layer
		id2 = node_mesh[i+1].get_node_id()

		# Check if the edge/node is on the boundary
		if i == 0:
			# Add two non base boundary edges between these two nodes
			add_edge(grid, id1, id2, base, bnd,bnd_dirichlet)
		elif i == N_angle:
			if i%2 == 0:
				add_edge(grid, id1, id2, base, bnd,bnd_dirichlet)
			else:
				add_edge(grid, id1, id2, not_base, bnd,bnd_dirichlet)
		else:
			if i%2 == 0:
				# Add two base iterior edges between these two nodes
				add_edge(grid, id1, id2, base, intp)
			else:
				# Add two base iterior edges between these two nodes
				add_edge(grid, id1, id2, not_base, intp)
	# The number of nodes in the layer
	N_nodes = N_angle+1

	# the index of the node
	node_index = 1

	# Iterate through layers and add edges between neighbouring nodes
	for i in range(0,N_length):
		# Iterate through nodes in each layer
		for j in range(0, N_nodes-1):

			# Get the node id from the right node on the layer
			id1 = node_mesh[node_index].get_node_id()
			
			# Get the node id from the next node
			id2 = node_mesh[node_index+1].get_node_id()
			
			# Check if it is on the outer layer
			if i == N_length-1:
				# Add two non base boundary edges between these two nodes
				add_edge(grid, id1, id2, not_base, bnd, bnd_dirichlet)
			else: 
				# Add two non base interior edges between these two nodes
				add_edge(grid, id1, id2, not_base, intp)

			# Increase the node index count to the next layer
			node_index += 1

		# Increase the node index count to the next layer
		node_index += 1

		# Adjust the size of the layer
		# N_i = 2*N_i-1	
		N_nodes = 2*N_nodes-1

	# Set the start index as 1, as the first node in the first layers
	start_index = 1

	# the the end index as the first node in the second layer
	end_index = N_angle+2

	# Set the number of nodes in the layer
	N_nodes = N_angle+1

	# Iterate through layers and add edges between neighbouring nodes
	for i in range(0,N_length-1):

		# Create a pointer that stores the index of the end nodes of 
		# the outer layer
		pointer = end_index

		# edge count that determine the base edges
		# even ->  base; odd -> not base
		edge_index = 0

		# Iterate through the layer from the start angle to the end
		# angle 
		for j in range(start_index,end_index):

			# Get the node3 id from one node from the first layer
			id1 = node_mesh[j].get_node_id()

			# The first node has two edges to the outer layer
			# The first edge is on the boundary
			if j == start_index:
				
				# Get the node id from one node from the first layer
				id2 = node_mesh[pointer].get_node_id()

				# Add two base interior edges between these two nodes
				add_edge(grid, id1, id2, base, bnd, bnd_dirichlet)

				# Get the node id from the next node from the first layer
				id2 = node_mesh[pointer+1].get_node_id()

				# Add two base interior edges between these two nodes
				add_edge(grid, id1, id2, not_base, intp)
			
				edge_index += 2
			# The last node has two edges to the outer layer
			# The second edge is on the boundary								
			elif j == end_index-1:
				# Get the node id from the last node from the first layer
				id2 = node_mesh[pointer].get_node_id()

				if edge_index%2 == 0:
					# Add two base interior edges between these two nodes
					add_edge(grid, id1, id2, not_base, bnd, bnd_dirichlet)
				else:
					# Add two base interior edges between these two nodes
					add_edge(grid, id1, id2, base, bnd, bnd_dirichlet)

				# Get the node id from the previous node from the first layer
				id2 = node_mesh[pointer-1].get_node_id()

				if edge_index%2 == 0:
					# Add two base interior edges between these two nodes
					add_edge(grid, id1, id2, base, intp)
				else:
					# Add two base interior edges between these two nodes
					add_edge(grid, id1, id2, not_base, intp)
			
			# The nodes in the middle of the layer
			else:
				# Get the node id from the previous node from the first layer
				id2 = node_mesh[pointer-1].get_node_id()
				if edge_index%2 == 0:
					# Add two base iterior edges between these two nodes
					add_edge(grid, id1, id2, base, intp)
				else:
					# Add two base iterior edges between these two nodes
					add_edge(grid, id1, id2, not_base, intp)

				edge_index += 1

				# Get the node id from the previous node from the first layer
				id2 = node_mesh[pointer].get_node_id()
				if edge_index%2 == 0:
					# Add two base iterior edges between these two nodes
					add_edge(grid, id1, id2, base, intp)
				else:
					# Add two base iterior edges between these two nodes
					add_edge(grid, id1, id2, not_base, intp)

				edge_index += 1

				# Get the node id from the previous node from the first layer
				id2 = node_mesh[pointer+1].get_node_id()

				if edge_index%2 == 0:
					# Add two base iterior edges between these two nodes
					add_edge(grid, id1, id2, base, intp)
				else:
					# Add two base iterior edges between these two nodes
					add_edge(grid, id1, id2, not_base, intp)
			
				edge_index += 1

			# Increase the pointer of node index to the next node
			pointer += 2
		
		# The previous end node index is the start index of the next iteration		
		start_index = end_index

		# Adjust the number of nodes in the next layer
		N_nodes = 2*N_nodes-1

		# Update the end index for the last iteration
		end_index += N_nodes


def add_pacman_nodes(start_angle, end_angle, N_angle, end_length, 
					 N_length, bnd_dirichlet, grid, node_mesh):
	""" Create nodes and add them to the pacman grid """

	# Calculate the grid interval
	h = float(end_length)/N_length
	
	# Initialise current radius
	# that defines the coordinates of the nods
	current_radius = h

	# Initialise the first node at the origin of the pacman
	node = Node()

	# Set coordinate at the origin
	coord = [0.0,0.0]

	# Initialise a global node ID
	global_id = GlobalNodeID()

	# set the global id at the origin
	global_id.set_no(mesh_integers_2D(0, 0))

	# Set level as 0 since there is no refinement yet
	global_id.set_level(0)

	# Create a new node in the grid with the global ID and coordinates
	# In default, this node is a slave node and has value 0
	if start_angle == 0 and end_angle == 6:
		# The origin is not a boundary node if the pacman is a circle
		node = grid.create_node(global_id, coord, False, 0.0)
	else:
		# Create a boundary node in the grid with id and coordinates 
		node = grid.create_node(global_id, coord, True, 0.0)
		
	# Add the new node into the grid
	grid.add_node(node)

	# Add the new node into the node meshs
	node_mesh.append(copy(node))

	# Calculate the number of nodes in the next layer
	n = N_angle + 1

	# Iterate through each layer of the pacman
	for i in range(0,N_length):
		# Set current angle as the start angle of the pacman
		current_angle = start_angle

		# Calculate the angle of current node
		single_angle = (end_angle-start_angle)*1.0/(n-1)

		# Iterate through the nodes on this layer for right to left
		# (from start angle to end angle)
		for j in range(0,n):
			# Initialise a new node
			node = Node()

			# Set coordinate of the node, whose x and y coordinates
			# are obtained by radius times a sine or cosine function
			coord = [current_radius*cosine_function(current_angle),\
					current_radius*sine_function(current_angle)]

			# Initialise a global node ID
			global_id = GlobalNodeID()

			# set the global id at the origin
			global_id.set_no(mesh_integers_2D(i+1, j))

			# Set level as 0 since there is no refinement yet
			global_id.set_level(0)

			# Check if the nodes are on the arc of the pacman
			if i == N_length-1:
				# Create a boundary node in the grid with id and coordinates
				node = grid.create_node(global_id, coord, True, 0.0)
			# This node is not at the boundary if the pacman is a circle
			elif start_angle == 0 and end_angle == 6:
				# Create an interior node in the grid with id and coordinates
				node = grid.create_node(global_id, coord, False, 0.0)
			# Check if the nodes are on the radius of the pacman
			elif j == 0 or j == n-1:
				# Create a boundary node in the grid with id and coordinates
				node = grid.create_node(global_id, coord, True, 0.0)
			else:
				# Create an interior node in the grid with id and coordinates
				node = grid.create_node(global_id, coord, False, 0.0)

			# Add the new node into the grid
			grid.add_node(node)

			# Add the new node into the node meshs
			node_mesh.append(copy(node))

			# increase the angle for the next node of current layer
			current_angle += single_angle

		# Calculate the number of nodes in the next layer
		n = 2*n-1

		# Increase the radius for the next layer of the pacman
		current_radius += h


def add_pacman_connections(grid, node_mesh):
    """ Add connections between square nodes """
    
    # Loop over the nodes in the grid
    for node in node_iterator(grid):

        # Add connection to itself
        grid.add_connection(node.get_node_id(),node.get_node_id(),-1)

        # Loop over all of the endpoints of an edge joined to the
        # current node
        for endpt in endpt_iterator(grid, node.get_node_id()):

            # Add a connection between two nodes
            grid.add_connection(node.get_node_id(),endpt,-1)


def build_pacman_grid(grid, start_angle, end_angle, N_angle, \
					end_length, N_length, bnd_dirichlet):

	# The start angle of the pacman should be larger than or equal to 0
	assert start_angle >= 0, "the start angle must be >= 0"

	# The end angle of the pacman should be smaller than or equal to 6
	assert end_angle <= 6, "the end angle must be <= 6"

	# The end angle of the pacman should be larger than the start angle
	assert end_angle >= start_angle, \
					"the end angle must be larger than the start angle"

	# The number of angles should be larger than 1
	assert N_angle > 1, "the grid size must be > 1"
	
	# The radius of the pacman should be larger than 0
	assert end_length > 0, "the radius length must be > 0"

	# The number of grids on the boundary should be larger than 2
	assert N_length > 2, "the grid size must be > 2"

	# a list of nodes in the grid
	node_mesh = []

	add_pacman_nodes(start_angle, end_angle, N_angle, end_length, N_length,
				  bnd_dirichlet, grid, node_mesh)


	add_pacman_edges(start_angle, end_angle, N_angle, end_length, N_length,
				  bnd_dirichlet, grid, node_mesh)

	add_pacman_connections(grid, node_mesh)