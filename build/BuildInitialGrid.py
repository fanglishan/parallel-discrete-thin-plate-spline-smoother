#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-21 14:23:38
@Last Modified time: 2019-04-21 15:34:20

This class contains routines to build an initial grid.

"""

# Import other classes
from grid.Grid import Grid
from build.BuildLine import build_line_grid
from build.BuildSquare import build_square_grid
from build.BuildPacman import build_pacman_grid
from build.BuildLShaped import build_L_shaped_grid


def build_initial_grid(n, u, grid_type):
	""" Build an initial grid """
	
	grid = Grid()

	if grid_type == 1:
		build_line_grid(n, grid, u)

	elif grid_type == 2:
		build_square_grid(n, grid, u)

	elif grid_type == 3:	
		# starting angle of the pacman, minimum 0
		start_angle = 0.0

		# ending angle of the pacman, maximum 6
		end_angle = 4.5

		# number of angles of pacman
		N_angle = 5

		# radius of the pacman [0,1]
		end_length = 1

		# number of sections
		N_length = 3

		# Create a square FEM grid of size n*n
		build_pacman_grid(grid, start_angle, end_angle, N_angle, \
			end_length, N_length, u)

	elif grid_type ==4:
		pass

	else:
		print "Warning: invalid grid type"

	return grid