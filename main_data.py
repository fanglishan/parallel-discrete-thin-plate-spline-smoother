#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 17:10:41
@Last Modified time: 2019-04-17 04:12:39

This class creates datasets and store them in the data folder.

Dataset name
1:dimension
2:model problem
3:distribution
4:noise level
5:data size

"""

# Import other classes
from file.DataFile import read_file, write_file
from plot.DataPlot import plot_data 
from data.DatasetManager import obtain_dataset, set_model, set_noise, set_size
from data.BuildData2D import create_uniform_data


def plot_dataset(dataset):
	""" Plot the dataset"""

	data = obtain_dataset(dataset)
	
	print "Plot", len(data), "data points"
	
	plot_data(data)



def build_dataset(dataset):
	""" Build a dataset """

	# Number of data points (in each direction)
	n = set_size(dataset)

	# Model problem
	u = set_model(dataset)

	# Noise size
	noise = set_noise(dataset)

	# Outliers
	outlier_no = 0
	outlier_size = 0.0

	data = create_uniform_data(n, u, noise, outlier_no, outlier_size)

	write_file(data, str(dataset))


#Dataset name
#1:dimension
#2:model problem
#3:distribution
#4:noise level
#5:data size

# Model problem
#zero=0, linear=1, quadratic=2, cubic=3, exp=4
#sine=5, osci_sine=6, steep_exp=7

# Noise level
#0-0.0, 1-0.01, 2-0.03, 3-0.05, 4-0.07, 5-0.1

# Data size
#1-20, 2-50, 3-100, 4-250, 5-500, 6-1000

dataset = 25101


build_dataset(dataset)

plot_dataset(dataset)
