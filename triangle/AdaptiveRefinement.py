#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 13:44:36

This files contains the refinement routines for a triangular grid. The
main routine of interest is uniform_refinement, the rest are helper
routines.

"""

# Import other classes
from grid.GlobalNodeID import GlobalNodeID
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.ConnectTable import connect_iterator
from grid.Edge import DomainSet, RefineSet
from grid.NghNodes import NghNodes, ngh_iterator
from grid.Grid import Grid
from indicator.ErrorIndicator import indicate_error,indicate_edge_error
from commun.RefinementCommun import update_refine_edge_list, communicate_refine_edge_list
from commun.IndicatorCommun import update_error_indicators
from grid.GridFunctions import find_copy_node, find_node, find_global_id, \
			check_node_in_grid, check_edge_nodes_in_grid, check_edge_nodes_ghost
from commun.SynchronisationFunctions import get_global_keep_refine, find_population_table
import plot.FemPlot2D as femPlot2D

# Import libraries
from copy import deepcopy


class NodeSet:
	""" Full node or ghost node """
	full_node = 1
	ghost_node = 2


def trim_ghost_nodes(grid):
	""" Remove extra ghost nodes """
	
	# Find the ghost neighbour node table
	ghost_commun = grid.reference_ghost_commun()

	# Find the ghost table
	ghost_table = grid.reference_ghost_table()
	
	# Keep a record of which nodes must be removed, as well as any
	# connections or edges joined to that node
	remove_nodes = Grid()
	
	# Loop over the ghost nodes
	for node in node_iterator(ghost_table):
		node_id = node.get_node_id()
		
		# Initially assume the ghost node should be removed
		remove = True
		
		# Check to see if the ghost node is joined to a full node
		for endpt in connect_iterator(grid, node_id):
			if grid.is_in(endpt):
				
				# If it is joined to a full node then don't remove it
				remove = False
				break    
				
				
		# If the ghost node is to be removed, get a copy of the extra
		# information that must be removed from the grid
		if remove:
			
			# Note that the node should be removed
			remove_nodes.add_node(node)
			
			# Any connections joined to the node should be removed
			for endpt in connect_iterator(grid, node_id):
				remove_nodes.add_connection(node_id, endpt)
				
			# Any edge joined to the node should be removed
			for endpt in endpt_iterator(grid, node_id):
				remove_nodes.add_edge(node_id, endpt)

	# Now remove the nodes
	
	# Loop over the nodes to be removed
	for node in node_iterator(remove_nodes):
		node_id = node.get_node_id()
 
		# Any connection to or from the node must be removed
		# (being careful that the connection may have already been removed
		# when another node was removed from the grid)
		for endpt in connect_iterator(remove_nodes, node_id):
			if grid.is_connection(node_id, endpt):
				grid.delete_connection(node_id, endpt)
			if grid.is_connection(endpt, node_id):
				grid.delete_connection(endpt, node_id)
				
		# Any edge to or from the node must be removed
		# (being careful that the edge may have already been removed
		# when another node was removed from the grid)
		for endpt in endpt_iterator(remove_nodes, node_id):
			if grid.is_edge(node_id, endpt):
				grid.delete_edge(node_id, endpt)
			if grid.is_edge(endpt, node_id):
				grid.delete_edge(endpt, node_id)
				
		# Remove the node from the ghost node table
		ghost_table.delete_node(node_id)
		
		# Remove the node from the neighbour node list
		
		# Find which worker has the full node copy
		for worker, id_list in ngh_iterator(ghost_commun):
			if node_id in id_list:
				full_worker_no = int(worker)
				break
				
		# Remove the node from that neighbour worker information    
		ghost_commun.delete_neighbour_node(full_worker_no, node_id)
			

def trim_full_nodes(grid):
	""" Check to see if updates still need to be sent to neighbouring nodes """
	
	# Get the full neighbour node table
	full_commun = grid.reference_full_commun()
	
	# Get the ghost neighbour node table
	ghost_commun = grid.reference_ghost_commun()

	# Keep a record of the information that needs to be removed
	remove_nodes = NghNodes()
	
	# Loop over the neighbour workers
	for worker, id_list in ngh_iterator(full_commun):
		
		# Loop over the list of full nodes that should send updated information
		# to the neighbouring worker
		for node_id in id_list:
			
			# Assume the information should be removed
			remove = True
			
			# If the ghost node copy of the current node in the neighbour worker
			# is connected to some full node, then do not remove it from the
			# list 
			for endpt in connect_iterator(grid, node_id):
				if ghost_commun.is_neighbour_node(int(worker), endpt):
					remove = False
					break
					
			# If the node should be removed, record the necessary information
			if remove:
				remove_nodes.add_neighbour_node(int(worker), node_id)
				
	# Loop over the neighbouring workers
	for worker, id_list in ngh_iterator(remove_nodes):
		
		# Loop over the list of nodes to be removed from the communication
		# pattern
		for node_id in id_list:
			
			# Remove those nodes from the full neighbour node table
			full_commun.delete_neighbour_node(int(worker), node_id)
			

def trim_nodes(grid):
	""" Remove unwanted ghost nodes and update the communication pattern """
	
	# Remove any unwanted ghost notes
	trim_ghost_nodes(grid)
	
	# Adjust the communication patter to take into account that some
	# ghost nodes may have been removed from neighbouring workers
	trim_full_nodes(grid)
	

def get_full_worker_no(my_worker_no, grid, node_id):
	""" Find which processor contains the full node copy of the given node """
	
	# Find the ghost neighbour node table 
	ghost_commun = grid.reference_ghost_commun()

	# Check to see if the node is a full node in the current grid
	if grid.is_in(node_id):
		return my_worker_no
		
	# If the node is not a full node, it must be a ghost node and 
	# thus have an entry in the ghost neighbour node table
	
	# Use the ghost neighbour node table to find which neighbouring worker
	# contains a full copy of the node
	worker_found = False
	for worker, id_list in ngh_iterator(ghost_commun):
		if node_id in id_list:
			id_worker = int(worker)
			worker_found = True
			break
			
	# If no neighbour worker was found then something went wrong
	assert worker_found, 'worker number not found for node '+str(node_id)
	
	# Return the neighbour worker id
	return id_worker
	

def add_to_ngh_nodes(my_worker_no, grid, full_worker_no, node):
	""" Add the new node to the communication pattern """

	# Get the ghost table
	ghost_table = grid.reference_ghost_table()
		   
	# Get the full neighbour node table
	full_commun = grid.reference_full_commun()
	
	# Get the ghost neighbour node table
	ghost_commun = grid.reference_ghost_commun()    
	
	# Find the node id
	node_id = node.get_node_id()
	
	# If the node is a ghost node, simply record the position of the
	# corresponding full node
	if ghost_table.is_in(node_id):
		ghost_commun.add_neighbour_node(full_worker_no, node_id)        
	else:
		
		# If the node is a full node, find all of the neighbouring processors
		# that have a ghost node copy
		for endpt in connect_iterator(grid, node_id):

			if ghost_table.is_in(endpt):

				full_worker_no = get_full_worker_no(my_worker_no, grid, endpt)

				if not full_commun.is_neighbour_node(full_worker_no, node_id):

					full_commun.add_neighbour_node(full_worker_no, node_id)


def find_worker_no(my_worker_no, population_table, grid, node1, node2):
	""" Find which worker should get the full copy of the midpoint """
	
	# Get a copy of the ghost node table
	ghost_table = grid.reference_ghost_table()
		   
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	global_id1 = node1.get_global_id()
	global_id2 = node2.get_global_id()
	
	# Find which worker has the full node copy of node1 
	if ghost_table.is_in(node_id1):
		id1_worker = get_full_worker_no(my_worker_no, grid, node_id1)
	else:
		id1_worker = my_worker_no
   
	# Find which worker has the full node copy of node2   
	if ghost_table.is_in(node_id2):
		id2_worker = get_full_worker_no(my_worker_no, grid, node_id2)
	else:
		id2_worker = my_worker_no

	# If one worker contains the full node copy of the node1 and node2, then
	# the midpoint should be added as a full node copy to that worker
	if id1_worker == id2_worker:
		return id1_worker
	
	# Now suppose different workers contain the full node copies of the 
	# endpotins. 
	# The midpoint should be added as a full node copy to one of those workers, 
	# but which one
	
	# Try to add the full node copy of the midpoint to a worker with the 
	# smallest number of full nodes
	if population_table[id1_worker] < population_table[id2_worker]:
		return id1_worker
	if population_table[id1_worker] > population_table[id2_worker]:
		return id2_worker

	# If both workers contain the same number of full nodes, then break the 
	# deadlock by using the global ids    
	if global_id1 < global_id2:
		return id1_worker
	else:
		return id2_worker
		
	
def build_midpoint_global_id(grid, global_id1, global_id2):
	"""Find the global id of the new midpoint""" 
	
	# Import appropriate information from other classes
	from grid.GlobalNodeID import mesh_integers_2D, unmesh_integers_2D
	
	# The node is being added a what level of refinement
	current_level = 0
	if global_id1.get_level() > global_id2.get_level():
		current_level = global_id1.get_level()+1
	else:
		current_level = global_id2.get_level()+1
		
	# The number must be scaled according to the level of refinement
	scale1 = 2**(current_level-global_id1.get_level()-1)
	scale2 = 2**(current_level-global_id2.get_level()-1)
		
	# Extract the coordinates that were used to find the ids of the endpoints 
	id1x, id1y = unmesh_integers_2D(global_id1.get_no())
	id2x, id2y = unmesh_integers_2D(global_id2.get_no())
		
	# Find the coordinates of the midpoint
	idx = id1x*scale1+id2x*scale2
	idy = id1y*scale1+id2y*scale2
		
	# Create a new global id
	id = mesh_integers_2D(idx, idy)
	global_id = GlobalNodeID(id, current_level)

	# Return the global id
	return global_id
	
	
def add_midpoint(grid, node1, node2, node_type):
	"""Add the midpoint to the grid""" 
	
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
		
	# Find the global id of the midpoint
	mid_global_id = build_midpoint_global_id(grid, node1.get_global_id(),
											 node2.get_global_id())

	# Determine if the midpoint will sit on the boundary                                         
	location = grid.get_location(node_id1, node_id2)
	bnd_func = grid.get_boundary_function(node_id1, node_id2)

	# If the midpoint is to be added as a full node
	if node_type == NodeSet.full_node:
		
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a full slave node
			value = bnd_func(coord)
			mid_node = grid.create_node(mid_global_id, coord, True, value)
			grid.add_node(mid_node)
				
		else:
			
			# Add the midpoint as a full node (and initialise the value and
			# load to be the average of the endpoints)
			value = (node1.get_value() + node2.get_value())/2.0
			load = (node1.get_load() + node2.get_load())/2.0
			mid_node = grid.create_node(mid_global_id, coord, False, value)
			mid_node.set_load(load)
			grid.add_node(mid_node)
			
	# If the midpoint is a ghost node
	else:
		ghost_table = grid.reference_ghost_table()
		
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a ghost slave node
			value = bnd_func(coord)
			mid_node = ghost_table.create_node(mid_global_id, coord, True, 
											   value)
			ghost_table.add_node(mid_node)
				
		else:
			
			# Add the midpoint as a ghost node (and initialise the value and
			# load to be the average of the endpoints)
			value = (node1.get_value() + node2.get_value())/2.0
			load = (node1.get_load() + node2.get_load())/2.0
			mid_node = ghost_table.create_node(mid_global_id, coord, False, value)
			mid_node.set_load(load)
			ghost_table.add_node(mid_node)

	return mid_node
	

def split_edge(grid, node1, node2, node_type, refine_edges):
	"""Split the edge between node1 and node2""" 
	
	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the edge location 
	location = grid.get_location(node_id1, node_id2)
	boundary_function = grid.get_boundary_function(node_id1, node_id2)

	# Find the refinement level
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Find the refine type of the edge
	refine_type = grid.get_refine_type(node_id1, node_id2)

	# Add the midpoint to the grid
	mid_node = add_midpoint(grid, node1, node2, node_type) 
	mid_id = mid_node.get_node_id()

	# Remove the old edges
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)
	
	# Add a new edge between the midpoint and node1
	grid.add_edge(mid_id, node_id1)
	grid.set_location(mid_id, node_id1, location)
	grid.set_boundary_function(mid_id, node_id1, boundary_function)
	grid.set_refine_type(mid_id, node_id1, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id1, refine_level)

	# Add a new edge between the node1 and midpoint
	grid.add_edge(node_id1, mid_id)
	grid.set_location(node_id1, mid_id, location)
	grid.set_boundary_function(node_id1, mid_id, boundary_function)
	grid.set_refine_type(node_id1, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id1, mid_id, refine_level) 

	# Add a new edge between the midpoint and node2
	grid.add_edge(mid_id, node_id2)
	grid.set_location(mid_id, node_id2, location)
	grid.set_boundary_function(mid_id, node_id2, boundary_function)
	grid.set_refine_type(mid_id, node_id2, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id2, refine_level)

	# Add a new edge between the node2 and midpoint
	grid.add_edge(node_id2, mid_id)
	grid.set_location(node_id2, mid_id, location)
	grid.set_boundary_function(node_id2, mid_id, boundary_function)
	grid.set_refine_type(node_id2, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id2, mid_id, refine_level)

	# Update the list of refined edges
	update_refine_edge_list(grid, node1, node2, refine_edges, refine_type)

	# Return the midpoint
	return mid_node
	 

def set_base_type(grid, node_id1, node_id2):
	
	if grid.get_location(node_id1, node_id2) != DomainSet.interior:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)
	elif grid.get_refine_type(node_id1, node_id2) == RefineSet.not_base_edge:
		grid.set_refine_type(node_id1, node_id2, RefineSet.interface_base_edge)
	else:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)


def add_triangle4(my_worker_no, population_table, grid, 
				  node_id1, node_id2, node_id3, node_id4,
				  true_soln, rhs,refine_edges,error_indicator_type):
	""" Refine two triangles along the given base edge """ 
	
	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)
	else:
		node1 = ghost_table.get_node(node_id1)
	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
	else:
		node2 = ghost_table.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Which worker should get the full copy of the new node
	full_worker_no = find_worker_no(my_worker_no, population_table, grid, 
								node1, node2)
	if full_worker_no == my_worker_no:
		node_type = NodeSet.full_node
	else:
		node_type = NodeSet.ghost_node
		
	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type,refine_edges)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)
	
	# Add the edges going from the midpoint to node_id4
	grid.add_edge(mid_id, node_id4)
	grid.set_location(mid_id, node_id4, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id4, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id4, refine_level)

	grid.add_edge(node_id4, mid_id)
	grid.set_location(node_id4, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id4, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id4, mid_id, refine_level)
	
	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)
		
	# Add connections between the midpoint and four vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(mid_id, node_id4)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)
	grid.add_connection(node_id4, mid_id)   
	
	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level)
	grid.set_refine_level(node_id1, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id1, refine_level)    
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)
	grid.set_refine_level(node_id2, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id2, refine_level)    

	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1)
	set_base_type(grid, node_id1, node_id4)
	set_base_type(grid, node_id4, node_id1)    
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)
	set_base_type(grid, node_id2, node_id4)
	set_base_type(grid, node_id4, node_id2)  

	# Initialise the error indicator 
	# (used for adaptive refinement)

	error_indicator = -1.0

	if grid.get_refine_type(node_id1,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id1,node_id3) == RefineSet.base_edge:

		#error_indicator = indicate_true_edge_error(grid,grid.get_edge(node_id1,node_id3),true_soln)
		error_indicator = indicate_edge_error(grid, grid.get_edge(node_id1,node_id3),true_soln,rhs,error_indicator_type)

		grid.set_error_indicator(node_id1, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id1, error_indicator)

	if grid.get_refine_type(node_id1,node_id4) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id1,node_id4) == RefineSet.base_edge:

		#error_indicator = indicate_true_edge_error(grid,grid.get_edge(node_id1,node_id4),true_soln)
		error_indicator = indicate_edge_error(grid, grid.get_edge(node_id1,node_id4),true_soln, rhs,error_indicator_type)

		grid.set_error_indicator(node_id1, node_id4, error_indicator)
		grid.set_error_indicator(node_id4, node_id1, error_indicator)

	if grid.get_refine_type(node_id2,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id2,node_id3) == RefineSet.base_edge:

		#error_indicator = indicate_true_edge_error(grid,grid.get_edge(node_id2,node_id3),true_soln)
		error_indicator = indicate_edge_error(grid, grid.get_edge(node_id2,node_id3),true_soln, rhs,error_indicator_type)

		grid.set_error_indicator(node_id2, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id2, error_indicator)

	if grid.get_refine_type(node_id2,node_id4) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id2,node_id4) == RefineSet.base_edge:

		#error_indicator = indicate_true_edge_error(grid,grid.get_edge(node_id2,node_id4),true_soln)
		error_indicator = indicate_edge_error(grid, grid.get_edge(node_id2,node_id4),true_soln, rhs,error_indicator_type)

		grid.set_error_indicator(node_id2, node_id4, error_indicator)
		grid.set_error_indicator(node_id4, node_id2, error_indicator)    
 
	# Update the communication pattern
	add_to_ngh_nodes(my_worker_no, grid, full_worker_no, mid_node)
   

 	return mid_id

 
 def add_triangle3(my_worker_no, population_table, grid, node_id1, node_id2, 
				  node_id3,true_soln, rhs,refine_edges,error_indicator_type):
	""" Refine a triangle along the given base edge """ 
	
	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()
	
	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)
	else:
		node1 = ghost_table.get_node(node_id1)
	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
	else:
		node2 = ghost_table.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Which worker should get the full copy of the new node
	full_worker_no = find_worker_no(my_worker_no, population_table, grid, 
								node1, node2)
	if full_worker_no == my_worker_no:
		node_type = NodeSet.full_node
	else:
		node_type = NodeSet.ghost_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type,refine_edges)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and three vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)

	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level) 
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)

	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1) 
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)

	# Initialise the error indicator 
	# (used for adaptive refinement)
	error_indicator = -1.0

	if grid.get_refine_type(node_id1,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id1,node_id3) == RefineSet.base_edge:

		#error_indicator = indicate_true_edge_error(grid,grid.get_edge(node_id1,node_id3),true_soln)
		error_indicator = indicate_edge_error(grid, grid.get_edge(node_id1,node_id3),true_soln, rhs,error_indicator_type)

		grid.set_error_indicator(node_id1, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id1, error_indicator)

	if grid.get_refine_type(node_id2,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id2,node_id3) == RefineSet.base_edge:

		#error_indicator = indicate_true_edge_error(grid,grid.get_edge(node_id2,node_id3),true_soln)
		error_indicator = indicate_edge_error(grid, grid.get_edge(node_id2,node_id3),true_soln, rhs,error_indicator_type)

		grid.set_error_indicator(node_id2, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id2, error_indicator)  
	
	# Update the communication pattern
	add_to_ngh_nodes(my_worker_no, grid, full_worker_no, mid_node)


	return mid_id


def add_triangle2(my_worker_no, population_table, grid, node_id1, node_id2,
				  true_soln, rhs,refine_edges,error_indicator_type):
	
	"""Refine a triangle along the given base edge""" 

	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)
	else:
		node1 = ghost_table.get_node(node_id1)
	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
	else:
		node2 = ghost_table.get_node(node_id2)

	# Which worker should get the full copy of the new node
	full_worker_no = find_worker_no(my_worker_no, population_table, grid, 
								node1, node2)
	if full_worker_no == my_worker_no:
		node_type = NodeSet.full_node
	else:
		node_type = NodeSet.ghost_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type,refine_edges)
	mid_id = mid_node.get_node_id()

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and two vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)

	# Update the communication pattern
	add_to_ngh_nodes(my_worker_no, grid, full_worker_no, mid_node)

	return mid_id


def find_coarse_triangle(grid,endpt1,endpt2,endpt3,endpt4):
	""" Find the triangle with lower refinement level """

	# Initialise refinement level for triangle 3 and 4
	refine_level_3 = 0
	refine_level_4 = 0

	# Find the refinement level for triangle 3
	if grid.get_refine_type(endpt1,endpt3) == RefineSet.base_edge:
		refine_level_3 = grid.get_refine_level(endpt1,endpt3)

	elif grid.get_refine_type(endpt2,endpt3) == RefineSet.base_edge:
		refine_level_3 = grid.get_refine_level(endpt2,endpt3)

	elif grid.get_refine_type(endpt1,endpt3) == RefineSet.interface_base_edge:
		refine_level_3 = grid.get_refine_level(endpt1,endpt3)

	elif grid.get_refine_type(endpt2,endpt3) == RefineSet.interface_base_edge:
		refine_level_3 = grid.get_refine_level(endpt2,endpt3)
	else:
		# Set refinement level from the interface base edge if no other base edges are found
		refine_level_3 = grid.get_refine_level(endpt1,endpt2)

	# Find the refinement level for triangle 4
	if grid.get_refine_type(endpt1,endpt4) == RefineSet.base_edge:
		refine_level_4 = grid.get_refine_level(endpt1,endpt4)

	elif grid.get_refine_type(endpt2,endpt4) == RefineSet.base_edge:
		refine_level_4 = grid.get_refine_level(endpt2,endpt4)

	elif grid.get_refine_type(endpt1,endpt4) == RefineSet.interface_base_edge:
		refine_level_4 = grid.get_refine_level(endpt1,endpt4)

	elif grid.get_refine_type(endpt2,endpt4) == RefineSet.interface_base_edge:
		refine_level_4 = grid.get_refine_level(endpt2,endpt4)
	else:
		# Set refinement level from the interface base edge if no other base edges are found
		refine_level_4 = grid.get_refine_level(endpt1,endpt2)
	

	# Print error message if refinement level between 
	# two neighouring triangles differs larger than 2
	if abs(refine_level_3-refine_level_4) > 2:
		print "refine difference between triangles larger than 2"

	# Assign the triangle with lower and higher refinement levels to the references
	if refine_level_3 < refine_level_4:
		endpt_low = endpt3
		endpt_high = endpt4
	elif refine_level_3 > refine_level_4:
		endpt_low = endpt4
		endpt_high = endpt3
	else:
		# Return if two sides of the interface base edges have the same refinement level
		print "refinement level is the same"
		return

	return endpt_low, endpt_high


def refine_coarse_triangle(my_worker_no, population_table, grid,endpt1,endpt2,endpt_low,
	                       true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges):
	""" Refine coarse triangle for interface base edge """

	# Find the base edge of low refinement level triangle
	if grid.get_refine_type(endpt1,endpt_low) == RefineSet.base_edge:

		# Split the base edge if it ends at end point 1
		mid_node_id = split_triangle(my_worker_no, population_table, grid, 
			           grid.get_edge(endpt1,endpt_low),true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges)

	elif grid.get_refine_type(endpt2,endpt_low) == RefineSet.base_edge:

		# Split the base edge if it ends at end point 2
		mid_node_id = split_triangle(my_worker_no, population_table,grid, 
			          grid.get_edge(endpt2,endpt_low),true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges)

	elif grid.get_refine_type(endpt1,endpt_low) == RefineSet.interface_base_edge and \
		grid.get_refine_type(endpt2,endpt_low) == RefineSet.interface_base_edge:

		# Compare the refinement level of two interface base edges
		if grid.get_refine_level(endpt2,endpt_low) > grid.get_refine_level(endpt1,endpt_low):

			# Refine the edge with lower refinement level
			mid_node_id = split_triangle(my_worker_no, population_table,grid, 
				            grid.get_edge(endpt1,endpt_low),true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges)

		elif grid.get_refine_level(endpt2,endpt_low) < grid.get_refine_level(endpt1,endpt_low):

			# Refine the edge with lower refinement level
			mid_node_id = split_triangle(my_worker_no, population_table,grid, 
				          grid.get_edge(endpt2,endpt_low),true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges)
		else:
			print "two interface base edges have the same refine level"

	elif grid.get_refine_type(endpt1,endpt_low) == RefineSet.interface_base_edge:

		# Split the base edge if it ends at end point 1
		mid_node_id = split_triangle(my_worker_no, population_table,grid, grid.get_edge(endpt1,endpt_low),
							true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges)

	elif grid.get_refine_type(endpt2,endpt_low) == RefineSet.interface_base_edge:
	
		# Split the base edge if it ends at end point 2
		mid_node_id = split_triangle(my_worker_no, population_table,grid, 
			           grid.get_edge(endpt2,endpt_low),true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges)

	else:
		# Return no base edge is found in this triangle
		print "no base edge found in worker",my_worker_no

		return

	return mid_node_id


def split_triangle(my_worker_no, population_table, grid, edge,
	 			   true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges):
	""" Add all of the nodes in a square grid""" 
	
	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)
		
	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			# See if we can find a second, different, triangle
			
			# Indicate whether the second triangle has been found
			second_triangle_found = False

			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):

					# Indicate the second triangle has been found
					second_triangle_found = True

					# Check if the edge is an interface base edge
					if grid.get_refine_type(endpt1,endpt2) == RefineSet.interface_base_edge:
						
						# Find the coarse and fine triangles out of two near triangles
						endpt_low, endpt_high = find_coarse_triangle(grid,endpt1,endpt2,edgestar[i],edgestar[j])

						# Refine the coarse triangle
						mid_node_id = refine_coarse_triangle(my_worker_no, population_table, grid, endpt1, endpt2, 
															endpt_low,true_soln, rhs,refine_edges,
															error_indicator_type,unrefined_edges)


						# Check if the coarse triangle refineement is successful
						if mid_node_id is None:

							# If unsuccessful, add the edge to the list
							# to be refined latter
							unrefined_edges.append(edge)

							return

						# Refine the interface base edge
						mid_id = add_triangle4(my_worker_no, population_table, grid, endpt1, 
								               endpt2, mid_node_id, endpt_high, 
								               true_soln, rhs,refine_edges,error_indicator_type)
						return mid_id
					elif grid.get_refine_type(endpt1,endpt2) == RefineSet.base_edge:
						
						# Refine those two triangles
						mid_id = add_triangle4(my_worker_no, population_table, grid, endpt1, 
								           endpt2, edgestar[i], edgestar[j], 
								           true_soln, rhs,refine_edges,error_indicator_type)
					

						return mid_id
			
			# If the edge is an interface base edge and only one triangle near it
			if not second_triangle_found and \
				grid.get_refine_type(endpt1,endpt2) == RefineSet.interface_base_edge:

				return
		
			# If only one triangle was found, refine that triangle
			mid_id = add_triangle3(my_worker_no, population_table, grid, 
						  endpt1, endpt2, edgestar[i],
						  true_soln, rhs,refine_edges,error_indicator_type)
		
			return mid_id
	
	# Sometime in the parallel implementation no triangle is found, so refine
	# the edge
	mid_id = add_triangle2(my_worker_no, population_table, grid, endpt1, endpt2,
							true_soln, rhs,refine_edges,error_indicator_type)

	return mid_id


def build_base_edges(grid):
	"""Build a list of base edges"""

	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()

	# Initialise the list
	base_edges = list()
	
	# Loop over the full nodes
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the full node
		for endpt in endpt_iterator(grid, node_id):
			
			# We only want to find each edge once
			if node_id < endpt:

				# Get the refinement type
				refine_type = grid.get_refine_type(node_id, endpt)

				# If the refinement type is a base type add the edge to the list
				if refine_type == RefineSet.base_edge\
				or refine_type == RefineSet.interface_base_edge:

					base_edges.append(grid.get_edge(node_id, endpt))
	

	# Loop over the ghost nodes
	for node in node_iterator(ghost_table):
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the ghost node
		for endpt in endpt_iterator(grid, node_id):

			# Get the refinement type
			refine_type = grid.get_refine_type(node_id, endpt)
			
			# We only want to find each edge once
			if node_id < endpt:
				
				# If the refinement type is a base type add the edge to the list
				# (In the parallel implementation an edge between two ghost 
				# nodes may only be listed as an interface_base_edge, we should
				# also take those as a base edge)
				if refine_type == RefineSet.base_edge\
				or refine_type == RefineSet.interface_base_edge:

					base_edges.append(grid.get_edge(node_id, endpt))
		
	# Return the list of base edges
	return base_edges


def refine_receive_edges(commun, population_table,grid, received_refine_edges,refine_edges, 
			             true_soln,rhs,error_indicator_type,unrefined_edges):
	"""" Communicate with the other processors and exchange refined edges """

	# Initialise and refinement count
	refine_count = 0

	# Iterate throught the received edges
	for key,node_pair_list in received_refine_edges.iteritems():
		
		# Iterate through a list from a processor (key)
		for i in range(0,len(node_pair_list)):

			# Check if node 1 of the edge is in the full node table
			if grid.is_in_global(node_pair_list[i][0]):
				
				# Obtain node id of node 1
				node_id1 = grid.get_local_id(node_pair_list[i][0])

			# Check if node 1 of the edge is in the ghost node table
			elif grid.reference_ghost_table().is_in_global(node_pair_list[i][0]):
				
				# Obtain node id of node 1
				node_id1 = grid.reference_ghost_table().get_local_id(node_pair_list[i][0])
			else:

				# If one node is not found, ignore this edge 
				continue

			# Check if node 2 of the edge is in the full node table
			if grid.is_in_global(node_pair_list[i][1]):
				
				# Obtain node id of node 2
				node_id2 = grid.get_local_id(node_pair_list[i][1])
			
			# Check if node 2 of the edge is in the ghost node table
			elif grid.reference_ghost_table().is_in_global(node_pair_list[i][1]):
				
				# Obtain node id of node 2
				node_id2 = grid.reference_ghost_table().get_local_id(node_pair_list[i][1])
			else:

				# If one node is not found, ignore this edge
				continue


			# Check if the edge has been refined
			if grid.is_edge(node_id1,node_id2):

				# Record original refine type
				original_refine_type = grid.get_refine_type(node_id1,node_id2)

				# If the refine type is not a base edge type
				if original_refine_type != 1:

					# Set refine type as the one from other processors
					grid.set_refine_type(node_id1,node_id2,node_pair_list[i][2])

				# If not (still an edge), refine the edge
				mid_id = split_triangle(commun.get_my_no(), population_table, 
							   grid, grid.get_edge(node_id1,node_id2),true_soln,rhs,
							   refine_edges,error_indicator_type,unrefined_edges)

				if mid_id is None:
					grid.set_refine_type(node_id1,node_id2,original_refine_type)

				# Increase the refinement count
				refine_count += 1

	return refine_count


def adaptive_refinement(commun, origin_grid, m,error_tol,true_soln,rhs,error_indicator_type):
	"""Implement adaptive refinement"""
	
	# Make a copy of the current grid
	grid = deepcopy(origin_grid)

	# Count the number of refinement
	# This does not mean the number of edges refined
	# Just an indicator that refinement happens 
	refine_edge_count = 0

	# Find out how many full nodes are in each worker
	population_table = find_population_table(commun, grid)

	# Apply m sweeps of refinement
	for i in range(m):
		
		# Find all of the base edges
		base_edges = build_base_edges(grid)

		# Update the error indicators of neighbouring processors
		update_error_indicators(commun, grid, base_edges, error_tol)

		# dictionary of refined edges for other processors
		refine_edges = {}

		# List of edges that will be refined after other edges are refined
		unrefined_edges = []

		# Loop over the base edges and split or refine the triangle along that
		# base edge
		for edge in base_edges:
		
			# Check if the edge still exist in the grid
			if grid.is_edge(edge.get_endpt1(),edge.get_endpt2()):

				if grid.get_error_indicator(edge.get_endpt1(),edge.get_endpt2()) > error_tol:

					# If both endpoints of this edge are in the ghost table
					# skip this edge
					if check_edge_nodes_ghost(grid, edge):
						continue

					# Split the triangle(s) besides the edge
					split_triangle(commun.get_my_no(), population_table, grid,
							       edge,true_soln,rhs,refine_edges,error_indicator_type,unrefined_edges)

					# Increase refinement count
					refine_edge_count += 1


		# Initialise another edge list for unfinished edges
		unfinished_edges = []
		
		#plot_fem_subgrid_base(commun.get_my_no(),grid)
		
		# Stop inidcator for while loop
		keep_refine = True

		# Keep refine if some edge in one of the procesors is not refined correctly
		while keep_refine: 

			# Reset keep refine
			keep_refine = False

			# Communicate with the other processors and exchange refined edges
			received_refine_edges = communicate_refine_edge_list(commun, 
				                                    grid, refine_edges)
			
			# Initialise refine edges again
			refine_edges = {}

			# check and refine the received edges from the other processors
			refine_count = refine_receive_edges(commun, population_table,grid, 
				 						     received_refine_edges, refine_edges,
				               				 true_soln,rhs,error_indicator_type,unrefined_edges)

			# Increase refinement edge count
			refine_edge_count += refine_count

			#plot_fem_subgrid_base(commun.get_my_no(),grid)

			# Initialise a new list for unsuccessful edge refinement 
			unrefined_edges_new = []

			# Refine the unrefined edges from the previous part
			for edge in unrefined_edges:

				# If the edge still exists
				if grid.is_edge(edge.get_endpt1(),edge.get_endpt2()):

					# If the error indicator shows the edge needs refinement
					if grid.get_error_indicator(edge.get_endpt1(),edge.get_endpt2()) > error_tol:

						# If both endpoints of this edge are in the ghost table
						if check_edge_nodes_ghost(grid, edge):

							# skip this edge
							continue

						# Split the triangle(s) besides the edge
						mid_id = split_triangle(commun.get_my_no(), population_table, 
										    grid, edge,true_soln,rhs,refine_edges,
											error_indicator_type,unrefined_edges_new)
						
						# If the refinement failed
						if mid_id is None:

							# Print error info
							print "worker", commun.get_my_no(), ": edge", edge, "failed" 
							
							# Keep refine in the next round
							keep_refine = True

			# Update the unrefined edge list
			unrefined_edges = unrefined_edges_new

			# Communicate with other processors
			keep_refine = get_global_keep_refine(commun, keep_refine)


		# In the parallel implementation we may find that some of the ghost
		# nodes are no longer joined to a full node, they should be removed
		# and the communication pattern updated
		trim_nodes(grid)

	# Return the refined grid
	return grid,refine_edge_count