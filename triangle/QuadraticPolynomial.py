#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 13:33:09

This class keeps record of a quadratic polynomial in two dimensions.
It currently only offers minimal functionality, additional polynomial
opertations, such as addition and scalar multiplication, could also 
be included.

"""

# Import other classes
from triangle.LinearPolynomial import LinearPolynomial


class QuadraticPolynomial:
	""" A quadratic polynomial in two dimensions"""
	
	# constant coefficient
	_const = 0.0
	
	# x coefficient
	_x = 0.0
	
	# y coefficient
	_y = 0.0
   
	# xy coefficient
	_xy = 0.0

	# x^2 coefficient
	_xx = 0.0

	# y^2 coefficient
	_yy = 0.0

	def __init__(self, const=0.0, x=0.0, y=0.0, xy=0.0, xx=0.0, yy=0.0):
		"""Initialise a polynomial
		
		By default, the polynomial is the zero polynomial
		"""
		self._const = const
		self._x = x
		self._y = y
		self._xy = xy
		self._xx = xx
		self._yy = yy
		
	def __str__(self):
		"""Convert a polynomial into a string so it can be printed
		
		The format is constant coefficient + x coefficient * x 
		+ y coefficient * y + xy coefficient * xy + xx coefficient
		* x^2 + yy coefficient * y^2
		
		"""
		return str(self._const) + " + " + str(self._x) + "x + " \
		+ str(self._y) + "y + " + str(self._xy) + "xy + " \
		+ str(self._xx) + "x^2 + " + str(self._yy) + "y^2"
	
	def __add__(self, polynomial2):
		""" addition operator """
		poly =  QuadraticPolynomial(self._const+polynomial2.get_const(), \
				self._x+polynomial2.get_x(), self._y+polynomial2.get_y(), \
				self._xy+polynomial2.get_xy(), self._xx+polynomial2.get_xx(), \
				self._yy+polynomial2.get_yy())
		return poly
		
	def set_const(self, const):
		"""Set constant coefficient"""
		self._const = const
	
	def set_x(self, x):
		"""Set the x coefficient"""
		self._x = x
		
	def set_y(self, y):
		"""Set the y coefficient"""
		self._y = y

	def set_xy(self, xy):
		"""Set the xy coefficient"""
		self._xy = xy

	def set_xx(self, xx):
		"""Set the xx coefficient"""
		self._xx = xx

	def set_yy(self, yy):
		"""Set the yy coefficient"""
		self._yy = yy

	def get_const(self):
		"""Get the constant coefficient"""
		return self._const
		
	def get_x(self):
		"""Get the x coefficient"""
		return self._x
	
	def get_y(self):
		"""Get the y coefficient"""
		return self._y

	def get_xy(self):
		"""Get the xy coefficient"""
		return self._xy

	def get_xx(self):
		"""Get the xx coefficient"""
		return self._xx

	def get_yy(self):
		"""Get the yy coefficient"""
		return self._yy
	
	def eval(self, x, y): 
		"""evaluate the polynomial at the posn (x, y)"""
		return self._const + self._x * x + self._y * y \
				+ self._xy * x * y + self._xx * x * x \
				+ self._yy * y * y

	def dx(self): 
		"""differentiate the polynomial in the x direction"""
		poly = LinearPolynomial(self._x, 2.0*self._xx, self._xy)
		return poly
		
	def dy(self): 
		"""differentiate the polynomial in the y direction"""
		poly = LinearPolynomial(self._y, self._xy, 2.0*self._yy)
		return poly
		