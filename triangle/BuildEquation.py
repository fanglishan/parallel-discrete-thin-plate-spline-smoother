#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-01-22 18:07:35
@Last Modified time: 2019-04-21 23:07:42

This class builds and stores the finite element matrix and load vector.
It also sets the Dirichlet boundary conditions.

"""

# Import other classes
from triangle.LinearPolynomial import LinearPolynomial
from triangle.Triangle import triangle_iterator
from triangle.SetPolynomial import set_polynomial_linear_2D
from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator
from grid.Edge import DomainSet      



def Poisson_tri_integrate_linear(poly1, poly2, node1, node2, node3):
    """Poisson model problem""" 
    
    # Import appropriate information from other classes
    from TriangleIntegrateLinear import linear_integrate
    
    # Apply numerical quadrature routines to approximate the integrals
    local_stiffness_x = linear_integrate(poly1.dx(), poly2.dx(), 
                                      node1, node2, node3)
    local_stiffness_y = linear_integrate(poly1.dy(), poly2.dy(), 
                                        node1, node2, node3)

    return local_stiffness_x+local_stiffness_y


def local_stiffness_linear_2D(node1, node2, node3, tri_integrate):
    """Find the element stiffness matrix"""

    # Find the polynomials who's support is on the trinalge
    poly1 = set_polynomial_linear_2D(node1, node2, node3)
    poly2 = set_polynomial_linear_2D(node2, node3, node1)
    poly3 = set_polynomial_linear_2D(node3, node1, node2)

    # Evaluate the contribution to the local stiffness matrix
    local_stiffness1 = tri_integrate(poly1, poly1, node1, node2, node3)
    local_stiffness2 = tri_integrate(poly1, poly2, node1, node2, node3)
    local_stiffness3 = tri_integrate(poly1, poly3, node1, node2, node3)

    return local_stiffness1, local_stiffness2, local_stiffness3


def set_slave_value(grid, parameter):
    """ Assign the slave node values using boundary function """

    # Model functions
    function = parameter.get_function()
    u = function[0]
    ux = function[1]
    uy = function[2]
    uxxyy = function[3]

    # Iterate through nodes
    for node in node_iterator(grid):

        # Set slave value
        if node.get_slave():
            node.set_value(u(node.get_coord()))
            node.set_d_values([ux(node.get_coord()),uy(node.get_coord()),\
                    uxxyy(node.get_coord())])

            
def sum_data_matrix(grid, node1, node2, local_data):
    """Add local_data to the current matrix A value"""
    
    # Get the node ids
    id1 = node1.get_node_id()
    id2 = node2.get_node_id()
    
    # Find the current data matrix value
    data_entry = grid.get_matrix_A_value(id1, id2)
    
    # Add local_stiff
    grid.set_matrix_A_value(id1, id2, data_entry + local_data)


def sum_load(node, local_load):
    """Add local_data to the current d value"""
    
    # Find the current load value
    load = node.get_load()
    
    # Add local load
    node.set_load(load + local_load)


def sum_stiffness(grid, node1, node2, local_stiff):
    """Add local_stiff to the current matrix value"""
    
    # Get the node ids
    id1 = node1.get_node_id()
    id2 = node2.get_node_id()
    
    # Find the current stiffness value
    stiff = grid.get_matrix_L_value(id1, id2)
    
    # Add local_stiff
    grid.set_matrix_L_value(id1, id2, stiff + local_stiff)
    

def sum_gradient(grid, node1, node2, local_grad):
    """Add local_grad to the current matrix G value"""
    
    # Get the node ids
    id1 = node1.get_node_id()
    id2 = node2.get_node_id()
    
    # Find the current stiffness value
    grad = grid.get_matrix_G_value(id1, id2)

    # Add contribution from the local gradient
    new_grad = [grad[0]+local_grad[0], grad[1]+local_grad[1]]

    # Add local_stiff
    grid.set_matrix_G_value(id1, id2, new_grad)


def initialise_connections_only(grid):
    """ Initialise connections of the grid to zero """

    for node in node_iterator(grid):
        node_id = node.get_node_id()
        if not grid.is_connection(node_id, node_id):
            grid.add_connection(node_id, node_id)
        for endpt in endpt_iterator(grid, node.get_node_id()):
            if not grid.is_connection(node_id, endpt):
                grid.add_connection(node_id, endpt)


def initialise_connections(grid):
    """ Initialise connections of the grid to zero """

    for node in node_iterator(grid):
        node_id = node.get_node_id()
        node.set_load(0.0)
        if not grid.is_connection(node_id, node_id):
            grid.add_connection(node_id, node_id)
        grid.set_matrix_A_value(node_id, node_id, 0.0)
        grid.set_matrix_L_value(node_id, node_id, 0.0)
        grid.set_matrix_G_value(node_id, node_id, [0.0,0.0])
        for endpt in endpt_iterator(grid, node.get_node_id()):
            if not grid.is_connection(node_id, endpt):
                grid.add_connection(node_id, endpt)
            grid.set_matrix_A_value(node_id, endpt, 0.0)
            grid.set_matrix_L_value(node_id, endpt, 0.0)
            grid.set_matrix_G_value(node_id, endpt, [0.0,0.0])


def initialise_connections_node(grid, node):
    """ Initialise connections of the new node """

    node_id = node.get_node_id()
    node.set_load(0.0)
    if not grid.is_connection(node_id, node_id):
        grid.add_connection(node_id, node_id)
    grid.set_matrix_A_value(node_id, node_id, 0.0)
    grid.set_matrix_L_value(node_id, node_id, 0.0)
    grid.set_matrix_G_value(node_id, node_id, [0.0,0.0])

    for endpt in endpt_iterator(grid, node_id):
        if not grid.is_connection(node_id, endpt):
            grid.add_connection(node_id, endpt)
        grid.set_matrix_A_value(node_id, endpt, 0.0)
        grid.set_matrix_L_value(node_id, endpt, 0.0)
        grid.set_matrix_G_value(node_id, endpt, [0.0,0.0])
        grid.set_matrix_A_value(endpt, node_id, 0.0)
        grid.set_matrix_L_value(endpt, node_id, 0.0)
        grid.set_matrix_G_value(endpt, node_id, [0.0,0.0])


def normalise_A_d(grid, parameter):
    """ Divide A and d entries with number of data points """

    # Normalisation ratio of A and d entries
    if parameter.get_data_size() == 0:
        ratio = 1.0
    else:
        ratio = 1.0/parameter.get_data_size()

    # Iterate through the nodes
    for node in node_iterator(grid):

        # Node id
        node_id = node.get_node_id()

        # Find the current stiffness value
        data_entry = grid.get_matrix_A_value(node_id, node_id)

        # Apply ratio
        grid.set_matrix_A_value(node_id, node_id, data_entry*ratio)

        # Find the current data load value
        load = node.get_load()

        # Apply ratio
        node.set_load(load*ratio)

        # Iterate through the endpoints
        for endpt in endpt_iterator(grid, node.get_node_id()):

            # Find the current stiffness value
            data_entry = grid.get_matrix_A_value(node_id, endpt)

            # Apply ratio
            grid.set_matrix_A_value(node_id, endpt, data_entry*ratio)



def renormalise_A_d(grid, old_data_size, new_data_size):
    """ Change the normalisation of A and d entries """

    # Normalisation ratio of A and d entries
    if new_data_size == 0:
        ratio = 0.0
    else:
        ratio = 1.0*old_data_size/new_data_size

    # Iterate through the nodes
    for node in node_iterator(grid):

        # Node id
        node_id = node.get_node_id()
        
        # Find the current data load value
        load = node.get_load()

        # Apply ratio
        node.set_load(load*ratio)

        # Find the current stiffness value
        data_entry = grid.get_matrix_A_value(node_id, node_id)

        # Apply ratio
        grid.set_matrix_A_value(node_id, node_id, data_entry*ratio)

        # Iterate through the endpoints
        for endpt in endpt_iterator(grid, node.get_node_id()):

            # Find the current stiffness value
            data_entry = grid.get_matrix_A_value(node_id, endpt)

            # Apply ratio
            grid.set_matrix_A_value(node_id, endpt, data_entry*ratio)