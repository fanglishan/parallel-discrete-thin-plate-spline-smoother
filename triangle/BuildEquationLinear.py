#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-21 22:03:06
@Last Modified time: 2019-04-22 13:34:43

This class builds and stores the entries of the matrix and load vector.

"""


# Import other classes
from triangle.SetPolynomial import set_polynomial_linear_2D
from Triangle import triangle_iterator, triangle_iterator_ref
from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator
from grid.Edge import DomainSet      
from triangle.BuildEquation import set_slave_value, sum_data_matrix, \
	sum_load, sum_stiffness, sum_gradient, \
	Poisson_tri_integrate_linear, initialise_connections, normalise_A_d 
from triangle.TriangleIntegrateLinear import linear_integrate
from parameter.GridParameter import BoundaryType
from data.ObtainData import obtain_data


def local_A_linear_2D(data_points, node1, node2, node3, poly1, poly2, poly3):
	""" Find the data projection matrix A values """

	# Initialise data projection matrix and vector entries as zero
	data_project1 = 0.0
	data_project2 = 0.0
	data_project3 = 0.0
	data_vector = 0.0

	# Iterate through each data point
	for point in data_points:

		# Coodinates of the data point
		coord1 = point[0]
		coord2 = point[1]

		# Evaluate the contribution to the data projection matrix
		data_project1 += poly1.eval(coord1, coord2)*poly1.eval(coord1, coord2)
		data_project2 += poly1.eval(coord1, coord2)*poly2.eval(coord1, coord2)
		data_project3 += poly1.eval(coord1, coord2)*poly3.eval(coord1, coord2)

		data_vector += poly1.eval(coord1, coord2)*point[2]

	return data_project1, data_project2, data_project3, data_vector


def local_L_linear_2D(node1, node2, node3, poly1, poly2, poly3, tri_integrate):
	""" Find the element stiffness matrix L values """

	# Evaluate the contribution to the local stiffness matrix
	local_stiffness1 = tri_integrate(poly1, poly1, node1, node2, node3)
	local_stiffness2 = tri_integrate(poly1, poly2, node1, node2, node3)
	local_stiffness3 = tri_integrate(poly1, poly3, node1, node2, node3)

	return local_stiffness1, local_stiffness2, local_stiffness3


def local_G_linear_2D(node1, node2, node3, poly1, poly2, poly3):
	""" Find the gradient matrix G values """

	# Evaluate the contribution to the local gradient matrix wrt x
	local_gx1 = linear_integrate(poly1, poly1.dx(), node1, node2, node3)
	local_gx2 = linear_integrate(poly1, poly2.dx(), node1, node2, node3)
	local_gx3 = linear_integrate(poly1, poly3.dx(), node1, node2, node3)
	
	# Evaluate the contribution to the local gradient matrix wrt y
	local_gy1 = linear_integrate(poly1, poly1.dy(), node1, node2, node3)
	local_gy2 = linear_integrate(poly1, poly2.dy(), node1, node2, node3)
	local_gy3 = linear_integrate(poly1, poly3.dy(), node1, node2, node3)

	return [local_gx1,local_gy1], [local_gx2,local_gy2], [local_gx3,local_gy3]


def calculate_connections(grid, data_points, node1, node2, node3):

	# Find the polynomials who's support is on the trinalge
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Find the local A and d entries
	data1, data2, data3, data_vector = local_A_linear_2D(data_points, \
			node1, node2, node3, poly1, poly2, poly3)

	sum_data_matrix(grid, node1, node1, data1)
	sum_data_matrix(grid, node1, node2, data2)
	sum_data_matrix(grid, node1, node3, data3)

	sum_load(node1, data_vector)


	# Find the local stiffness entries
	stiff1, stiff2, stiff3 = local_L_linear_2D(node1, node2, node3, \
			poly1, poly2, poly3, tri_integrate)
											
	sum_stiffness(grid, node1, node1, stiff1)
	sum_stiffness(grid, node1, node2, stiff2)
	sum_stiffness(grid, node1, node3, stiff3)


	# Find the local gradient entries
	grad1, grad2, grad3 = local_G_linear_2D(node1, node2, node3, poly1, poly2, poly3)

	sum_gradient(grid, node1, node1, grad1)
	sum_gradient(grid, node1, node2, grad2)
	sum_gradient(grid, node1, node3, grad3)
	
 
def build_equation_linear_2D(grid, region, parameter):
	""" Define the system and vector entries """
	
	# Reset region data usage
	region.init_usage()

	# Boundary type of the problem
	boundary_type = parameter.get_boundary()

	# Set slave values for Dirichlet boundaries (not for error indicators)
	if boundary_type == BoundaryType.Dirichlet and not parameter.get_local():
		set_slave_value(grid, parameter)

	# Add the matrix connections for linear basis functions and 
	# initialise to zero
	initialise_connections(grid)
					
	# Evaluate that TPSFEM matrix and rhs vector
	
	# Loop over the triangles once
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
			tri[1].get_node_id() < tri[2].get_node_id():
		
			data_points = obtain_data(region, [tri[0], tri[1], tri[2]])

			# Add connections between tri0 and tri1, tri2
			calculate_connections(grid, data_points, tri[0], tri[1], tri[2], tri_integrate)

			# Add connections between tri1 and tri0, tri2
			calculate_connections(grid, data_points, tri[1], tri[0], tri[2], tri_integrate)

			# Add connections between tri2 and tri0, tri1
			calculate_connections(grid, data_points, tri[2], tri[0], tri[1], tri_integrate)

	# Divide A and d entries by number of data points
	normalise_A_d(grid, parameter)
