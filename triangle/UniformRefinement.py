#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 13:40:49

This class contains the refinement routines for a triangular grid. The
main routine of interest is uniform_refinement, the rest are helper
routines.

"""

# import other classes
from copy import deepcopy
from grid.GlobalNodeID import GlobalNodeID
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.ConnectTable import connect_iterator
from grid.Edge import DomainSet, RefineSet
from grid.NghNodes import NghNodes, ngh_iterator
from grid.Grid import Grid

# Import libraries
from copy import deepcopy


class NodeSet:
	""" Full node or ghost node """
	full_node = 1
	ghost_node = 2
	

def trim_ghost_nodes(grid):
	"""Remove extra ghost nodes"""
	
	# Find the ghost neighbour node table
	ghost_commun = grid.reference_ghost_commun()

	# Find the ghost table
	ghost_table = grid.reference_ghost_table()
	
	# Keep a record of which nodes must be removed, as well as any
	# connections or edges joined to that node
	remove_nodes = Grid()
	
	# Loop over the ghost nodes
	for node in node_iterator(ghost_table):
		node_id = node.get_node_id()
		
		# Initially assume the ghost node should be removed
		remove = True
		
		# Check to see if the ghost node is joined to a full node
		for endpt in connect_iterator(grid, node_id):
			if grid.is_in(endpt):
				
				# If it is joined to a full node then don't remove it
				remove = False
				break    
				
				
		# If the ghost node is to be removed, get a copy of the extra
		# information that must be removed from the grid
		if remove:
			
			# Note that the node should be removed
			remove_nodes.add_node(node)
			
			# Any connections joined to the node should be removed
			for endpt in connect_iterator(grid, node_id):
				remove_nodes.add_connection(node_id, endpt)
				
			# Any edge joined to the node should be removed
			for endpt in endpt_iterator(grid, node_id):
				remove_nodes.add_edge(node_id, endpt)

	# Now remove the nodes
	
	# Loop over the nodes to be removed
	for node in node_iterator(remove_nodes):
		node_id = node.get_node_id()
 
		# Any connection to or from the node must be removed
		# (being careful that the connection may have already been removed
		# when another node was removed from the grid)
		for endpt in connect_iterator(remove_nodes, node_id):
			if grid.is_connection(node_id, endpt):
				grid.delete_connection(node_id, endpt)
			if grid.is_connection(endpt, node_id):
				grid.delete_connection(endpt, node_id)
				
		# Any edge to or from the node must be removed
		# (being careful that the edge may have already been removed
		# when another node was removed from the grid)
		for endpt in endpt_iterator(remove_nodes, node_id):
			if grid.is_edge(node_id, endpt):
				grid.delete_edge(node_id, endpt)
			if grid.is_edge(endpt, node_id):
				grid.delete_edge(endpt, node_id)
				
		# Remove the node from the ghost node table
		ghost_table.delete_node(node_id)
		
		# Remove the node from the neighbour node list
		
		# Find which worker has the full node copy
		for worker, id_list in ngh_iterator(ghost_commun):
			if node_id in id_list:
				full_worker_no = int(worker)
				break
				
		# Remove the node from that neighbour worker information    
		ghost_commun.delete_neighbour_node(full_worker_no, node_id)
			

def trim_full_nodes(grid):
	"""Check to see if updates still need to be sent to neighbouring nodes"""
	
	# Get the full neighbour node table
	full_commun = grid.reference_full_commun()
	
	# Get the ghost neighbour node table
	ghost_commun = grid.reference_ghost_commun()

	# Keep a record of the information that needs to be removed
	remove_nodes = NghNodes()
	
	# Loop over the neighbour workers
	for worker, id_list in ngh_iterator(full_commun):
		
		# Loop over the list of full nodes that should send updated information
		# to the neighbouring worker
		for node_id in id_list:
			
			# Assume the information should be removed
			remove = True
			
			# If the ghost node copy of the current node in the neighbour worker
			# is connected to some full node, then do not remove it from the
			# list 
			for endpt in connect_iterator(grid, node_id):
				if ghost_commun.is_neighbour_node(int(worker), endpt):
					remove = False
					break
					
			# If the node should be removed, record the necessary information
			if remove:
				remove_nodes.add_neighbour_node(int(worker), node_id)
				
	# Loop over the neighbouring workers
	for worker, id_list in ngh_iterator(remove_nodes):
		
		# Loop over the list of nodes to be removed from the communication
		# pattern
		for node_id in id_list:
			
			# Remove those nodes from the full neighbour node table
			full_commun.delete_neighbour_node(int(worker), node_id)
			

def trim_nodes(grid):
	"""Remove unwanted ghost nodes and update the communication pattern"""
	
	# Remove any unwanted ghost notes
	trim_ghost_nodes(grid)
	
	# Adjust the communication patter to take into account that some
	# ghost nodes may have been removed from neighbouring workers
	trim_full_nodes(grid)
	

def get_full_worker_no(my_worker_no, grid, node_id):
	"""Find which processor contains the full node copy of the given node"""
	
	# Find the ghost neighbour node table 
	ghost_commun = grid.reference_ghost_commun()

	# Check to see if the node is a full node in the current grid
	if grid.is_in(node_id):
		return my_worker_no
		
	# If the node is not a full node, it must be a ghost node and 
	# thus have an entry in the ghost neighbour node table
	
	# Use the ghost neighbour node table to find which neighbouring worker
	# contains a full copy of the node
	worker_found = False
	for worker, id_list in ngh_iterator(ghost_commun):
		if node_id in id_list:
			id_worker = int(worker)
			worker_found = True
			break
			
	# If no neighbour worker was found then something went wrong
	assert worker_found, 'worker number not found for node '+str(node_id)
	
	# Return the neighbour worker id
	return id_worker
	

def add_to_ngh_nodes(my_worker_no, grid, full_worker_no, node):
	"""Add the new node to the communication pattern"""
	
	# Get the ghost table
	ghost_table = grid.reference_ghost_table()
		   
	# Get the full neighbour node table
	full_commun = grid.reference_full_commun()
	
	# Get the ghost neighbour node table
	ghost_commun = grid.reference_ghost_commun()    
	
	# Find the node id
	node_id = node.get_node_id()
			
	# If the node is a ghost node, simply record the position of the
	# corresponding full node
	if ghost_table.is_in(node_id):
		ghost_commun.add_neighbour_node(full_worker_no, node_id)        
	else:
		
		# If the node is a full node, find all of the neighbouring processors
		# that have a ghost node copy
		for endpt in connect_iterator(grid, node_id):
			if ghost_table.is_in(endpt):
				full_worker_no = get_full_worker_no(my_worker_no, grid, endpt)
				if not full_commun.is_neighbour_node(full_worker_no, node_id):
					full_commun.add_neighbour_node(full_worker_no, node_id)
 

def find_worker_no(my_worker_no, population_table, grid, node1, node2):
	"""Find which worker should get the full copy of the midpoint"""
	
	# Get a copy of the ghost node table
	ghost_table = grid.reference_ghost_table()
		   
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	global_id1 = node1.get_global_id()
	global_id2 = node2.get_global_id()
	
	# Find which worker has the full node copy of node1 
	if ghost_table.is_in(node_id1):
		id1_worker = get_full_worker_no(my_worker_no, grid, node_id1)
	else:
		id1_worker = my_worker_no
   
	# Find which worker has the full node copy of node2   
	if ghost_table.is_in(node_id2):
		id2_worker = get_full_worker_no(my_worker_no, grid, node_id2)
	else:
		id2_worker = my_worker_no

	# If one worker contains the full node copy of the node1 and node2, then
	# the midpoint should be added as a full node copy to that worker
	if id1_worker == id2_worker:
		return id1_worker
	
	# Now suppose different workers contain the full node copies of the 
	# endpotins. 
	# The midpoint should be added as a full node copy to one of those workers, 
	# but which one
	
	# Try to add the full node copy of the midpoint to a worker with the 
	# smallest number of full nodes
	if population_table[id1_worker] < population_table[id2_worker]:
		return id1_worker
	if population_table[id1_worker] > population_table[id2_worker]:
		return id2_worker

	# If both workers contain the same number of full nodes, then break the 
	# deadlock by using the global ids    
	if global_id1 < global_id2:
		return id1_worker
	else:
		return id2_worker
		
	
def build_midpoint_global_id(grid, global_id1, global_id2):
	"""Find the global id of the new midpoint""" 
	
	# Import appropriate information from other classes
	from grid.GlobalNodeID import mesh_integers_2D, unmesh_integers_2D
	
	# The node is being added a what level of refinement
	current_level = 0
	if global_id1.get_level() > global_id2.get_level():
		current_level = global_id1.get_level()+1
	else:
		current_level = global_id2.get_level()+1
		
	# The number must be scaled according to the level of refinement
	scale1 = 2**(current_level-global_id1.get_level()-1)
	scale2 = 2**(current_level-global_id2.get_level()-1)
		
	# Extract the coordinates that were used to find the ids of the endpoints 
	id1x, id1y = unmesh_integers_2D(global_id1.get_no())
	id2x, id2y = unmesh_integers_2D(global_id2.get_no())
		
	# Find the coordinates of the midpoint
	idx = id1x*scale1+id2x*scale2
	idy = id1y*scale1+id2y*scale2
		
	# Create a new global id
	id = mesh_integers_2D(idx, idy)
	global_id = GlobalNodeID(id, current_level)

	# Return the global id
	return global_id
	
	
def add_midpoint(grid, node1, node2, node_type):
	"""Add the midpoint to the grid""" 
	
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
		
	# Find the global id of the midpoint
	mid_global_id = build_midpoint_global_id(grid, node1.get_global_id(),
											 node2.get_global_id())
											 
	# Determine if the midpoint will sit on the boundary                                         
	location = grid.get_location(node_id1, node_id2)
	bnd_func = grid.get_boundary_function(node_id1, node_id2)

	# If the midpoint is to be added as a full node
	if node_type == NodeSet.full_node:
		
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a full slave node
			value = bnd_func(coord)
			#print "boundary",value
			mid_node = grid.create_node(mid_global_id, coord, True, value)
			grid.add_node(mid_node)
				
		else:
			
			# Add the midpoint as a full node (and initialise the value and
			# load to be the average of the endpoints)
			value = (node1.get_value() + node2.get_value())/2.0
			#print "interior",value,node1.get_value(),node2.get_value()
			load = (node1.get_load() + node2.get_load())/2.0
			mid_node = grid.create_node(mid_global_id, coord, False, value)
			mid_node.set_load(load)
			grid.add_node(mid_node)
			
	# If the midpoint is a ghost node
	else:
		ghost_table = grid.reference_ghost_table()
		
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a ghost slave node
			value = bnd_func(coord)
			mid_node = ghost_table.create_node(mid_global_id, coord, True, 
											   value)
			ghost_table.add_node(mid_node)
				
		else:
			
			# Add the midpoint as a ghost node (and initialise the value and
			# load to be the average of the endpoints)
			value = (node1.get_value() + node2.get_value())/2.0
			load = (node1.get_load() + node2.get_load())/2.0
			mid_node = ghost_table.create_node(mid_global_id, coord, False, 
											   value)
			mid_node.set_load(load)
			ghost_table.add_node(mid_node)

	return mid_node
	

def split_edge(grid, node1, node2, node_type):
	""" Split the edge between node1 and node2 """ 
	
	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the edge location 
	location = grid.get_location(node_id1, node_id2)
	boundary_function = grid.get_boundary_function(node_id1, node_id2)

	# Find the refinement level
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Add the midpoint to the grid
	mid_node = add_midpoint(grid, node1, node2, node_type) 
	mid_id = mid_node.get_node_id()
	
	# Remove the old edges
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)
	
	# Add a new edge between the midpoint and node1
	grid.add_edge(mid_id, node_id1)
	grid.set_location(mid_id, node_id1, location)
	grid.set_boundary_function(mid_id, node_id1, boundary_function)
	grid.set_refine_type(mid_id, node_id1, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id1, refine_level)

	# Add a new edge between the node1 and midpoint
	grid.add_edge(node_id1, mid_id)
	grid.set_location(node_id1, mid_id, location)
	grid.set_boundary_function(node_id1, mid_id, boundary_function)
	grid.set_refine_type(node_id1, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id1, mid_id, refine_level) 

	# Add a new edge between the midpoint and node2
	grid.add_edge(mid_id, node_id2)
	grid.set_location(mid_id, node_id2, location)
	grid.set_boundary_function(mid_id, node_id2, boundary_function)
	grid.set_refine_type(mid_id, node_id2, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id2, refine_level)

	# Add a new edge between the node2 and midpoint
	grid.add_edge(node_id2, mid_id)
	grid.set_location(node_id2, mid_id, location)
	grid.set_boundary_function(node_id2, mid_id, boundary_function)
	grid.set_refine_type(node_id2, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id2, mid_id, refine_level)

	
	# Return the midpoint
	return mid_node
	 
	 
def set_base_type(grid, node_id1, node_id2):
	""" Set base edge types """

	if grid.get_location(node_id1, node_id2) != DomainSet.interior:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)
	elif grid.get_refine_type(node_id1, node_id2) == RefineSet.not_base_edge:
		grid.set_refine_type(node_id1, node_id2, RefineSet.interface_base_edge)
	else:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)



def add_triangle4(my_worker_no, population_table, grid, 
				  node_id1, node_id2, node_id3, node_id4):
	""" Refine two triangles along the given base edge """ 
	
	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)
	else:
		node1 = ghost_table.get_node(node_id1)
	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
	else:
		node2 = ghost_table.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Which worker should get the full copy of the new node
	full_worker_no = find_worker_no(my_worker_no, population_table, grid, 
								node1, node2)
	if full_worker_no == my_worker_no:
		node_type = NodeSet.full_node
	else:
		node_type = NodeSet.ghost_node
		
	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)
	
	# Add the edges going from the midpoint to node_id4
	grid.add_edge(mid_id, node_id4)
	grid.set_location(mid_id, node_id4, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id4, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id4, refine_level)

	grid.add_edge(node_id4, mid_id)
	grid.set_location(node_id4, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id4, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id4, mid_id, refine_level)
	
	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)
		
	# Add connections between the midpoint and four vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(mid_id, node_id4)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)
	grid.add_connection(node_id4, mid_id)   
	
	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level)
	grid.set_refine_level(node_id1, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id1, refine_level)    
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)
	grid.set_refine_level(node_id2, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id2, refine_level)    

	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1)
	set_base_type(grid, node_id1, node_id4)
	set_base_type(grid, node_id4, node_id1)    
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)
	set_base_type(grid, node_id2, node_id4)
	set_base_type(grid, node_id4, node_id2)  

	# Initialise the error indicator 
	# (used for adaptive refinement)
	error_indicator = -1.0
	grid.set_error_indicator(node_id1, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id1, error_indicator)
	grid.set_error_indicator(node_id1, node_id4, error_indicator)
	grid.set_error_indicator(node_id4, node_id1, error_indicator)    
	grid.set_error_indicator(node_id2, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id2, error_indicator)
	grid.set_error_indicator(node_id2, node_id4, error_indicator)
	grid.set_error_indicator(node_id4, node_id2, error_indicator)    
	
  
	# Update the communication pattern
	add_to_ngh_nodes(my_worker_no, grid, full_worker_no, mid_node)
   
 
def add_triangle3(my_worker_no, population_table, grid, node_id1, node_id2, 
				  node_id3):
	""" Refine a triangle along the given base edge """ 
	
	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()
	
	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)
	else:
		node1 = ghost_table.get_node(node_id1)
	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
	else:
		node2 = ghost_table.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Which worker should get the full copy of the new node
	full_worker_no = find_worker_no(my_worker_no, population_table, grid, 
								node1, node2)
	if full_worker_no == my_worker_no:
		node_type = NodeSet.full_node
	else:
		node_type = NodeSet.ghost_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and three vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)

	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level) 
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)

	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1) 
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)

	# Initialise the error indicator 
	# (used for adaptive refinement)
	error_indicator = -1.0
	grid.set_error_indicator(node_id1, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id1, error_indicator)   
	grid.set_error_indicator(node_id2, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id2, error_indicator)  
	
	# Update the communication pattern
	add_to_ngh_nodes(my_worker_no, grid, full_worker_no, mid_node)

 
 def add_triangle2(my_worker_no, population_table, grid, node_id1, node_id2):
	""" Refine a triangle along the given base edge """ 

	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)
	else:
		node1 = ghost_table.get_node(node_id1)
	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
	else:
		node2 = ghost_table.get_node(node_id2)

	# Which worker should get the full copy of the new node
	full_worker_no = find_worker_no(my_worker_no, population_table, grid, 
								node1, node2)
	if full_worker_no == my_worker_no:
		node_type = NodeSet.full_node
	else:
		node_type = NodeSet.ghost_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and two vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)

	# Update the communication pattern
	add_to_ngh_nodes(my_worker_no, grid, full_worker_no, mid_node)

  
def split_triangle(my_worker_no, population_table, grid, edge):
	""" Add all of the nodes in a square grid """ 
	
	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()
	
	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		print "no edge found"
		return

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)
		
	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			# See if we can find a second, different, triangle
			
			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):
					
					# Refine those two triangles
					add_triangle4(my_worker_no, population_table, grid, endpt1, 
								  endpt2, edgestar[i], edgestar[j])
					return
					
			# If only one triangle was found, refine that triangle
			add_triangle3(my_worker_no, population_table, grid, 
						  endpt1, endpt2, edgestar[i])

			return
	
	# Sometime in the parallel implementation no triangle is found, so refine
	# the edge
	add_triangle2(my_worker_no, population_table, grid, endpt1, endpt2)


def build_base_edges(grid):
	"""Build a list of base edges"""

	# Find the ghost node table
	ghost_table = grid.reference_ghost_table()

	# Initialise the list
	base_edges = list()
	
	# Loop over the full nodes
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the full node
		for endpt in endpt_iterator(grid, node_id):
			
			# We only want to find each edge once
			if node_id < endpt:

				# Get the refinement type
				refine_type = grid.get_refine_type(node_id, endpt)

				# If the refinement type is a base type add the edge to the list
				if refine_type == RefineSet.base_edge\
				or refine_type == RefineSet.interface_base_edge:

					base_edges.append(grid.get_edge(node_id, endpt))
				
	# Loop over the ghost nodes
	for node in node_iterator(ghost_table):
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the ghost node
		for endpt in endpt_iterator(grid, node_id):
			
			# Get the refinement type
			refine_type = grid.get_refine_type(node_id, endpt)
			
			# We only want to find each edge once
			if node_id < endpt:
				
				# If the refinement type is a base type add the edge to the list
				# (In the parallel implementation an edge between two ghost 
				# nodes may only be listed as an interface_base_edge, we should
				# also take those as a base edge)
				if refine_type == RefineSet.base_edge\
				or refine_type == RefineSet.interface_base_edge:

					base_edges.append(grid.get_edge(node_id, endpt))
		
	# Return the list of base edges
	return base_edges


def uniform_refinement(commun, grid, m):
	"""Implement uniform refinement"""
	
	# Make a copy of the current grid
	refine_grid = deepcopy(grid)

	# Find out how many full nodes are in each worker
	population_table = [0]*commun.get_no_workers()
	pp = commun.all_gather(grid.get_no_nodes())
	for i in range(commun.get_no_workers()):
		population_table[i] = pp[i]

	# Apply m sweeps of refinement
	for i in range(m):
		
		# Find all of the base edges
		base_edges = build_base_edges(refine_grid)
	
		# Loop over the base edges and split or refine the triangle along that
		# base edge
		for edge in base_edges:
			split_triangle(commun.get_my_no(), population_table, 
						   refine_grid, edge)
		
		# In the parallel implementation we may find that some of the ghost
		# nodes are no longer joined to a full node, they should be removed
		# and the communication pattern updated
		trim_nodes(refine_grid)

	# Return the refined grid
	return refine_grid