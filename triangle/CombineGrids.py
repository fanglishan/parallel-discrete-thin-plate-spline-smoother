#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 13:33:58

This files contains the routine that combines subgrids into 
the original grid.

"""

# Import other classes
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Grid import Grid
from function.FunctionStore import zero
from grid.Edge import Edge

# Import libraries
from copy import copy


def add_edge(grid, id1, id2, refine_type, refine_level, \
		location, boundary_function = zero):
	""" Add two edges to tdhe grid """

	# Create the first edge from 1 to 2
	edge1 = Edge(id1, id2)

	# Set location for the edge
	edge1.set_location(location)

	# Set refine type for the edge
	edge1.set_refine_type(refine_type)

	# Set the boundary function for the edge
	edge1.set_boundary_function(boundary_function)
	
	# Set refine level as 0 since no refinement yet
	edge1.set_refine_level(refine_level)
	
	# Copy the frist edge with all its information
	edge2 = copy(edge1)

	# Replace start node 1 with node 2
	edge2.set_endpt1(id2)

	# Replace end node 2 with node 1
	edge2.set_endpt2(id1)

	# Add edge 1 to the grid
	grid.add_edge_all(edge1)

	# Add edge 2 to the grid
	grid.add_edge_all(edge2)


def add_subgrid_edges(grid, subgrid):
	""" Add nodes from the subgrid to the grid """

	# Loop over the full nodes in the grid
	for node in node_iterator(subgrid):
		
		# Node id of the node
		node_id = node.get_node_id()

		# Loop over all of the endpoints of an edge joined to the
		# current node
		for endpt in endpt_iterator(subgrid, node_id):

			# Obtain new grid local id 1 by global id 1
			id1 = grid.get_local_id(node.get_global_id())

			# If the endpoint is in the full node table
			if subgrid.is_in(endpt):

				# Obtain global node id
				global_id = subgrid.get_global_id(endpt)

			# If the endpoint is in the ghost node table
			elif subgrid.reference_ghost_table().is_in(endpt):

				# Obtain global node id
				global_id = subgrid.reference_ghost_table().get_global_id(endpt)

			# Obtain new grid local id 2 by global id 2
			id2 = grid.get_local_id(global_id)

			# If no edge has been added between these two nodes
			if not grid.is_edge(id1, id2):

				# Edge refine type in the subgrid
				refine_type = subgrid.get_refine_type(node_id, endpt)

				# Edge refine level in the subgrid
				refine_level = subgrid.get_refine_level(node_id, endpt)

				# Edge location info in the subgrid
				location = subgrid.get_location(node_id, endpt)

				# Edge boundary function in the subgrid
				boundary_function = subgrid.get_boundary_function(node_id, endpt)
			
				# Add two edges between two nodes
				add_edge(grid, id1, id2, refine_type, refine_level, \
						location, boundary_function)


def add_subgrid_nodes(grid, subgrid):
	""" Add nodes from the subgrid to the grid """

	# Loop over the full nodes in the grid
	for node in node_iterator(subgrid):
		
		# If the node has not been added
		# Normally full nodes will not be in two subgrids
		if not grid.is_in_global(node.get_global_id()):

			# Global id of the node
			global_id = node.get_global_id()

			# Coordinate of the node
			coord = node.get_coord()

			# Whether the node is a boundary node
			slave = node.get_slave()

			# Value of the node
			value = node.get_value()

			# Create a new node
			new_node = grid.create_node(global_id, coord, slave, value)

			# Add the new node to the grid
			grid.add_node(new_node)


def combine_subgrids(subgrid_list):
	""" Combine input subgrids"""
	
	# Initialise the grid
	grid = Grid()

	# Iterate through the subgrids
	for i in range(0,len(subgrid_list)):
			
		# Add nodes of the subgrid
		add_subgrid_nodes(grid, subgrid_list[i])

	# Iterate through the subgrids
	for i in range(0,len(subgrid_list)):

		# Add edges of the subgrid
		add_subgrid_edges(grid, subgrid_list[i])


	return grid