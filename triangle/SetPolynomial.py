#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-19 11:11:57
@Last Modified time: 2019-04-21 22:39:02

This class contains routines to create polynomials using 
data points in the triangle.

"""

# Import other classes
from triangle.LinearPolynomial import LinearPolynomial

# Import libraries
from math import fabs
import numpy as np


def set_polynomial_linear_2D(node1, node2, node3):
	""" Construct a 2D linear polynomial """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
			
	# Get the coordinates of the three vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()
	coord3 = node3.get_coord()
	
	# Break down the information to make the code easier to read
	x1 = coord1[0]
	y1 = coord1[1]
	x2 = coord2[0]
	y2 = coord2[1]
	x3 = coord3[0]
	y3 = coord3[1]

	# Find h
	division = (y1-y2)*(x2-x3)-(y2-y3)*(x1-x2);
	
	# Avoid division by zero errors
	assert fabs(division) > 1.0E-12, "divide by zero in set_polynomial_linear_2D"

	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_const((x3*y2 - y3*x2)/division)
	poly.set_x((y3-y2)/division)
	poly.set_y((x2-x3)/division)
	
	# Return the polynomial
	return poly


def set_polynomial_linear_solution_2D(node1, node2, node3, value1, value2, value3):
	""" Construct a 2D linear polynomial using given values """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
	
	# Get the coordinates of the three vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()
	coord3 = node3.get_coord()
	
	# Solve the system of equations
	matrix = np.array([[coord1[0],coord1[1],1.0],[coord2[0],coord2[1],1.0],[coord3[0],coord3[1],1.0]])
	vector = np.array([[value1],[value2],[value3]])
	coef = np.linalg.solve(matrix, vector)

	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_const(coef[2][0])
	poly.set_x(coef[0][0])
	poly.set_y(coef[1][0])
	
	# Return the polynomial
	return poly