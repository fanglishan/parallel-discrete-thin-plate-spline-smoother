#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 13:41:21

This class offers a number of numerical quadrature routines to integrate
a function over a triangle. Currently only offers quadrature routines that
are exact for linear functions.

There are generally two types of rountines, one that integrates a 
polynomial*polynomial and one that integrates a function*polynomial.

"""

# Import libraries
from numpy import array
from math import fabs


def calc_area(coord1, coord2, coord3):
	"""Calculate the area of the triangle"""

	return 0.5 * fabs((coord2[0]-coord1[0])*(coord3[1]-coord1[1])
		- (coord3[0]-coord1[0])*(coord2[1]-coord1[1]))     


def linear_integrate(poly1, poly2, node1, node2, node3): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
			
	# Get the coordinates of the 3 vertices
	coord1 = array(node1.get_coord())
	coord2 = array(node2.get_coord())
	coord3 = array(node3.get_coord())
	
	# Find the point at the centre of the triangle
	centre_point = (coord1+coord2+coord3)/3.0
	x = centre_point[0]
	y = centre_point[1]
	
	# Apply the quadrature rule    
	return  poly1.eval(x, y) * poly2.eval(x, y) * \
		calc_area(coord1, coord2, coord3)
		
def linear_func_integrate(rhs, poly, node1, node2, node3): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
	 and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"

	# Get the coordinates of the 3 vertices
	coord1 = array(node1.get_coord())
	coord2 = array(node2.get_coord())
	coord3 = array(node3.get_coord())
	
	# Find the point at the centre of the triangle
	centre_point = (coord1+coord2+coord3)/3.0
	x = centre_point[0]
	y = centre_point[1]
	
	# Apply the quadrature rule    
	return  rhs(centre_point) * poly.eval(x, y) * \
		calc_area(coord1, coord2, coord3)