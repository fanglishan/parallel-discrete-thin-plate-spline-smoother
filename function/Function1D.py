#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 13:22:29
@Last Modified time: 2019-04-16 14:08:32

This class contains a list of 1D problem functions.
The input of the functions is list of length 1.

"""

# Import libaries
from math import pi, sin, exp, cos


""" Linear function x_0 """
def lin_u(x):
	return x[0]
 
def lin_ux(x):
	return 1

def lin_uxx(x):
	return 0


""" Square function x_0^2 """
def sqr_u(x):
	return x[0]*x[0]
 
def sqr_ux(x):
	return 2*x[0]

def sqr_uxx(x):
	return 2


""" Cubic function x_0^3 """
def cub_u(x):
	return x[0]**3
 
def cub_ux(x):
	return 3*x[0]*x[0]

def cub_uxx(x):
	return 6*x[0]


""" Exponential function exp(x_0) """
def exp_u(x):
	return exp(x[0])

def exp_ux(x):
	return exp(x[0])

def exp_uxx(x):
	return exp(x[0])


""" Sine function sin(pi*x_0) """
def sin_u(x):
	return sin(pi*x[0])
	
def sin_ux(x):
	return pi*cos(pi*x[0])

def sin_uxx(x):
	return -pi*pi*sin(pi*x[0])


""" Oscillatory sine function sin(a*pi*x_0) """
a = 5
def oscisin_u(x):
	return sin(a*pi*x[0])
	
def oscisin_ux(x):
	return a*pi*cos(a*pi*x[0])

def oscisin_uxx(x):
	return -a*a*pi*pi*sin(a*pi*x[0])


""" Steep exponential function exp(-a*x_0*x0)exp(-a*x_1*x1) """
a = 30
def stexp_u(x):
	return exp(-a*(x[0]-0.5)**2)

def stexp_ux(x):
	return (-2*a*x[0]+a)*exp(-a*(x[0]-0.5)**2)

def stexp_uxx(x):
	return 2*a*exp(-a*(x[0]-0.5)**2)-(-2*a*x[0]+a)**2*exp(-a*(x[0]-0.5)**2)
