#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 13:22:29
@Last Modified time: 2019-04-17 03:59:26

This class contains a list of 2D problem functions.
The input of the functions is list of length 2.

"""

# Import libaries
from math import pi, sin, exp, cos


""" Linear function x0+x1 """
def lin_u(x): 
	return x[0]+x[1]

def lin_ux(x): 
	return 1

def lin_uy(x): 
	return 1

def lin_uxxyy(x): 
	return 0


""" Square function x_0^2+x_1^2 """
def sqr_u(x): 
	return x[0]*x[0]+x[1]*x[1]
 
def sqr_ux(x): 
	return 2*x[0]

def sqr_uy(x): 
	return 2*x[1]
			
def sqr_uxxyy(x): 
	return 4


""" Cubic function x_0^3+x_1^3 """
def cub_u(x):
	return x[0]**3+x[1]**3
 
def cub_ux(x):
	return 3*(x[0]**2)

def cub_uy(x):
	return 3*(x[1]**2)

def cub_uxxyy(x):
	return 6*x[0]+6*x[1]


""" Exponential function exp(x_0)exp(x_1) """
def exp_u(x):
	return exp(x[0])*exp(x[1])

def exp_ux(x):
	return exp(x[0])*exp(x[1])
   
def exp_uy(x):
	return exp(x[0])*exp(x[1])
	
def exp_uxxyy(x):
	return 2.0*exp(x[0])*exp(x[1])


""" Sine function sin(pi*x_0)sin(pi*x_1) """
def sin_u(x):
	return sin(pi*x[0])*sin(pi*x[1])
	
def sin_ux(x):
	return pi*cos(pi*x[0])*sin(pi*x[1])

def sin_uy(x):
	return pi*sin(pi*x[0])*cos(pi*x[1])
	
def sin_uxxyy(x):
	return 2.0*pi*pi*sin(pi*x[0])*sin(pi*x[1])# DELETE LATER -2.0*pi*pi*sin(pi*x[0])*sin(pi*x[1])


""" Oscillatory sine function sin(a*pi*x_0)sin(a*pi*x_1) """
a = 1
def oscisin_u(x):
	return sin(a*pi*x[0])*sin(a*pi*x[1])
	
def oscisin_ux(x):
	return a*pi*cos(a*pi*x[0])*sin(a*pi*x[1])

def oscisin_uy(x):
	return a*pi*sin(a*pi*x[0])*cos(a*pi*x[1])
	
def oscisin_uxxyy(x):
	return -2.0*a*a*pi*pi*sin(a*pi*x[0])*sin(a*pi*x[1])


""" Steep exponential function exp(-a*(x_0-b)**2)exp(-a*(x_0-b)**2) """
a = 30    # size (how steep is the slope)
b = 0.5    # center of dataset
def stexp_u(x):
	return exp(-a*(x[0]-b)**2)*exp(-a*(x[1]-b)**2)

def stexp_ux(x):
	return (-2*a*x[0]+2*a*b)*stexp_c_u(x)

def stexp_uy(x):
	return (-2*a*x[1]+2*a*b)*stexp_c_u(x)

def stexp_uxxyy(x):
	return (-2*a*stexp_c_u(x)+((-2*a*x[0]+2*a*b)**2)*stexp_c_u(x)) + \
		   (-2*a*stexp_c_u(x)+((-2*a*x[1]+2*a*b)**2)*stexp_c_u(x))