#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 13:22:29
@Last Modified time: 2019-04-16 14:36:51

This class manages the functions of the program. The functions will 
take function parameters from the main routine and return corresponding
functions.

"""

# Import other classes
import function.Function1D as function1D
import function.Function2D as function2D
from parameter.ModelParameter import ModelProblem


""" Zero function """
def zero(coord):
	"""Define the function 0"""
	return 0


def obtain_model_problem(model_problem, d):
	""" Obtain the model problem functions """
	
	# For 1D model problems
	if d == 1:

		# Zero
		if model_problem == ModelProblem.zero:
			return [zero, zero, zero]

		# Linear
		elif model_problem == ModelProblem.linear:
			return [function1D.lin_u, function1D.lin_ux, \
				function1D.lin_uxx]

		# Quadratic
		elif model_problem == ModelProblem.quadratic:
			return [function1D.sqr_u, function1D.sqr_ux, \
				function1D.sqr_uxx]

		# Cubic
		elif model_problem == ModelProblem.cubic:
			return [function1D.cub_u, function1D.cub_ux, \
				function1D.cub_uxx]

		# Exponential
		elif model_problem == ModelProblem.exp:
			return [function1D.exp_u, function1D.exp_ux, \
				function1D.exp_uxx]

		# Sine
		elif model_problem == ModelProblem.sine:
			return [function1D.sin_u, function1D.sin_ux, \
				function1D.sin_uxx]

		# Oscillatory sine
		elif model_problem == ModelProblem.osci_sine:
			return [function1D.oscisin_u, function1D.oscisin_ux, \
				function1D.oscisin_uxx]

		# Steep exponential
		elif model_problem == ModelProblem.steep_exp:
			return [function1D.stexp_u, function1D.stexp_ux, \
				function1D.stexp_uxx]

	# For 2D model problems
	elif d == 2:

		# Zero
		if model_problem == ModelProblem.zero:
			return [zero, zero, zero, zero]

		# Linear
		elif model_problem == ModelProblem.linear:
			return [function2D.lin_u, function2D.lin_ux, \
				function2D.lin_uy, function2D.lin_uxxyy]

		# Quadratic
		elif model_problem == ModelProblem.quadratic:
			return [function2D.sqr_u, function2D.sqr_ux, \
				function2D.sqr_uy, function2D.sqr_uxxyy]

		# Cubic
		elif model_problem == ModelProblem.cubic:
			return [function2D.cub_u, function2D.cub_ux, \
				function2D.cub_uy, function2D.cub_uxxyy]

		# Exponential
		elif model_problem == ModelProblem.exp:
			return [function2D.exp_u, function2D.exp_ux, \
				function2D.exp_uy, function2D.exp_uxxyy]

		# Sine
		elif model_problem == ModelProblem.sine:
			return [function2D.sin_u, function2D.sin_ux, \
				function2D.sin_uy, function2D.sin_uxxyy]

		# Oscillatory sine
		elif model_problem == ModelProblem.osci_sine:
			return [function2D.oscisin_u, function2D.oscisin_ux, \
				function2D.oscisin_uy, function2D.oscisin_uxxyy]

		# Steep exponential
		elif model_problem == ModelProblem.steep_exp:
			return [function2D.stexp_u, function2D.stexp_ux, \
				function2D.stexp_uy, function2D.stexp_uxxyy]

	else:
		print "Invalid dimension", d, "or model problem", model_problem