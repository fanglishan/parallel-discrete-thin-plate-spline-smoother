#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-16 14:02:23

All the model problem functions should be included in the FunctionStore
class. This allows workers to access to the functions.

"""

# Import other classes
import function.Function1D as function1D
import function.Function2D as function2D


""" Zero function """
def zero(coord):
	"""Define the function 0"""
	return 0


class FunctionStore:
	""" Store the functions used in the test problems """
	
	# Create an empty list
	_func_list = list()
	
	def __init__(self):
		"""Initialise the function store
		
		The functions used in the test problems should be appended here"""
		
		self._func_list = [zero]
		self._func_list.append(function1D.lin_u)
		self._func_list.append(function1D.sqr_u)
		self._func_list.append(function1D.cub_u)
		self._func_list.append(function1D.exp_u)
		self._func_list.append(function1D.sin_u)
		self._func_list.append(function1D.oscisin_u)
		self._func_list.append(function1D.stexp_u)

		self._func_list.append(function2D.lin_u)
		self._func_list.append(function2D.sqr_u)
		self._func_list.append(function2D.cub_u)
		self._func_list.append(function2D.exp_u)
		self._func_list.append(function2D.sin_u)
		self._func_list.append(function2D.oscisin_u)
		self._func_list.append(function2D.stexp_u)

		
	def is_in_store(self, func):
		"""Is a given function in the store?"""
		return func in self._func_list
		
	def get_index(self, func):
		"""Get the position of the function in the store"""
		assert self.is_in_store(func), \
			"function not in store "
		return self._func_list.index(func)
		
	def get_function(self, index):
		"""Get the function that is in the given position in the store"""
		assert index >= 0 and index < len(self._func_list), \
			"index out of range "
		return self._func_list[index]