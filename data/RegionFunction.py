#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-18 11:31:49
@Last Modified time: 2019-04-21 23:29:42

This class contains rountines to access data regions, including building
a data region, search data from a data region. It assumes the dimensions
of all the data points are consistent.

"""

# Import other classes
from data.Region import Region
from grid.NodeTable import node_iterator


def obtain_region_index(region, coord):
	""" Find the region in the mesh for the given coord """

	# Region parameters
	region_coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()

	# Deterimine the index of the region
	index = []
	for i in range(0, len(coord)):

		position = int((coord[i]-region_coord[i])/region_size[i])

		# Print warning message
		if position < 0 or position > region_no[i]:
			print "Warning: data out of domain"
			return [-1]

		index.append(position)

	return index


def add_mesh_data(data, region):
	""" Assign data points to the mesh """

	# Region parameters
	dim = region.get_dim()
	mesh = region.get_mesh()

	# Iterate through each data point
	for point in data:

		# Determine region index for the coordinate
		coord = point[:-1]
		index = obtain_region_index(region, coord)

		# Skip this point if region is not found
		if index[0] == -1:
			continue

		# Add data point to the mesh
		if dim == 1:
			mesh[index[0]].append(point)
		elif dim == 2:
			mesh[index[0]][index[1]].append(point)
		elif dim == 3:
			mesh[index[0]][index[1]].append(point)


def initialise_mesh(dim, region_no):
	""" Initialise a data mesh """

	# 1D data mesh
	if dim == 1:
		mesh = [[] for x in xrange(region_no[0])]

		# Reset lists
		for i in range(region_no[0]):
			mesh[i] = []

	# 2D data mesh
	elif dim == 2:
		mesh = [[[]]*region_no[1] for x in xrange(region_no[0])]

		# Reset lists
		for i in range(region_no[0]):
			for j in range(region_no[1]):
				mesh[i][j] = []

	# 3D data mesh
	elif dim == 3:
		mesh = [[[[] for x in xrange(region_no[2])]]*region_no[1] for x in xrange(region_no[0])]
		
		# Reset lists
		for i in range(region_no[0]):
			for j in range(region_no[1]):
				for k in range(region_no[2]):
					mesh[i][j][k] = []	

	else:
		print "Warning: invalid dimennsion"
		return

	return mesh


def build_region(data, region_no, min_coord=None, max_coord=None):
	""" Build a new data region """

	# Check empty dataset
	if len(data) == 0:
		print "Warning: empty dataset"
		return

	# Dimension of data
	dim = len(data[0])-1

	# Check input
	if len(region_no) != dim:
		print "Warning: invalid number of regions"
		return

	# Initialise a data mesh
	mesh = initialise_mesh(dim, region_no)

	# Set start and end coordinates if it is not specified
	if min_coord == None:
		min_coord = [0.0]*dim
	if max_coord == None:
		max_coord = [1.0]*dim

	# Set region sizes
	region_size = []
	for i in range(0, dim):
		region_size.append((max_coord[i]-min_coord[i])/region_no[i])

	# Build a data region
	region = Region(dim, mesh, [], min_coord, region_no, region_size, len(data))

	# Assign data to the data mesh
	add_mesh_data(data, region)

	# Initialise usage of data mesh
#	region.init_usage()

	return region


def obtain_subregion(region, sub_grid):
	""" Obtain a subregion for the sub_grid """

	# Initialise max and min coordinates
	dim = region.get_dim()
	mesh = region.get_mesh()
	region_coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()
	max_coord = [-100000000.0]*dim
	min_coord = [100000000.0]*dim

	# Check coordinates of each full node
	for node in node_iterator(sub_grid):
		coord = node.get_coord()

		# Compare and replace max and min coordinates
		for i in range(0, len(coord)):
			if coord[i] > max_coord[i]:
				max_coord[i] = coord[i]
			if coord[i] < min_coord[i]:
				min_coord[i] = coord[i]

	# Check coordinates of each ghost node
	for node in node_iterator(sub_grid.reference_ghost_table()):
		coord = node.get_coord()

		# Compare and replace max and min coordinates
		for i in range(0, len(coord)):
			if coord[i] > max_coord[i]:
				max_coord[i] = coord[i]
			if coord[i] < min_coord[i]:
				min_coord[i] = coord[i]

	# Find element of the given coordiantes
	max_index = obtain_region_index(region, max_coord)
	min_index = obtain_region_index(region, min_coord)

	# Number of regions for the sub region
	new_region_no = []
	for i in range(0, len(max_index)):

		if (max_index[i]-min_index[i]) <= 0:
			print "Warning: invalid region_no"

		new_region_no.append(max_index[i]-min_index[i])

	# Start coordinates for the subregion
	new_coord = []
	for i in range(0, dim):
		new_coord.append(region_coord[i]+min_index[i]*region_size[i])

	# Data mesh for the subregion
	new_mesh = []
	if dim == 1:
		pass
	elif dim == 2:
		for i in range(0, new_region_no[0]):
			new_mesh.append([])
			for j in range(0, new_region_no[1]):
				new_mesh[i].append(mesh[i+min_index[0]][j+min_index[1]])
	elif dim == 3:
		pass
	else:
		print "Warning: invalid mesh dimension"
	
	# Create a new subregion
	sub_region = Region(dim, new_mesh, [], new_coord, new_region_no, region_size, region.get_data_size()) 

	return sub_region


def obtain_subregions(region, sub_grids):
	""" Obtain a list of subregions for the sub grids """

	subregions = []
	for i in range(0, len(sub_grids)):
		subregions.append(obtain_subregion(region, sub_grids[i][0]))


