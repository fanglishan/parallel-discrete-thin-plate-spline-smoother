#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-19 10:23:33
@Last Modified time: 2019-04-19 10:45:33

This class contains routines to choose data that is inside
the given element.

"""

from triangle.SetPolynomial import set_polynomial_linear_2D


def choose_data_2D(data, signal, node1, node2, node3, tol=0.0):
	""" Obtain a set of data points for triangle 1-2-3 """

	# Initialise a data list
	chosen_data = []

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Iterate through each data point in the triangle and add to the list
	for i in range(0, len(data)):
		if signal[i]:
			continue

		x1 = data[i][0]
		x2 = data[i][1]

		# Add data points that are in the triangle
		if poly1.eval(x1, x2)>=tol and poly2.eval(x1, x2)>=tol and poly3.eval(x1, x2)>=tol:
			chosen_data.append(data[i])
			signal[i] = True
				
	return chosen_data
