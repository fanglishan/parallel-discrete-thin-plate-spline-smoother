#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-21 23:24:32
@Last Modified time: 2019-04-21 23:49:43

This class contains routines to select data in elements from
a data region.

"""

# Import other classes
from data.RegionFunction import obtain_region_index
from triangle.SetPolynomial import set_polynomial_linear_2D


def choose_data_1d(data, signal, node1, node2):
	""" Obtain a set of data points for interval 1-2 """
	
	# Initialise a data list
	chosen_data = []

	# Iterate through each data point in the interval and add to the list
	for i in range(0, len(data)):
		if signal[i]:
			continue

		point = data[i][0]
		x1 = node1.get_coord()[0]
		x2 = node2.get_coord()[0]

		# Add data points that are in the interval
		if ((point >= x1) and (point <= x2)) or \
			((point >= x2) and (point <= x1)):
			chosen_data.append(data[i])
			signal[i] = True
				
	return chosen_data


def choose_data_2d(data, data_usage, node1, node2, node3, tol=0.0):
	""" Obtain a set of data points for triangle 1-2-3 """

	# Initialise a data list
	chosen_data = []

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Iterate through each data point in the triangle and add to the list
	for i in range(0, len(data)):
		if signal[i]:
			continue

		x1 = data[i][0]
		x2 = data[i][1]

		# Add data points that are in the triangle
		if poly1.eval(x1, x2)>=tol and poly2.eval(x1, x2)>=tol and poly3.eval(x1, x2)>=tol:
			chosen_data.append(data[i])
			data_usage[i] = True
				
	return chosen_data


def obtain_data(region, element):
	""" Find the data points in a given element """

	# Parameters
	dim = region.get_dim()
	mesh = region.get_mesh()
	usage = region.get_usage()

	# Initialise max and min coordinates
	max_coord = [-100000000.0]*dim
	min_coord = [100000000.0]*dim

	# Check dimension consistency
	if region.get_dim() != len(element):
		print "Warning: inconsistent dimension"
		return

	# Element is assumed to be a list of nodes
	for node in element:
		coord = node.get_coord()
		for i in range(0, len(coord)):

			# Compare and replace max and min coordinates
			if coord[i] > max_coord[i]:
				max_coord[i] = coord[i]
			if coord[i] < min_coord[i]:
				min_coord[i] = coord[i]

	# Find element of the given coordiantes
	max_index = obtain_region_index(region, max_coord)
	min_index = obtain_region_index(region, min_coord)

	chosen_data = []
	if dim == 2:

		# Nodes
		node1 = element[0]
		node2 = element[1]
		node3 = element[2]

		# Iterate through the selected mesh sections
		for i in range(min_index[0], max_index[0]):
			for j in range(min_index[1], max_index[1]):

				data = mesh[i][j]
				data_usage = usage[i][j]

				chosen_data += choose_data_2d(data, data_usage, node1, node2, node3)


	return chosen_data