#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 22:30:35
@Last Modified time: 2019-04-16 22:32:18

This class contains rountines for building 1D datasets. 

"""

# Import libaries
import random


def create_uniform_data(n, u, noise, no_outlier=0, size_outlier=0.0):
	""" Create 1D uniform data """

	# Range of domain
	start = 0.0
	end = 1.0

	# Initialise a list for the data points in the grid 
	data = []

	# Interval length between data points
	interval = (end-start)/n

	# Coordinate of the point
	coord = start + 0.0

	# Iterate through data points in the x direction
	for i in range(0, n):

		# Set x, y coordinates and z value of the data point
		data_point = [coord, u([coord]) + random.uniform(-noise, noise)]

		# Add the data point to the list
		data.append(data_point)

		# Update x coordinate
		coord += interval

	# Create a number of outliers
	for i in range(0, no_outlier):

		# Randomly choose one data point
		data_point = random.choice(data)

		# Recompute the data value
		data_point[1] = u([data_point[0]]) + random.uniform(-size_outlier, size_outlier)

	return data


def create_uniform_data_hole(n, u, noise, no_outlier=0, size_outlier=0.0, hole_start=0.5, hole_end=0.5):
	""" Create 1D uniform data with a data free region """

	# Create uniformly distributed data points
	uniform_data = create_uniform_data(n, u, noise, no_outlier, size_outlier)

	# Initialise a list for data
	data = []

	# Iterate through each data point
	for i in range(0, len(uniform_data)):

		# If the data point is outside the hole, add it to the list
		if uniform_data[i][0] < hole_start or uniform_data[i][0] > hole_end:
			data.append(uniform_data[i])
	
	return data

