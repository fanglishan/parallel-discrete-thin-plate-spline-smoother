#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 23:35:59
@Last Modified time: 2019-04-17 04:07:11

This class contains routines to extract information from dataset
names.

Dataset name
1:dimension
2:model problem
3:distribution
4:noise level
5:data size

"""

# Import other libraries
from file.DataFile import read_file
from function.Function2D import lin_u, sqr_u, cub_u, exp_u, \
	sin_u, oscisin_u, stexp_u
from parameter.ModelParameter import ModelProblem


def obtain_dimension(dataset):
	""" Return the dimension of dataset """

	# Dimension
	d = int(str(dataset)[0])

	# Print error message
	if d != 1 and d != 2 and d!= 3:
		print "Warning: DatasetManager - unknown dataset"

	return d


def set_model(dataset):
	""" set the model problem of the datset """
	
	model_problem = int(str(dataset)[1])

	# Return corresponding model problem
	if model_problem == ModelProblem.linear:
		return lin_u
	elif model_problem == ModelProblem.quadratic:
		return sqr_u
	elif model_problem == ModelProblem.cubic:
		return cub_u
	elif model_problem == ModelProblem.exp:
		return exp_u
	elif model_problem == ModelProblem.sine:
		return sin_u
	elif model_problem == ModelProblem.osci_sine:
		return oscisin_u
	elif model_problem == ModelProblem.steep_exp:
		return stexp_u
	else:
		print "Warning: invalid model problem"
		return


def set_noise(dataset):
	""" set the noise size of the datset """
	
	# Find noise level from dataset name
	noise_level = int(str(dataset)[3])

	# Return corresponding noise size
	if noise_level == 0:
		return 0.0
	elif noise_level == 1:
		return 0.01
	elif noise_level == 2:
		return 0.03
	elif noise_level == 3:
		return 0.05
	elif noise_level == 4:
		return 0.07
	elif noise_level == 5:
		return 0.1
	else:
		print "Warning: invalid noise level"
		return


def set_size(dataset):
	""" set the data size of the datset """
	
	# Find data size from dataset name
	data_size = int(str(dataset)[4])

	# Return corresponding noise size
	if data_size == 1:
		return 20
	elif data_size == 2:
		return 50
	elif data_size == 3:
		return 100
	elif data_size == 4:
		return 250
	elif data_size == 5:
		return 500
	elif data_size == 6:
		return 1000
	else:
		print "Warning: invalid data size"
		return


def obtain_dataset(dataset):
	""" Return the required dataset """
	# Print error message
	if dataset == 0:
		print "Warning: DatasetManager"

	else:
		return read_file(str(dataset))