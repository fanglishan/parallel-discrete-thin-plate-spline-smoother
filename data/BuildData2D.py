#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-01-22 18:07:35
@Last Modified time: 2019-04-17 04:07:49

This class contains rountines for building 2D datasets. 

"""

# Import libaries
from random import uniform


def create_uniform_data(n, u, noise, no_outlier=0, size_outlier=0.0):
	""" Create 1D uniform data """

	# Range of domain
	x_start = 0.0
	x_end = 1.0
	y_start = 0.0
	y_end = 1.0

	# Initialise a list for the data points in the grid 
	data = []

	# Interval length between data points
	x_interval = (x_end-x_start)/(n+1)
	y_interval = (y_end-y_start)/(n+1)

	# Set initial y coordinate
	y_coord = y_start

	# Iterate through data points in the y direction
	for j in range(0, n):

		# Update y coordinate
		y_coord += y_interval

		# Set initial x coordinate
		x_coord = x_start

		# Iterate through data points in the x direction
		for i in range(0, n):

			# Update x coordinate
			x_coord += x_interval

#			if ((x_coord > 0.15 and x_coord < 0.3) and (y_coord > 0.15 and y_coord < 0.3)) or \
#				((x_coord > 0.45 and x_coord < 0.6) and (y_coord > 0.45 and y_coord < 0.6)) or \
#				((x_coord > 0.65 and x_coord < 0.8) and (y_coord > 0.2 and y_coord < 0.35)):
#				noise = 0.15

			# Set x, y coordinates and z value of the data point
			data_point = [x_coord, y_coord, u([x_coord, y_coord]) + uniform(-noise, noise)]

#			noise = 0.05

			# Add the data point to the list
			data.append(data_point)

	# Create a number of outliers
	for i in range(0, no_outlier):

		# Randomly choose one data point
		data_point = random.choice(data)

		# Recompute the data value
		data_point[2] = u(data_point[:2]) + random.uniform(-size_outlier, size_outlier)
	
	return data


def create_uniform_data_hole(n, u, noise, no_outlier=0, size_outlier=0.0, \
		hole_x_start=0.5, hole_x_end=0.5, hole_y_start=0.5, hole_y_end=0.5):
	""" Create 1D uniform data with a data free region """

	# Create uniformly distributed data points
	uniform_data = create_uniform_data(n, u, noise, no_outlier, size_outlier)

	# Initialise a list for data
	data = []

	# Iterate through each data point
	for i in range(0, len(uniform_data)):

		if not (uniform_data[i][0] > hole_x_start and uniform_data[i][0] < hole_x_end and \
			uniform_data[i][1] > hole_y_start and uniform_data[i][1] < hole_y_end):

			data.append(uniform_data[i])
	
	return data
