#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-17 00:39:23
@Last Modified time: 2019-04-22 12:51:14

The Region class contains data points and necessary parameters to define
the range of the regions.

"""


class Region:
	""" A region that contains data points """

	# Dimension of data
	_dim = 1

	# Data mesh
	_mesh = []

	# Signal for data usage
	_usage = []

	# Start coordinates
	_coord = []

	# Number of regions in each direction
	_region_no = []

	# Size of regions in each direction
	_region_size = []

	# Number of data points
	_data_size = 0
	

	def __init__(self, dim=1, mesh=[], usage=[], coord=[], \
			region_no=[], region_size=[], data_size=0):
		self._dim = dim
		self._mesh = mesh
		self._usage = usage
		self._coord = coord
		self._region_no = region_no
		self._region_size = region_size
		self._data_size = data_size


	def set_dim(self, dim):
		""" Set data dimension """
		self._dim = dim

	def set_mesh(self, mesh):
		""" Set data mesh """
		self._mesh = mesh

	def set_usage(self, usage):
		""" Set usage of data points """
		self._usage = usage

	def init_usage(self):
		""" Initialise usage of data points """
		usage = []	
		for i in range(0, self._region_no[0]):
			usage.append([])
			for j in range(0, self._region_no[1]):
				usage[i].append([False]*len(self._mesh[i][j]))
		self._usage = usage

	def set_coord(self, coord):
		""" Set start coordinates """
		self._coord = coord

	def set_region_no(self, region_no):
		""" Set number of regions """
		self._region_no = region_no

	def set_region_size(self, region_size):
		""" Set size of regions """
		self._region_size = region_size

	def set_data_size(self, data_size):
		""" Set number of data points """
		self._data_size = data_size


	def get_dim(self):
		""" Get data dimension """
		return self._dim

	def get_mesh(self):
		""" Get data mesh """
		return self._mesh

	def get_usage(self):
		""" Get usage of data points """
		return self._usage

	def get_coord(self):
		""" Get start coordinates """
		return self._coord

	def get_region_no(self):
		""" Get number of regions """
		return self._region_no

	def get_region_size(self):
		""" Get size of regions """
		return self._region_size

	def get_data_size(self):
		""" Get number of data points """
		return self._data_size