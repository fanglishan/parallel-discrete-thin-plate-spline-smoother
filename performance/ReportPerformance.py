

from performance.FindIntervalLength import find_interval_length_h
from performance.FindRefineLevel import find_refine_level
from performance.FindTriangle import find_triangle_number, find_angle
from indicator.ErrorIndicator import indicate_error,estimate_true_error

def report_setting(grid,error_tol,max_refine_no,max_refine_sweep,indicator_type,true_soln,rhs):
	print "error tolerance:", error_tol
	print "max # refine each sweep: ", max_refine_no
	print "max # refine sweep:", max_refine_sweep
	
	# Print the error indicator used for the computations

	print "indicator type:",
	if indicator_type == 1:
		print "true error indicator"
	elif indicator_type == 2:
		print "Mitchell's error indicator"
	elif indicator_type == 3:
		print ""
	else:
		print "unknown error indicator"

	print ""

def report_performance(my_worker_no, grid):

	print "Grid information of worker", my_worker_no
	print ""
	
	max_h, min_h = find_interval_length_h(grid)

	print "maximum interval length h:", max_h
	print "minimum interval length h:", min_h

	max_refine, min_refine = find_refine_level(grid)

	print "maximum refine level:", max_refine
	print "minimum refine level:", min_refine

	# Estimate number of triangles in the grid
	num_triangles = find_triangle_number(grid)

	print "number of triangles:", num_triangles

	# Estimate number of interior nodes in the grid
	#num_nodes = find_interior_node_number(grid)

	#print "number of nodes:", num_nodes

	print ""