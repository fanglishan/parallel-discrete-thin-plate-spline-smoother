

def find_refine_level(grid):
	""" Determine the maximum and minimum refinement level of edges in the grid """

	from grid.NodeTable import node_iterator
	from grid.EdgeTable import endpt_iterator

	# Initialise maximum refinement level
	max_refine_level = 0

	# Initialise minimum refinement level 
	min_refine_level = 10000000

	# Iterate through the nodes in the grid
	for node in node_iterator(grid):

		# Iterate through the endpoints connected to the node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Calculate the length of the edge between two nodes
			refine_level = grid.get_refine_level(node.get_node_id(),endpt)

			# Compare the edge length with highest recorded h
			if max_refine_level < refine_level:

				# Add the new value to the list
				max_refine_level = refine_level

			# Compare the edge length with lowest recorded h
			if min_refine_level > refine_level:

				# Add the new value to the list
				min_refine_level = refine_level


	# Iterate through the nodes in the grid
	for node in node_iterator(grid.reference_ghost_table()):

		# Iterate through the endpoints connected to the node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Calculate the length of the edge between two nodes
			refine_level = grid.get_refine_level(node.get_node_id(),endpt)

			# Compare the edge length with highest recorded h
			if max_refine_level < refine_level:

				# Add the new value to the list
				max_refine_level = refine_level

			# Compare the edge length with lowest recorded h
			if min_refine_level > refine_level:

				# Add the new value to the list
				min_refine_level = refine_level
	return max_refine_level, min_refine_level