def calculate_distance_2d(coord1,coord2):
	""" Calcualte the distance in two dimension space """
	import numpy

	# Calculate the length between two nodes in 2d space
	return numpy.sqrt((coord1[0]-coord2[0])**2+(coord1[1]-coord2[1])**2)


def find_interval_length_h(grid):
	""" Calculate the maximum and minimum edge length h of the grid """

	from grid.NodeTable import node_iterator
	from grid.EdgeTable import endpt_iterator

	# Initialise maximum h 
	max_h = 0

	# Initialise minimum h 
	min_h = 10000000

	# Iterate through the nodes in the grid
	for node in node_iterator(grid):

		# Iterate through the endpoints connected to the node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# If the endpoint is a full node
			if grid.is_in(endpt):

				# Calculate the length of the edge between two nodes
				edge_length = calculate_distance_2d(node.get_coord(),grid.get_coord(endpt))

			# If the endpoint is a ghost node
			if grid.reference_ghost_table().is_in(endpt):

				# Calculate the length of the edge between two nodes
				edge_length = calculate_distance_2d(node.get_coord(),grid.reference_ghost_table().get_coord(endpt))

			# Compare the edge length with highest recorded h
			if max_h < edge_length:

				# Add the new value to the list
				max_h = edge_length

			# Compare the edge length with lowest recorded h
			if min_h > edge_length:

				# Add the new value to the list
				min_h = edge_length


	# Iterate through the nodes in the grid
	for node in node_iterator(grid.reference_ghost_table()):

		# Iterate through the endpoints connected to the node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# If the endpoint is a full node
			if grid.is_in(endpt):

				# Calculate the length of the edge between two nodes
				edge_length = calculate_distance_2d(node.get_coord(),grid.get_coord(endpt))

			# If the endpoint is a ghost node
			if grid.reference_ghost_table().is_in(endpt):

				# Calculate the length of the edge between two nodes
				edge_length = calculate_distance_2d(node.get_coord(),grid.reference_ghost_table().get_coord(endpt))

			# Compare the edge length with highest recorded h
			if max_h < edge_length:

				# Add the new value to the list
				max_h = edge_length

			# Compare the edge length with lowest recorded h
			if min_h > edge_length:

				# Add the new value to the list
				min_h = edge_length


	return max_h, min_h