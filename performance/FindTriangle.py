# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the refinement routines for a triangular grid. The
main routine of interest is uniform_refinement, the rest are helper
routines.

"""


from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet


def find_two_triangles(grid,edge):


	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)
		
	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			# See if we can find a second, different, triangle

			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):

					# Indicate the second triangle has been found
					return True

			return False


def find_triangle_number(grid):
	""" Estimate the number of triangles in the given grid """

	# Initialise edge count for triangles
	triangle_edge_count = 0

	# Loop over the full nodes
	for node in node_iterator(grid):
		
		# The node id of the node
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the full node
		for endpt in endpt_iterator(grid, node_id):
			
			edge = grid.get_edge(node_id,endpt)
			
			if find_two_triangles(grid,edge):

				# 2 for interior edges since it neighbours two triangles
				triangle_edge_count += 2

			# If the refinement type is a base type
			else:

				# 2 for boundary edges since it neighbours two triangles
				triangle_edge_count += 1					


	# Loop over the full nodes
	for node in node_iterator(grid.reference_ghost_table()):
		
		# The node id of the node
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the full node
		for endpt in endpt_iterator(grid, node_id):
			
			edge = grid.get_edge(node_id,endpt)
			
			if find_two_triangles(grid,edge):

				# 2 for interior edges since it neighbours two triangles
				triangle_edge_count += 2

			# If the refinement type is a base type
			else:

				# 2 for boundary edges since it neighbours two triangles
				triangle_edge_count += 1	

	# Divide the edge count by 6 since every triangle has three edges
	# and each edge in the grid has been visited twice
	return triangle_edge_count/6


def find_angle(grid):
	""" Estimate the maximum and minimum angles of triangles in the grid """
	pass