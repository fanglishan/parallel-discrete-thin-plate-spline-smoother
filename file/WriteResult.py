#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-02-22 08:36:33
@Last Modified time: 2019-04-21 00:10:25

This class contains routines to write the results of the TPSFEM
solution, including errors and grids to a file.

"""

# Import other classes
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet
from file.FigureFunction import save_grid_figure
from file.GridFunction import write_grid

# Import libraries
import time
import matplotlib.pyplot as plt


def write_result(grid, parameter): #, grid_error_1, grid_error_2):
	""" Write the results of the TPSFEM program """

	# Check whether needs to write results
	if not parameter.get_write_file():
		return

	# Write results to a file
	file_name = parameter.get_folder() + folder_name + "result" + ".txt"
	text_file = open(file_name, "w")
	
	text_file.write(str(parameters))
	text_file.write("")

#	text_file.write(str(grid_error_1))
#	if parameters.get_solution_type() == 2:
#		text_file.write(str(grid_error_2))

	text_file.close()


	# Save the plot of the TPSFEM grid
#	save_grid_figre(grid, parameter)

	# Write the grid
#	write_file(grid, str(parameters.get_dataset()), \
#		"smoother/", str(parameters.get_indicator()))

	print "Results written"