#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-20 14:25:56
@Last Modified time: 2019-04-20 14:28:18

This class contains a routine to create a folder in the result 
folder to store results from the program.

"""

# Import libraries
import os


def create_folder(directory):
	""" Creata a folder """

	try:
		if not os.path.exists(directory):
			os.makedirs(directory)
	except OSError:
		print ('Error: Creating directory. ' +  directory)