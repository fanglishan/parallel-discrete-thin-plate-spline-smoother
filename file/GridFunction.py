#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-20 21:57:09
@Last Modified time: 2019-04-20 22:46:50

This class contains routines to write or read a grid from a file
(currently support csv file).

"""


# Import other classes
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator

# Import libaries
import csv


def write_grid(grid, parameter):
	""" Write a grid to a csv file """

	# File path
	path = parameter.get_folder() + filename + "_" + str(grid.get_no_nodes()) + ".csv"

	# Open a csv file
	grid_file = open(path, 'w')

	grid_info = []

	# Grid dimension
	d = grid.get_dim()

	# Add dimension of the grid
	grid_info.append([d])

	# Add number of nodes
	grid_info.append([grid.get_no_nodes()])

	# Add nodes id, coordinate and value
	for node in node_iterator(grid):

		row = []

		# Add node id
		row.append(str(node.get_node_id()))

		# Add coordinate
		for i in range(0, d):
			row.append(node.get_coord()[i])

		# Add value
		row.append(node.get_value())

		# Add slave
		if node.get_slave():
			row.append(0)
		else:
			row.append(1)

		# Add derivative values
		d_values = node.get_d_values()
		for i in range(0, len(d_values)):
			row.append(d_values[i])

		grid_info.append(row)

	# Add edges (two node id)
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id): 
			if node_id > endpt:

				row = [str(node_id), str(endpt)]

				row.append(grid.get_location(node_id, endpt))

				row.append(grid.get_refine_type(node_id, endpt))

				row.append(grid.get_error_indicator(node_id, endpt))

				row.append(grid.get_refine_level(node_id, endpt))

				grid_info.append(row)

	# Write data
	with grid_file:
		writer = csv.writer(grid_file)
		writer.writerows(grid_info)

	print "Grid written"


def read_grid(filename, folder):
	""" Read TPSFEM grid from a csv file """

	from grid.Grid import Grid
	from grid.GlobalNodeID import GlobalNodeID
	from build.BuildSquare import add_edge
	from function.FunctionStore import zero

	# Initialise a list for data points
	grid = Grid()

	grid_info = []

	# Open data file
	with open(folder+filename+".csv") as csvfile:

		# Read numeric fields
		reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)

		# Read each row as a list
		for row in reader:	
			grid_info.append(row)

	# Dimension of the grid
	d = int(grid_info[0][0])

	# Number of nodes
	m = int(grid_info[1][0])

	# Set default global id (carry no meaning)
	global_id = GlobalNodeID(0,0)

	# Go through each node
	for i in range(0, m):

		row_number = i+2

		row = grid_info[row_number]

		node_id = row[0]

		# Read coord
		coord = []
		for j in range(1, d+1):
			coord.append(row[j])

		value = row[d+1]

		if row[d+2] == 0:
			slave = True
		else:
			slave = False

		# Set up a node
		node = grid.create_node(global_id, coord, slave, value)

		node.set_d_values(row[d+3:])

		# Modify node id number
		node_id1 = node.get_node_id()
		node_id1.set_no(int(node_id))
		node.set_node_id(node_id1)

		grid.add_node(node)

	# The rest are edges
	no_edges = len(grid_info)-2-m

	row_number = m + 2

	# Add each edge to the grid
	for i in range(0, no_edges):

		row = grid_info[row_number]

		node1 = grid.get_node_ref(str(int(row[0])))
		node2 = grid.get_node_ref(str(int(row[1])))

		location = int(row[2])

		refine_type = int(row[3])

		error_indicator = float(row[4])

		refine_level = int(row[5])
		

		add_edge(grid, node1.get_node_id(), node2.get_node_id(), \
					refine_type, location, zero)

		grid.set_refine_level(node1.get_node_id(), node2.get_node_id(), refine_level)
		grid.set_refine_level(node2.get_node_id(), node1.get_node_id(), refine_level)

		grid.set_error_indicator(node1.get_node_id(), node2.get_node_id(), error_indicator)
		grid.set_error_indicator(node2.get_node_id(), node1.get_node_id(), error_indicator)

		row_number += 1

	return grid

