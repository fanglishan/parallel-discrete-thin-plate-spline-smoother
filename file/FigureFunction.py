#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-20 21:57:17
@Last Modified time: 2019-04-21 00:32:31

This class contains routines to save a figure to a file.

"""

# Import other classes
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet
from plot.FemPlot2D import plot_fem_grid_2D, plot_fem_subgrid_2d

# Import other classes
import matplotlib.pyplot as plt

	

def save_grid_figure(grid, parameter):
	""" Plot the finite element grid with highl-lighted bases """

	# Parameters
	folder = parameter.get_folder()
	dataset = parameter.get_dataset()
	processor = parameter.get_processor()
	iteration = parameter.get_iteration()

	# Initialise a figure
	plt.figure("tpsfem_grid_"+str(dataset)+"_"+str(grid.get_no_nodes()))
	
	# Plot 1D grids
	if grid.get_dim() == 1:
		pass

	# Plot 2D grids
	elif grid.get_dim() == 2:
			
		plot_fem_subgrid_2d(grid, processpr)

	# Save figure
	plt.savefig(folder+"grid_"+str(processor)+"_"+str(iteration)+'.png')
