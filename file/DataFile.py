#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 14:40:17
@Last Modified time: 2019-04-17 04:10:10

This class contains functions to write or read from data files (csv).

"""


# Import libaries
import csv


def write_file(data, filename, folder="dataset/"):
	""" Write data into a file in .csv """
 
 	# Open data file
	data_file = open(folder+filename + ".csv", 'w')

	# Write data
	with data_file:
		writer = csv.writer(data_file)
		writer.writerows(data)
	 
	print("Dataset written")


def read_file(filename, folder="dataset/"):
	""" Read data from a csv file """

	# Initialise a list for data points
	data = []

	# Open data file
	with open(folder+filename+".csv") as csvfile:

		# Read numeric fields
		reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)

		# Read each row as a list
		for row in reader:
			data.append(row)

	return data

