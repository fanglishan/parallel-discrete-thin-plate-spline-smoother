#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-21 20:50:29

This class contains information about a FEM node.

"""

# Import other classes
from NodeID import NodeID
from GlobalNodeID import GlobalNodeID

# Import libraries
from numpy import array
from copy import copy


class Node:
	""" A finite element node"""
	
	# Node dimension (should match up with the dimension of the
	# node table)
	_dim = 0
	
	# Local node ID
	_node_id = NodeID()
	
	# Global node ID
	_global_id = GlobalNodeID()
	
	# Is the node a slave node (i.e. does it sit on the boundary?)
	_slave = False
	
	# What is the value assigned to the node
	_value = 0.0
	
	# Data load (d entries)
	_load = 0.0
	
	# What are the node's coordinates (size should be same as dim)
	_coord = array([])
   
	# derivative values (2D: dx, dy, dxxyy)
	_d_values = array([])

	# Other parameters (for error indicators)
	_parameters = array([])

	def __init__(self, dim=2, coord=[0, 0], 
				 slave=False, value=0.0, load=0.0):
		""" Initialise a node """
		self._dim = dim
		self._coord = coord
		self._slave = slave
		self._value = value
		self._load = load
		self._node_id = NodeID()
		self._global_id = GlobalNodeID()
		self._parameters = [0]*dim

	def __str__(self):
		""" Output node ID, global ID and node value"""
		return str(self._node_id) + \
			"  (" + str(self._global_id) + ") "+ \
			repr(self._value)
	
	 
	def set_node_id(self, node_id):
		""" Set the local node ID """
		self._node_id = copy(node_id)
	
	def set_global_id(self, global_id):
		""" Set the global node ID """
		self._global_id = copy(global_id)
		
	def set_dim(self, dim):
		""" Set the node dimension """
		self._dim = dim
		
	def set_load(self, load):
		""" Set the load (RHS value) """
		self._load = load
		
	def set_value(self, value):
		""" Set current value """
		self._value = value
	
	def set_slave(self, slave):
		""" Is the node a slave node (T/F) """
		self._slave = slave
		
	def set_coord(self, coord):
		""" Set the coordinates """
		assert self._dim == len(coord), \
			"coordinates should be of length %d"% self._dim 
		self._coord = coord[:]

	def set_d_values(self, d_values):
		"""Set the derivative values"""
		self._d_values = d_values

	def set_parameters(self, parameters):
		""" Set parameters """
		self._parameters = parameters

	def set_parameters_entry(self, entry, i):
		""" Set one entry of the parameters """
		self._parameters[i] = entry


	def get_node_id(self):
		""" Get the local node ID """
		node_id = copy(self._node_id)
		return node_id
		
	def get_global_id(self):
		""" Get the global node ID """
		global_id = copy(self._global_id)
		return global_id 
	
	def get_dim(self):
		""" Get the node dimension """
		return self._dim 
	   
	def get_load(self):
		""" Get the load (data d entry) """
		return self._load
		
	def get_value(self):
		""" Get the node value """
		return self._value
		
	def get_slave(self):
		""" Is the node a slave node (T/F) """
		return self._slave
	
	def get_coord(self):
		""" Get the node coordinates """
		return self._coord[:]
		
	def get_d_values(self):
		""" Get the derivative values """
		return self._d_values

	def get_parameters(self):
		""" Get the the parameters """
		return self._parameters

	def get_parameters_entry(self, i):
		""" Get the the parameters entry """
		return self._parameters[i]   
		