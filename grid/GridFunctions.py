# -*- coding: utf-8 -*-
"""
author: Fang

This file contains several functions that use node id or
global id to obtain node (or its reference) or check if 
the ndoe exists in the grid. It mostly deal with both 
full node table and ghost node table and extracts information
from there.

"""

def find_copy_node(grid,node_id):
	"""
	Find the node with given node id in the grid
	Return a deep copy of the node
	"""

	from copy import deepcopy

	# If the node is in full node table
	if grid.is_in(node_id):

		# Return a deep copy of the node
		return deepcopy(grid.get_node(node_id))

	# If the node is in ghost node table
	elif grid.reference_ghost_table().is_in(node_id):

		# Return a deep copy of the node
		return deepcopy(grid.reference_ghost_table().get_node(node_id))

	else:
		# Print error message and return
		print "find_copy_node: node not found"
		return


def find_node(grid,node_id):
	""" Return the node with given node id in the grid """

	# If the node is in full node table
	if grid.is_in(node_id):

		# Return the node
		return grid.get_node(node_id)

	# If the node is in ghost node table
	elif grid.reference_ghost_table().is_in(node_id):

		# Return the node
		return grid.reference_ghost_table().get_node(node_id)

	else:
		# Print error message and return
		print "find_node: node not found"
		return


def find_global_id(grid, node_id):
	""" find global id using node id """

	# If the node is in full node table
	if grid.is_in(node_id):

		# Return the node's global id
		return grid.get_global_id(node_id)

	# If the node is in ghost node table
	elif grid.reference_ghost_table().is_in(node_id):

		# Return the node's global id
		return grid.reference_ghost_table().get_global_id(node_id)

	else:
		# Print error message and return
		print "find_global_id: node not found"
		return



def check_node_in_grid(grid,node_id):
	"""
	Check if the node is in the grid 
	return True if it is
	"""

	# If the node is in full node table
	if grid.is_in(node_id):
		return True

	# If the node is in ghost node table
	if grid.reference_ghost_table().is_in(node_id):		
		return True

	# return False if the node is not in the grid
	return False


def check_edge_nodes_in_grid(grid, edge):
	""" Check if both endpoints of the edge is in the grid """

	# If node 1 is not in the grid
	if not check_node_in_grid(grid,edge.get_endpt1()):
		return False

	# If node 2 is not in the grid
	if not check_node_in_grid(grid,edge.get_endpt2()):
		return False

	return True


def check_edge_nodes_ghost(grid, edge):
	""" Check if both endpoints of the edge is in the ghost table """

	# If endpoint 1 of the edge is not in the ghost node table
	if not grid.reference_ghost_table().is_in(edge.get_endpt1()):
		return False

	# If endpoint 2 of the edge is not in the ghost node table
	if not grid.reference_ghost_table().is_in(edge.get_endpt2()):
		return False

	return True


def find_node_id_by_global_id(grid, global_id):
	""" Find node ID by input global ID """

	# Check if the global ID is in the full node table
	if grid.is_in_global(global_id):
				
		# Return the node ID
		return grid.get_local_id(global_id)

	# Check if the global ID in the ghost node table
	elif grid.reference_ghost_table().is_in_global(global_id):
				
		# return the node ID
		return grid.reference_ghost_table().get_local_id(global_id)
	
	return

def check_edge_by_global_id_string(node1,node2,string1,string2):
	""" Check if a given edge fits input strings """

	if str(node1.get_global_id()) == string1 and str(node2.get_global_id()) == string2:
		return True
	elif str(node1.get_global_id()) == string2 and str(node2.get_global_id()) == string1:
		return True
	else:
		return False