#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 13:03:48
@Last Modified time: 2019-04-19 09:34:31

This class contains routines to plot data points. The length of all
the data points should be consistent.

"""

# Import libraries
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


def plot_data_1D(data):
	""" Plot 1D data points """

	# Plot data points as green triangles
	for data_point in data:
		plt.plot(data_point[0],data_point[1],"g^")


def plot_data_2D(data):
	""" Plot 2D data points """

	# Initialise a new figure and axis
	fig = plt.figure("2D data points")
	ax = Axes3D(fig)

	# Initialise values of 3D coordinate for the data points
	x_vals = []
	y_vals = []
	z_vals = []

	# Add data coordinates and values to the arrays
	for data_point in data:
		x_vals.append(data_point[0])
		y_vals.append(data_point[1])
		z_vals.append(data_point[2])

	# Create the scatter data points
	ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm)


def plot_data(data):
	""" Plot the given data """

	# Print warning message
	if len(data) == 0:
		print "Input data is empty"
		return

	# Plot data
	if len(data[0]) == 2:
		plot_data_1D(data)
	elif len(data[0]) == 3:
		plot_data_2D(data)
	else:
		print "Warning: invalid data dimension"

	plt.show()