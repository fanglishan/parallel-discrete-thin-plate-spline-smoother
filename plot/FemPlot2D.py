#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-21 00:29:06

This class contains routines to plot the FEM grids.

"""

# Import other classes
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet

# Import libraries
import matplotlib.pyplot as plt


def plot_fem_subgrid_2d(grid, p):
	""" Plot a finite element sub-grid """
	
	plt.figure("Subgrid " + str(p))

	# Loop through edges from full nodes
	for node in node_iterator(grid):
		for endpt in endpt_iterator(grid, node.get_node_id()):
			
			# Check if the end point is a ghost node
			if grid.reference_ghost_table().is_in(endpt):
				
				# Coordinates of two ends
				x = [node.get_coord()[0],grid.reference_ghost_table().get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.reference_ghost_table().get_coord(endpt)[1]]
			else:
				# Coordinates of two ends
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.get_coord(endpt)[1]]

			# Plot the line
			line = plt.plot(x,y)

			# Interior edge with width 1 and colour blue
			if grid.get_location(node.get_node_id(), endpt) == 1:
				plt.setp(line,linewidth=1,color="b")
		
			# Boundary edge with width 3 and colour red
			elif grid.get_location(node.get_node_id(), endpt) == 2:
				plt.setp(line,linewidth=3,color="r")

	# Loop through edges between ghost nodes
	for node in node_iterator(grid.reference_ghost_table()):
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Check if the end point is a ghost node
			if grid.reference_ghost_table().is_in(endpt):
				
				# Coordinates of two ends
				x = [node.get_coord()[0],grid.reference_ghost_table().get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.reference_ghost_table().get_coord(endpt)[1]]

				# Plot the line
				line = plt.plot(x,y)

				# Set the line with width 1 and color blue
				plt.setp(line,linewidth=1,color="m")

	# Plot full nodes as green circles
	for node in node_iterator(grid):
		coord = node.get_coord()
		plt.plot(coord[0],coord[1],"go")

	# Plot full nodes as green dots
	for node in node_iterator(grid.reference_ghost_table()):
		coord = node.get_coord()
		plt.plot(coord[0],coord[1],"g.")


def plot_fem_subgrids_2D(subgrids):
	""" Plot individual sub-grids """

	# Iterate the list and plot individual subgrid
	for i in range(0,len(subgrids)):
		plot_fem_subgrid_2d(subgrids[i][0], i)

	plt.show()

	
def plot_fem_grid_2D(grid):
	""" Plot the finite element grid """

	# Loop through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:

				# Coordinates of two ends
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.get_coord(endpt)[1]]

				# Plot the line
				line = plt.plot(x,y)

				# Interior edge with width 1 and colour blue
				if grid.get_location(node_id, endpt) == 1:
					plt.setp(line,linewidth=1, color="b")
			
				# Boundary edge with width 3 and colour red
				elif grid.get_location(node_id, endpt) == 2:
					plt.setp(line,linewidth=3, color="r")

				# Base edge with width 3 and colour green
				if grid.get_refine_type(node_id, endpt) == 1:
					plt.setp(line,linewidth=3, color="g")

				# Interface base edge with width 2 and colour yellow
				elif grid.get_refine_type(node_id, endpt) == 3:
					plt.setp(line,linewidth=2, color="y")

	# Loop over the nodes in the grid
	for node in node_iterator(grid):

		# Plot boundary nodes as red circles
		if grid.get_slave(node.get_node_id()):
			plt.plot(node.get_coord()[0],node.get_coord()[1],"ro")
		
		# Plot interior nodes as blue circles
		else:
			plt.plot(node.get_coord()[0],node.get_coord()[1],"bo")

	# Show the plot
#	plt.show()