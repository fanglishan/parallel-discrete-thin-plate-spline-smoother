#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-16 15:41:51

This class contains routines to plot the TPSFEM solution.

"""

# Import other classes
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet

# Import libraries
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import griddata
from numpy import mgrid, array


def plot_tpsfem_solution_2d(grid):
	""" Plot the TPSFEM solution """

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []

	# Add node coordinates and values
	for node in node_iterator(grid):
		coord = node.get_coord()
		node_x.append(coord[0])
		node_y.append(coord[1])
		node_v.append(node.get_value())

	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)

	# Initialise the figure
	fig = plt.figure()
	ax = fig.gca(projection ='3d')
	ax = fig.gca()

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():50j,
				 node_y.min(): node_y.max():50j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

	# Make a surface plot
	surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
		cmap=cm.coolwarm, linewidth=0, antialiased=False)
	
	# Set the z axis limits
	ax.set_zlim(node_v.min(), node_v.max())

	# Make the ticks look pretty
	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
	fig.colorbar(surf, shrink=0.5, aspect=5)

	# Show the plot
	plt.show()


def plot_tpsfem_solution_combine_2d(subgrid_list):
	""" Plot the TPSFEM solution using subgrids """

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []

	# Add node coordinates and values
	for subgrid in subgrid_list:
		for node in node_iterator(subgrid):
			coord = node.get_coord()
			node_x.append(coord[0])
			node_y.append(coord[1])
			node_v.append(node.get_value())

	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)

	# Initialise the figure
	fig = plt.figure()
	ax = fig.gca(projection ='3d')
	ax = fig.gca()

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():50j,
				 node_y.min(): node_y.max():50j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

	# Make a surface plot
	surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
		cmap=cm.coolwarm, linewidth=0, antialiased=False)
	
	# Set the z axis limits
	ax.set_zlim(node_v.min(), node_v.max())

	# Make the ticks look pretty
	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
	fig.colorbar(surf, shrink=0.5, aspect=5)

	# Show the plot
	plt.show()