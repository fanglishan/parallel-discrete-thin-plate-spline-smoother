#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-19 09:08:55
@Last Modified time: 2019-04-19 09:55:10

This class contains routines to plot a data region. The data inside
data region should be consistent with the dimension of the region.

"""

# Import libraries
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


def plot_region_1D(region, p):
	""" Plot a 1D data region """
	pass


def plot_region_2D(region, p):
	""" Plot a 2D data region """

	# Figure name
	plt.figure("Data region " + str(p))

	# Region parameters
	mesh = region.get_mesh()
	coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()

	# Plot region vertices
	for i in range(0, region_no[0]+1):
		for j in range(0, region_no[1]+1):
			plt.plot(coord[0]+i*region_size[0],coord[1]+j*region_size[1],"ro")

	# Plot data
	for i in range(0, region_no[0]):
		for j in range(0, region_no[1]):

			for data in mesh[i][j]:
				plt.plot(data[0], data[1], "g^")
	

def plot_region(region, p=0):
	""" Plot a data region """

	# Dimension of data
	dim = region.get_dim()

	# Plot data region
	if dim == 1:
		plot_region_1D(region, p)
	elif dim == 2:
		plot_region_2D(region, p)
	else:
		print "Warning: invalid data region dimension"

	plt.show()


def plot_regions(regions):
	""" Plot a list of data regions """

	for i in range(0, len(regions)):
		plot_region(regions[i])
