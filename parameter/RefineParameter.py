#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 14:11:41
@Last Modified time: 2019-04-16 14:20:40

This class contains a set of classes that define the index of certain
parameter. The classes in this file relates to the refinement.

"""


class IndicatorType:
	""" Indicator types """

	infinity = 1    # Uniform refinement
	true = 2        # True solution


class NormType:
	""" Norm types """
	maximum = 0
	one = 1
	two = 2


	