#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-20 12:18:59
@Last Modified time: 2019-04-20 22:53:13

This class contain parameter related routines. Currently it iniitialises
parameters at the beginning of the program and validate inputs.

"""

# Import other libraries
from parameter.Parameter import Parameter
from function.FunctionManager import obtain_model_problem


def validate_parameter(d, boundary, basis, region_no, model_problem, \
		alpha, solver_tol, dataset, error_tol, refine, uni_refine, \
		max_no, max_sweep, indicator, norm, min_data, write_file, \
		solution_type, solution_grid_size):
	""" Validate input parameters """

	pass


def init_parameter(d, boundary, basis, region_no, model_problem, \
		alpha, solver_tol, dataset, error_tol, refine, uni_refine, \
		max_no, max_sweep, indicator, norm, min_data, write_file, \
		solution_type, solution_grid_size):
	""" Initialise a parameter to record input settings """

	# Validate input parameters
	validate_parameter(d, boundary, basis, region_no, model_problem, \
		alpha, solver_tol, dataset, error_tol, refine, uni_refine, \
		max_no, max_sweep, indicator, norm, min_data, write_file, \
		solution_type, solution_grid_size)

	# Obtain model problem functions
	function = obtain_model_problem(model_problem, d)

	# Set the true solution
	if solution_type == 2:
		pass

	# Initialise a parameter
	parameter = Parameter(d, boundary, basis, region_no, function, \
		alpha, solver_tol, dataset, error_tol, [refine, uni_refine, \
		max_no, max_sweep], indicator, norm, min_data, write_file, \
		solution_type, solution_grid_size, None)

	return parameter