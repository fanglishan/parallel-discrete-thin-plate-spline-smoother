#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 14:10:32
@Last Modified time: 2019-04-22 12:50:24

This class contains a set of parameters that may be used in the 
TPSFE program. Most of the parameters are specified at the main file. 

"""

# Import libraries
import time


class Parameter:
	""" A set of parameters for the TPSFEM program """

	# Dimension (default:2)
	_d = 2

	# Boundary type
	_boundary = 0

	# Basis function
	_basis = 0

	# Number of regions
	_region_no = []

	# Model problem function
	_function = []

	# Number of data points
	_data_size = 0

	# Smoothing parameter
	_alpha = 0.0

	# Solver tolerance
	_solver_tol = 0.0

	# Index of the dataset
	_dataset = 0

	# Error tolerance
	_error_tol = 0.0

	# Adaptive refinement parameters
	# 1-refine approach, 2-#uniform refinement
	# 3-max refine in a sweep, 4-#refine sweep
	_refine = []

	# Type of error indicator
	_indicator = 0

	# Norm type
	_norm = 0

	# Minimum number of data points for refinement
	_min_data = 0

	# Weather to write results to a file
	_write_file = False

	# Solution type (1-true solution, 2-TPSFEM fine grid)
	_solution_type = 1

	# Solution grid size
	_solution_grid_size = 10

	# TPSFEM with a fine grid
	_solution = None

	# Result folder name
	_folder = ""

	# Additional optional information
	# 1-processor ID, 2-iteration number, 3-indicator
	_info = []


	def __init__(self,d=2,boundary=1,basis=1,region_no=[], function=[], \
		alpha=0.0,solver_tol=0.1,dataset=0,error_tol=0.1,refine_approach=1, \
		uni_refine=0, max_no=1, max_sweep=1,indicator=0,norm=0, min_data=0, \
		write_file=False, solution_type=1, solution_grid_size=20):
		""" Initialise the parameters"""

		self._d = d
		self._boundary = boundary
		self._basis = basis
		self._region_no = region_no
		self._function = function
		self._data_size = 0
		self._alpha = alpha
		self._solver_tol = solver_tol
		self._dataset = dataset
		self._error_tol = error_tol
		self._refine = [refine_approach, uni_refine, max_no, max_sweep]
		self._indicator
		self._norm
		self._min_data
		self._write_file
		self._solution_type
		self._solution_grid_size
		self._solution = None
		self._folder = "result/result_"+str(dataset)+"_"+str(indicator)+ \
				"_"+time.strftime('%m_%d_%H_%M',time.localtime())+"/"
		self._info = [-1,-1, False]


	def __str__(self):
		""" Output parameters """

		string = "dimension: " + str(self._d) + "\n"
		string += "Boundary type: " + str(self._boundary) + "\n"
		string += "Basis function: " + str(self._basis) + "\n"
		string += "Number of regions: " + str(self._region_no) + "\n"
		string += "Model problem: " + str(self._function[0]) + "\n"
		string += "Number of data points: " + str(self._data_size) + "\n"
		string += "Smoothing parameter: " + str(self._alpha) + "\n"
		string += "Solver tolerance: " + str(self._solver_tol) + "\n"
		string += "Dataset: " + str(self._dataset) + "\n"
		string += "Error tolerance: " + str(self._error_tol) + "\n"
		string += "Refinement approach: " + str(self._refine[0]) + "\n"
		string += "Number of uniform refine: " + str(self._refine[1]) + "\n"
		string += "Refine each sweep: " + str(self._refine[2]) + "\n"
		string += "Max refine sweeps: " + str(self._refine[3]) + "\n"
		string += "Error indicator: " + str(self._indicator) + "\n"
		string += "Norm type: " + str(self._norm) + "\n"
		string += "Min data refine: " + str(self._min_data) + "\n"
		string += "Solution type: " + str(self._solution_type) + "\n"
		string += "Solution grid size: " + str(self._solution_grid_size) + "\n"
		if self._write_file:
			string += "Results stored at: " + str(self._folder)
		
		return string


	def set_boundary(self, boundary):	
		""" Set boundary type """
		self._boundary = boundary

	def set_basis(self, basis):
		""" Set basis function """
		self._basis = basis

	def set_region_no(self, region_no):
		""" Set number of data regions """
		self._region_no = region_no

	def set_function(self, function):
		""" Set model problem functions """
		self._function = function

	def set_data_size(self, data_size):
		""" Set number of data points """
		self._data_size = data_size

	def set_alpha(self, alpha):
		""" Set smoothing parameter alpha """
		self._alpha = alpha

	def set_solver_tol(self, solver_tol):
		"""  Set solver tolerance """
		self._solver_tol = solver_tol

	def set_dataset(self, dataset):
		""" Set dataset index """
		self._dataset = dataset

	def set_error_tol(self, error_tol):
		"""  Set error tolerance """
		self._error_tol = error_tol

	def set_refine(self, refine):
		""" Set adaptive refinement parameters """
		self._refine = refine

	def set_indicator(self, indicator):
		""" Set error indicator type """
		self._indicator = indicator

	def set_norm(self, norm):
		""" Set norm type """
		self._norm = norm

	def set_min_data(self, min_data):
		""" Set min number of data for refine """
		self._min_data = min_data

	def set_write_file(self, write_file):
		""" Set whether to write results to a file """
		self._write_file = write_file

	def set_solution_type(self, solution_type):
		""" Set what solution will the grid be compared to """
		self._solution_type = solution_type

	def set_solution_grid_size(self, solution_grid_size):
		""" Set the size of the solution grid """
		self._solution_grid_size = solution_grid_size

	def set_solution(self, solution):
		""" Set the reference solution """
		self._solution = solution

	def set_folder(self, folder):
		""" Set the folder for the results """
		self._folder = folder

	def set_processor(self, p):
		""" Set processor ID """
		if len(self._info) > 0:
			self._info[0] = p
		else:
			self._info.append(p)

	def set_iteration(self, iteration):
		""" Set iteration number """
		if len(self._info) > 1:
			self._info[1] = iteration
		else:
			self._info.append(iteration)

	def set_local(self, local=True):
		""" Set local parameter error indicator """
		self._info[2] = local


	def get_boundary(self):	
		""" Get boundary type """
		return self._boundary

	def get_basis(self):
		""" Get basis function """
		return self._basis

	def get_region_no(self):
		""" Get number of data regions """
		return self._region_no

	def get_function(self):
		""" Get model problem functions """
		return self._function

	def get_data_size(self):
		""" Get number of data points """
		return self._data_size

	def get_alpha(self):
		""" Get smoothing parameter alpha """
		return self._alpha

	def get_solver_tol(self):
		"""  Get solver tolerance """
		return self._solver_tol

	def get_dataset(self):
		""" Get dataset index """
		return self._dataset

	def get_error_tol(self):
		"""  Get error tolerance """
		return self._error_tol

	def get_refine(self):
		""" Get adaptive refinement parameters """
		return self._refine

	def get_uni_refine(self):
		""" Get the number of uniform refinement """
		return self._uni_refine

	def get_max_no(self):
		""" Get the max number of refinement each sweep """
		return self._max_no

	def get_max_sweep(self):
		""" Get the max number of refinement sweeps """
		return self._max_sweep

	def get_indicator(self):
		""" Get error indicator type """
		return self._indicator

	def get_norm(self):
		""" Get norm type """
		return self._norm

	def get_min_data(self):
		""" Get min number of data for refine """
		return self._min_data

	def get_write_file(self):
		""" Get whether to write results to a file """
		return self._write_file

	def get_solution_type(self):
		""" Get what solution will the grid be compared to """
		return self._solution_type

	def get_solution_grid_size(self):
		""" Get the size of the solution grid """
		return self._solution_grid_size

	def get_solution(self):
		""" Get the reference solution """
		return self._solution

	def get_folder(self):
		""" Get the folder for the results """
		return self._folder

	def get_processor(self):
		""" Get processor ID """
		return self._info[0]

	def get_iteration(self):
		""" Get iteration number """
		return self._info[1]

	def get_local(self):
		""" Get local parameter error indicator """
		return self._info[2]