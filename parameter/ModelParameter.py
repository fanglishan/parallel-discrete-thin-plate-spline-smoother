#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 14:11:17
@Last Modified time: 2019-04-16 23:42:11

This class contains a set of classes that define the index of certain
parameter. The classes in this file relates to the model problems.

"""


class ModelProblem:
	""" Model problem functions """

	zero = 0
	linear = 1
	quadratic = 2
	cubic = 3
	exp = 4
	sine = 5         # Sine function
	osci_sine = 6    # Oscillatory sine function
	steep_exp = 7    # Steep exponential function

