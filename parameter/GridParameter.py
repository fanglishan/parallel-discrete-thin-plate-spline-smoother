#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 14:11:41
@Last Modified time: 2019-04-16 14:17:58

This class contains a set of classes that define the index of certain
parameter. The classes in this file relates to the grid.

"""

class NodeSet:
	""" Node types """
	full_node = 1
	ghost_node = 2


class BoundaryType:
	""" Edge boundary types """
	Dirichlet = 1
	Neumann = 2


class BasisFunction:
	""" Degree of polynomial basis function """
	linear = 1
	quadratic = 2
	cubic = 3