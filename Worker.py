#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 23:23:13

This worker class will solve and refine each of the sub-grids in each 
worker processor. It will communicate with the other workers and return
the final sub-grid to the host processor.

"""

# Import other classes
from commun.Communication import Communication
from iterative.PCG import conjugate_gradient
from iterative.Preconditioner import identity_preconditioner, DD_init, DD_preconditioner
from triangle.UniformRefinement import uniform_refinement
from triangle.AdaptiveRefinement import adaptive_refinement, build_base_edges
from triangle.BuildEquationLinear import build_equation_linear_2D
from triangle.TriangleIntegrateLinear import linear_integrate
from indicator.ErrorIndicator import indicate_error, estimate_true_error
from grid.NodeTable import node_iterator
from indicator.ErrorIndicatorTrue import indicate_true_edge_error
from performance.FindTriangle import find_triangle_number
from commun.SynchronisationFunctions import get_max_error_indicator
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot2D as tpsfemPlot2D

# Import libraries



def worker(commun, parameter, true_soln, rhs, error_tol, tolerance, m, max_sweep, indicator_type):
	"""" Worker function for each processor in parallel """

	# Processor ID
	p = commun.get_my_no()

	print "Worker", p, "starts"

	# Parameters
	max_sweep = parameter.get_refine()[3]
	error_tol = parameter.get_error_tol()

	# Get the subgrid from the host
	grid = commun.get_grid(commun.get_host_no())

	# Data region
	region = grid.get_region()

	# Update parameter
	parameter.set_processor(p)
	parameter.set_data_size(region.get_data_size())



	# Approximate the error indicator of base edges
	# Obtain the error indicator norm
	error_indicator = indicate_error(commun, grid, parameter)

	# Obtain the maximum error indicator norm throughout processors
	max_error_indicator = get_max_error_indicator(commun, error_indicator)

	# Estimate true error
	error = estimate_true_error(grid, parameter)

	print " ", p, "      ", 0, "      ", "{0:.6f}".format(error_indicator), "        ","{0:.6f}".format(error)

	# The number of refinement sweeps
	sweep = 1

	while max_error_indicator > error_tol:
	#while error > error_tol:
		parameter.set_iteration(sweep)

		# Refine the given grid m sweeps
		#grid = uniform_refinement(commun, grid, m)
#		grid,refine_edge_count = adaptive_refinement(commun, grid, m,
#			                   error_tol,true_soln,rhs,indicator_type)


		# Calculate the stiffness value and load value for each node in the grid 
#		build_equation_linear_2D(grid,Poisson_tri_integrate,rhs)


		#fem_matrix, l_matrix, u_matrix, node_id_list = DD_init(grid)

		# Solve the system of equations using the CG method
#		loops, resid = conjugate_gradient(commun, grid,identity_preconditioner)#,tolerance=1.0E-6)
		#loops, resid = conjugate_gradient(commun, grid, DD_preconditioner,
		#						l_matrix, u_matrix, node_id_list,tolerance=1.0E-6)   
		
		# Output the performance of conjugate gradient method
		#print " CG method took "+str(loops)+" to reach a tolerance of "+str(resid)

		# Approximate the error indicator of base edges
		# Obtain the error indicator norm
		error_indicator = indicate_error(commun, grid, parameter)

		# Obtain the maximum error indicator norm throughout processors
		max_error_indicator = get_max_error_indicator(commun, error_indicator)

		# Calculate the true error of the grid for comparison		
		error = estimate_true_error(grid, parameter)

		# Output details
		print " ", commun.get_my_no(), "      ", sweep, "      ", "{0:.6f}".format(error_indicator), "        ","{0:.6f}".format(error)
		
		# increase sweep count
		sweep += 1

		# terminate if maximum sweep is reached
		if sweep > max_sweep:
			print "reach maximum refinement sweep"
			return
	

	# Send sub-grid to the host processor
	commun.send_grid(commun.get_host_no(),grid)