# -*- coding: utf-8 -*-
"""
@author: Fang

This files contains the synchronisation routines for processors
"""


def find_population_table(commun, grid):
	""" Determine the number of full nodes in each worker """
	
	# Initialise a list of the size of the number of workers
	population_table = [0]*commun.get_no_workers()
	
	# Gather full node number from each processor
	pp = commun.all_gather(grid.get_no_nodes())

	# Update the population table for each worker
	for i in range(commun.get_no_workers()):
		population_table[i] = pp[i]

	return population_table

def get_global_keep_refine(commun, keep_refine):
	""" Check if any worker needs to continue refining """

	# Find out any workers require keep refining
	pp = commun.all_gather(keep_refine)
	
	# Iterate through workers
	for i in range(commun.get_no_workers()):
		
		# If any worker needs to refine, return True
		if pp[i]:
			return True

	# If no refinement is needed for all workers
	return False


def get_max_error_indicator(commun, error_indicator):
	""" Find out the error indicator norm in each worker """

	# Initialise a list of the size of the number of workers
	error_indicator_table = [0]*commun.get_no_workers()

	# Gather error indicators from all processors
	pp = commun.all_gather(error_indicator)
	
	# Update the error indicator for all workers
	for i in range(commun.get_no_workers()):
		error_indicator_table[i] = pp[i]

	# Return the maximum error indicator
	return max(error_indicator_table)

