# -*- coding: utf-8 -*-
"""

@author: Fang

This file contains the functions that perform error indicator 
communication and update routines among the processors

"""

# Import libraries
from grid.GridFunctions import find_copy_node, find_node, find_global_id, \
			check_node_in_grid, check_edge_nodes_in_grid, \
			check_edge_nodes_ghost, find_node_id_by_global_id


def check_neighbour_edge(grid, node1, node2):
	""" 
	Check if the given edge is in the neighbour processors 
	Return a list of processors that these two edges are in
	"""

	from grid.NghNodes import ngh_iterator

	# Full commun neighbour nodes of the grid
	full_commun = grid.reference_full_commun()

	# Ghost commun neighbour nodes of the grid
	ghost_commun = grid.reference_ghost_commun()
	
	# Initialise two lists of neighbour processor id
	# for both node 1 and node 2
	ngh_list_1 = []
	ngh_list_2 = []

	# Iterate through the full commun dictionary
	for ngh_id, node_id_list in ngh_iterator(full_commun):

		# Check if node 1 appears in the list 
		if node1.get_node_id() in node_id_list:
			ngh_list_1.append(ngh_id)

		# Check if node 2 appears in the list
		if node2.get_node_id() in node_id_list:
			ngh_list_2.append(ngh_id)

	# Iterate through the ghost commun dictionary
	for ngh_id, node_id_list in ngh_iterator(ghost_commun):

		# Check if node 1 appears in the list 
		if node1.get_node_id() in node_id_list:
			ngh_list_1.append(ngh_id)

		# Check if node 2 appears in the list
		if node2.get_node_id() in node_id_list:
			ngh_list_2.append(ngh_id)

	# Use set to remove duplicates and & to find the intersection
	return list(set(ngh_list_1) | set(ngh_list_2))


def update_error_indicator(grid, node1, node2, update_edges, error_indicator):
	""" Update the update edge lists if the edge is in other processors """

	from copy import copy

	# Check if the nodes are in other processors
	ngh_lists = check_neighbour_edge(grid, node1, node2)

	# Iterate through the processors the edge is in
	for i in range(0,len(ngh_lists)):

		# If the processor has not been added
		if not update_edges.has_key(str(ngh_lists[i])):

			# Initialise the values of the dictionary as an empty list
			update_edges[ngh_lists[i]] = list()

		# Add the edge to the dictionary
		update_edges[ngh_lists[i]].append((copy(node1.get_global_id()),
				copy(node2.get_global_id()),error_indicator))


def update_error_indicator_list(commun, grid, base_edges, error_tol):
	"""
	Check if the edges in base edge list is in other processors
	Update them if the error indicators are larger than the error
	tolerance
	"""

	from grid.GridFunctions import find_global_id, find_node

	# Initialise a dictionary for edges that need updated error indicator
	update_edges = {}

	# Iterate through the base edges
	for edge in base_edges:

		# If both endpoints of the edge are ghost nodes
		if check_edge_nodes_ghost(grid, edge):

			# Skip this edge
			continue

		# If the error indicator of the edge is larger than the error
		# tolerance (need refine)
		if grid.get_error_indicator(edge.get_endpt1(),edge.get_endpt2()) > error_tol:

			# Node of endpoints of the edge
			node1 = find_node(grid, edge.get_endpt1())
			node2 = find_node(grid, edge.get_endpt2())

			# Error Indicator of the edge
			error_indicator = grid.get_error_indicator(edge.get_endpt1(),edge.get_endpt2())

			# Update the error indicator of the edge in the dictionary
			update_error_indicator(grid, node1, node2, update_edges, error_indicator)

	return update_edges


def communicate_edge_indicator_list(commun, grid, update_edges):
	""" 
	Send the refine edges to corresponding processors
	and receive edges that need to be checked from
	the other processors
	"""

	from grid.NghNodes import ngh_iterator

	# Full commun neighbour nodes of the grid
	full_commun = grid.reference_full_commun()

	# Initialise a list of neighbour processors
	ngh_id_list = []

	# Add the neighbouring processors to a list
	for ngh_id, node_id_list in ngh_iterator(full_commun):
		ngh_id_list.append(ngh_id)


	# Iterate through the neighbouring processors
	for i in range(0,len(ngh_id_list)):
		
		# If some edge that is also in that processor has been refined
		if update_edges.has_key(str(ngh_id_list[i])):

			# Send a list information of the edge
			commun.send_array(int(ngh_id_list[i]),update_edges[str(ngh_id_list[i])])
		else:

			# If not send an empty list anyway
			commun.send_array(int(ngh_id_list[i]),list())

	# Initialise a new dictionary for error indicators from the other processors
	received_update_edges = {}

	# Iterate through the neighbouring processors
	for i in range(0,len(ngh_id_list)):

		# Get the list from the processor i
		received_update_edges[str(ngh_id_list[i])] = commun.get_array(int(ngh_id_list[i]))
	

	return received_update_edges


def update_receive_indicators(commun, grid, receive_edge_indicators, base_edges):

	# Iterate throught the received edges
	for key,node_pair_list in receive_edge_indicators.iteritems():
		
		# Iterate through a list from a processor (key)
		for i in range(0,len(node_pair_list)):

			# Obtain the node id of the first node with global id
			node_id1 = find_node_id_by_global_id(grid,node_pair_list[i][0])

			# If the node id is not found, ignore this edge 
			if node_id1 is None:	
				continue

			# Obtain the node id of the second node with global id
			node_id2 = find_node_id_by_global_id(grid,node_pair_list[i][1])

			# If the node id is not found, ignore this edge 
			if node_id2 is None:	
				continue

			# Check if the edge has been refined
			if grid.is_edge(node_id1,node_id2):

				# If the error indicator of the edge is smaller than the
				# error indicator of the edge from other processors
				if grid.get_error_indicator(node_id1,node_id2) < node_pair_list[i][2]:

					# Update the error indicator of the edge
					grid.set_error_indicator(node_id1,node_id2,node_pair_list[i][2])
					grid.set_error_indicator(node_id2,node_id1,node_pair_list[i][2])
						

def update_error_indicators(commun, grid, base_edges, error_tol):
	""" Update the error indicators of base edges across processors """

	# Obtain error indicators for the base edges of the current processor
	base_edge_indicators = update_error_indicator_list(commun, grid, base_edges, error_tol)

	# Send the error indicators to other processors
	# Also receive error indicators from the other processors
	receive_edge_indicators = communicate_edge_indicator_list(commun, grid, base_edge_indicators)

	# Update the error indicators with the received error indicators
	update_receive_indicators(commun, grid, receive_edge_indicators, base_edges)
