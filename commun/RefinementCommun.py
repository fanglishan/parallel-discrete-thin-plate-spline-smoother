# -*- coding: utf-8 -*-
"""

@author: Fang

This file contains the functions that perform refined edges 
communication and refine the received edges among the processors

"""

# Import libraries
from grid.GridFunctions import find_copy_node, find_node, find_global_id, \
			check_node_in_grid, check_edge_nodes_in_grid, check_edge_nodes_ghost


def check_neighbour_edge(grid, node1, node2):
	""" 
	Check if the given edge is in the neighbour processors 
	Return a list of processors that at least one of the
	endpoint of the edge are in
	"""

	from grid.NghNodes import ngh_iterator

	# Full commun neighbour nodes of the grid
	full_commun = grid.reference_full_commun()

	# Ghost commun neighbour nodes of the grid
	ghost_commun = grid.reference_ghost_commun()
	
	# Initialise two lists of neighbour processor id
	# for both node 1 and node 2
	ngh_list_1 = []
	ngh_list_2 = []

	# Iterate through the full commun dictionary
	for ngh_id, node_id_list in ngh_iterator(full_commun):

		# Check if node 1 appears in the list 
		if node1.get_node_id() in node_id_list:
			ngh_list_1.append(ngh_id)

		# Check if node 2 appears in the list
		if node2.get_node_id() in node_id_list:
			ngh_list_2.append(ngh_id)

	# Iterate through the ghost commun dictionary
	for ngh_id, node_id_list in ngh_iterator(ghost_commun):

		# Check if node 1 appears in the list 
		if node1.get_node_id() in node_id_list:
			ngh_list_1.append(ngh_id)

		# Check if node 2 appears in the list
		if node2.get_node_id() in node_id_list:
			ngh_list_2.append(ngh_id)

	# Use set to remove duplicates and & to find the intersection
	return list(set(ngh_list_1) | set(ngh_list_2))


def update_refine_edge_list(grid, node1, node2, refine_edges, refine_type):
	""" Update the refined edge lists if the edge is in other processors """

	from copy import copy

	# Check if the nodes are in other processors
	ngh_lists = check_neighbour_edge(grid, node1, node2)
	
	# Iterate through the processors the edge is in
	for i in range(0,len(ngh_lists)):

		# If the processor has not been added
		if not refine_edges.has_key(str(ngh_lists[i])):

			# Initialise the values of the dictionary as an empty list
			refine_edges[ngh_lists[i]] = list()

		# Add the edge to the dictionary
		refine_edges[ngh_lists[i]].append((copy(node1.get_global_id()),
					copy(node2.get_global_id()),refine_type))


def communicate_refine_edge_list(commun, grid, refine_edges):
	""" 
	Send the refine edges to corresponding processors
	and receive edges that need to be checked from
	the other processors
	"""

	from grid.NghNodes import ngh_iterator

	# Full commun neighbour nodes of the grid
	full_commun = grid.reference_full_commun()

	# Initialise a list of neighbour processors
	ngh_id_list = []

	# Add the neighbouring processors to a list
	for ngh_id, node_id_list in ngh_iterator(full_commun):
		ngh_id_list.append(ngh_id)


	# Iterate through the neighbouring processors
	for i in range(0,len(ngh_id_list)):
		
		# If some edge that is also in that processor has been refined
		if refine_edges.has_key(str(ngh_id_list[i])):

			# Send a list information of the edge
			commun.send_array(int(ngh_id_list[i]),refine_edges[str(ngh_id_list[i])])
		else:

			# If not send an empty list anyway
			commun.send_array(int(ngh_id_list[i]),list())

	# Initialise a new dictionary for edges that are refined from the other processors
	received_refine_edges = {}

	# Iterate through the neighbouring processors
	for i in range(0,len(ngh_id_list)):

		# Get the list from the processor i
		received_refine_edges[str(ngh_id_list[i])] = commun.get_array(int(ngh_id_list[i]))

	return received_refine_edges
