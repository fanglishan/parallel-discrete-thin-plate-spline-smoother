#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 23:28:05

This class contains routines to call the corresponding error indicator.

"""

# Import other classes
from grid.NodeTable import node_iterator
#import interval.AdaptiveRefinement as refine1d
import triangle.AdaptiveRefinement as refine2d
from parameter.RefineParameter import IndicatorType
from indicator.norm import vector_norm
import indicator.EIInf as IndicatorInf
import indicator.EITrue as TrueSoln

# Import libraries
from scipy import linalg
from numpy import inf


def indicate_edge_error(grid, edge, parameter):
	""" Choose the error indicator function and pass the parameters """

	# Indicator type
	indicator_type = parameter.get_indicator()

	# Return infinity for the edge => uniform refine
	if indicator_type == IndicatorType.infinity:
		return IndicatorInf.indicate_edge_error(grid, edge, parameter)

	# Indicate error using true solution
	elif indicator_type == IndicatorType.true:
		return TrueSoln.indicate_true_edge_error(grid, edge, parameter)

	else:
		print "Warning: invalid indicator"
		return


def indicate_error(grid, parameter):
	""" Choose the error indicator function and pass the parameters """

	# Indicator type
	indicator_type = parameter.get_indicator()

	# Obtain a list of base edges
	if grid.get_dim() == 1:

#		base_edges = refine1d.build_base_edges(grid)
		pass

	elif grid.get_dim() == 2:

		base_edges = refine2d.build_base_edges(grid)

	else:
		print "Warning: invalid dimension"


	# Set all indicators infinity => uniform refine
	if indicator_type == IndicatorType.infinity:
		error_indicator_list = IndicatorInf.indicate_error(grid, base_edges, parameter)

	# Indicate error using true solution
	elif indicator_type == IndicatorType.true:
		error_indicator_list = TrueSoln.indicate_true_error(grid, base_edges, parameter)

	else:
		print "Warning: invalid indicator"
		return

	# Norm of the error indicator values
	error_norm =  vector_norm(error_indicator_list, parameter.get_norm())

	return error_indicator_list, error_norm


def estimate_true_error(grid, parameter):
	""" Estimate error of every node in the grid """

	# True solution
	true_soln = parameter.get_function()[0]

	# Initialise a list of error 
	error_list = []

	# Iterate through the nodes in the grid
	for node in node_iterator(grid):

		# Calculate the error 
		error = abs(node.get_value() - true_soln(node.get_coord()))

		# Add the new error to the list
		error_list.append(error)

	return vector_norm(error_list, parameter.get_norm())