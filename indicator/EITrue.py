#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-22 23:08:37
@Last Modified time: 2019-04-22 23:22:07

This class contains the error indicator that compares the interpolated 
midpoint value with the true solution and return the difference as the 
error indicator. This error indicator is for testing purpose.

"""



def calculate_midpoint(coord1, coord2):
	""" Calculate the midpoint of coordinates 1 and 2 """

	if len(coord1) != len(coord2):
		print "Warning: dimension mismatch"

	dim = len(coord1)
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (coord1[i]+coord2[i])/2.0

	return coord


def indicate_true_edge_error(grid, edge, parameter):
	""" Indicate the true error of the given edge """

	# Coordinate of the first end point of the edge
	coord1 = grid.get_coord(edge.get_endpt1())

	# Coordinate of the second end point of the edge
	coord2 = grid.get_coord(edge.get_endpt2())

	# Calculate the value of the midpoint
	midpoint_value = (grid.get_value(edge.get_endpt1())+grid.get_value(edge.get_endpt2()))/2

	# Middle coordinate
	mid_coord = calculate_midpoint(coord1, coord2)

	# Calcualte the true value of the midpoint
	true_mid_value = parameter.get_function()[0](mid_coord)

	# Estimate edge error at the midpoint
	edge_error = abs(midpoint_value-true_mid_value)
	
	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	return edge_error


def indicate_true_error(grid, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Initialise the error list for error indicators
	error_list = []

	# Indicate error of base edges
	for edge in base_edges:

		# Indicate the error of the edge
		edge_error_indicator = indicate_true_edge_error(grid, edge, parameter)

		# Add the new error to the list
		error_list.append(edge_error_indicator)


	return error_list
