#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-22 23:08:37
@Last Modified time: 2019-04-22 23:18:51

This files contains the error indicator returns a large number for all 
base edges. The adaptive refinement will become uniform refinement.

"""

# Import other classes
from grid.NodeTable import node_iterator


def indicate_edge_error(grid, edge, parameter):
	""" Indicate a high error of given edge """
	
	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(), edge.get_endpt2(), 1000000000000)
	grid.set_error_indicator(edge.get_endpt2(), edge.get_endpt1(), 1000000000000)

	return 1000000000000


def indicate_error(grid, base_edges, parameter):
	""" Indicate a very high error of the grid """

	# Initialise a list for error indicators
	error_list = []

	# Indicate error of base edges
	for edge in base_edges:

		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, edge)

		# Add the new error to the list
		error_list.append(edge_error_indicator)


	return error_list
