#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2019-04-22 23:25:31

This class contains norm for the error.

"""

# Import libraries
from scipy import linalg
from numpy import inf


def vector_norm(input_list, norm_type=2):
	""" calculate different norms """

	if len(input_list) == 0:
		return 0.0
		
	# Maximum norm
	if norm_type == 0:
		return max(input_list)

	# One norm
	elif norm_type == 1:
		return sum(input_list)/len(input_list)

	# Two norm
	elif norm_type == 2:
		return two_norm(input_list)
